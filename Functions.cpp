//---------------------------------------------------
/* namespace FUNCTIONS
 *
 * namespace containing various functions used across the model
 
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include <fstream>
#include <numeric>
#include <algorithm>
#include <tgmath.h>
#include <memory>
#include "Person.h"
#include "Functions.h"
#include "GlobalParams.h"

using namespace std;
using namespace GLOBALPARAMS;

namespace FUNCTIONS {
    
    //This is a helper for the outputVector function
    unique_ptr<ofstream> _setupOutputVectorFile ( const string filename, const bool overwrite) {
        
        auto base = ios::app;
        
        if (overwrite)
            base = ios_base::out;
        
        //Rewrite using shared pointer removes the need for the calling function to clean up
        //Changed to unique_ptr because the ofstream destructor calls .close()
        return unique_ptr<ofstream> ( new ofstream (filename, base) );
    }
    void writeFile(const string s, const string filename, const bool overwrite){
        
        unique_ptr<ofstream> ofile = _setupOutputVectorFile ( filename, overwrite );
        *ofile<<s << endl;
    }
    
    //INITIALIZATIONS -------------------------------
    vector<double> zip_csa_conv ( const std::vector<double> zip_data) {
        if (zip_data.size() != 32) //there should be 32 entries to convert (32 baltimore zip codes)
            return std::vector<double> {};
        std::vector<double> csa_data (55, 0.0); //nCSA
        for (auto i = static_cast<std::vector<double>::size_type>(0); i < vvCSA_COV_BY_ZIP.size(); i ++)
            for (auto j = static_cast<std::vector<double>::size_type>(0); j < vvCSA_COV_BY_ZIP[i].size(); j++)
                csa_data[j] += vvCSA_COV_BY_ZIP[i][j] * zip_data[i];
        return csa_data;
    }

    int assignAge0(gsl_rng * rng,vector<double> vAgeGroups){//returns a random age [15-18][19-24][25-64][65+]
        double r=gsl_rng_uniform(rng);
        bool cont=true;
        if (r<vAgeGroups[0]){//[15-18]
            int age= (gsl_rng_uniform_int(rng, 3)+15);
            return age;
        }
        else{
            if (r<vAgeGroups[1]){//[19-25]
                int age= (gsl_rng_uniform_int(rng, 5)+19);
                return age;
            }
            else{
                if (r<vAgeGroups[2]){//[25-64]
                    int age= (gsl_rng_uniform_int(rng, 39)+25);
                    return age;
                }
                else{//[65-75]
                    int age= (gsl_rng_uniform_int(rng, 9)+65);
                    return age;
                }}}
    }
    
    int assignRace0(gsl_rng * rng, double bRatio){//returns a random race
        //0 for W; 1 for B;
        if (gsl_rng_uniform(rng)<bRatio) return 1;
        else return 0;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
    //HIV NATURAL HISTORY-------------------------------------------------------------------------------------------------------------------------------------------
    int return_stateDuration(gsl_rng * rng, vector<int> vD){//returns a random value for duration of acute infection
        if (vD[1]<= vD[0]) return 0;
        else
        return gsl_rng_uniform_int(rng, vD[1]-vD[0])+vD[0];
    }
    
    int return_chronicDurationAfterART(gsl_rng * rng, vector<int> vCHRONIC_DURATION_AFTER_ART){
        return  (gsl_rng_uniform_int(rng,vCHRONIC_DURATION_AFTER_ART[1]-vCHRONIC_DURATION_AFTER_ART[0])+vCHRONIC_DURATION_AFTER_ART[0]);
     }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //PARTNERSHIPS------------------------------------------------------------------------------------------------------------------------------------
    int findIndex(Person * p,vector<Person> vec){ //returns the index of person *p in a given queue
        int N=vec.size();
        for(int i =0;i<N;i++){            if(&vec[i]== p) return i;        }
        return -1;
    }
    
    int findIndex(Person * p,vector<Person*> vec){//returns the index of person *p in a given queue
        int N=vec.size();
        for(int i =0;i<N;i++){           if(vec[i]== p) return i;        }
        return -1;
    }
    
    Person * returnRandomMember(gsl_rng * rng,vector<Person*> vec){//retunrs the queue member at a random index
        int r = gsl_rng_uniform_int(rng,vec.size());
        return vec[r];
    }
    
    Person * returnRandomMember(gsl_rng * rng,vector<vector<Person*>> vec){//retunrs the queue member at a specific index
        int r1 = gsl_rng_uniform_int(rng,vec.size());
        int r2=gsl_rng_uniform_int(rng,vec[r1].size());
        return vec[r1][r2];
    }
    
    int returnRandomMember(gsl_rng * rng,vector<int> vec){//retunrs the queue member at a specific index
        int r1 = gsl_rng_uniform_int(rng,vec.size());
        return vec[r1];
    }
    
    //---------------------------------
    int assignStbPartnershipDuration(gsl_rng * rng ){//returns a random value for stable Partnership duration
        return (gsl_ran_geometric(rng, nD_STB_MEAN));
    }
    int assignCslPartnershipDuration(gsl_rng * rng ){//returns a random value for casual Partnership duration
        return nD_CSL;
    }
    //---------------------------------
    double assign_pUnsafeAct (gsl_rng * rng, int partnershipType,double pUAIcsl, double pUAIstb){
        //can be a function of p and q attributes as well
        double pr;
        if (partnershipType==0) pr= pUAIcsl;
        else pr=pUAIstb;
        return pr;
     }
    //------------------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------------------
     //New version of returnShuffledSequence, same parameters as the old one
    vector<int> returnShuffledSequence( gsl_rng *rng, int max) {
        
        //Define a vector of the correct size
        vector<int> sequence (max);
        //iota will fill a vector, list, etc. will values starting with a
        //certain number and running until the object's size is reached
        std::iota(sequence.begin(), sequence.end(), 0);
        //random_shuffle takes a senquence and shuffles it: optionally
        //takes a random function, so I wrote a lambda function (anonymous
        //function to use the gsl.
        
        std::random_shuffle (sequence.begin(), sequence.end(),
                             [rng] (int i) -> int {return gsl_rng_uniform_int(rng,i);} );
        //The above function random_shuffle is going to be out of the C++ standard by
        // 2017, so we should move to shuffle (below).  Shuffle, the way I have written
        //it below, is much slower than random_shuffle.  Also requires another class,
        //commented out at the top of the file
        
        //std::shuffle (sequence.begin(), sequence.end(), [rng] (int i) {return gsl_rng_uniform_int(rng,i);} );
        
        //std::shuffle (sequence.begin(), sequence.end(), hivGSLRNG(rng, max));
        //return the sequence
        return sequence;
    }
    
    double returnRand_triang(gsl_rng * rng, double mode){//retunrs a double random number between [0,1] from trang dist
        double r= gsl_rng_uniform(rng); //uniform rand num
        //        double F = mode;// (c - a) / (b - a);
        if (r <= mode)
            return   (sqrt(r * mode)); //a + sqrt(U * (b - a) * (c - a));
        else
            return   (1- sqrt((1-r)* (1-mode)));  //b - sqrt((1 - U) * (b - a) * (b - c));
    }
    double returnRand_triang(gsl_rng * rng, double a, double c,double b){//retunrs a double random number between [0,1] from trang dist
        double U= gsl_rng_uniform(rng); //uniform rand num
        double F =  (c - a) / (b - a);
        if (U <= F)
            return   (a + sqrt(U * (b - a) * (c - a)));
        else
            return   (b - sqrt((1 - U) * (b - a) * (b - c)));
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    //VECTOR OPERATIONS ---------------------------------
    double vectorMedian( vector<double> vec){
        sort(vec.begin(), vec.end());
        int n=vec.size()/2;
        return vec[n];
    }
    
    vector<double> vectorRelDiffPercent(  vector<double> vec, double dom){//computes the the Percent difference of vec1 and vec2 elemenst relative to vec2
        vector<double> res;
        int n=vec.size();
        for(int i=0; i<n;i++)
            res.push_back(100*(vec[i]-dom)/dom);
        return res;
    }
    
    vector<double> vectorSummaryStat( vector<double> vec){
        vector<double> res;
        sort(vec.begin(), vec.end());
        int n=vec.size()/2;
        res.push_back(vec.front());//min
        res.push_back(vec.back());//max
        res.push_back(vectorAve(vec));//mean
        res.push_back(vec[n]);//median
        return res;
    }
/*    //---------------------------------
    vector<vector<double>> vectorChangeElements_Relative(vector<double> vec,int index,double inc){//take an index element and increment it, then changes all other values relative to that one
        vector<vector<double>> res;
        vector<double> temp;
        double d0=vec[index];
        double d1= d0*(1+inc);
        for(double s:vec){
            temp.push_back(s*d1/d0);
        }
        res.push_back(temp);
        temp={};
        d1= d0*(1-inc);
        for(double s:vec){
            temp.push_back(s*d1/d0);
        }
        res.push_back(temp);
        return res;
    }
    
    vector<vector<int>> vectorChangeElements_Relative(vector<int> vec,int index,double inc){//take an index element and increment it, then inc all other values relative to that one
        vector<vector<int>> res;
        vector<int> temp;
        int d0=vec[index];
        int d1= d0*(1+inc);
        for(double s:vec){
            temp.push_back(s*d1/d0);
        }
        res.push_back(temp);
        temp={};
        d1= d0*(1-inc);
        for(double s:vec){
            temp.push_back(s*d1/d0);
        }
        res.push_back(temp);
        return res;
     }
    */
    vector<vector<double>> vectorChangeElements_OneByOne(vector<double> vec,double inc){
        vector<vector<double>> res;
        int N=vec.size();
        for(int i=0;i < N;i++){
            vector<double> temp=vec;
            temp[i]=temp[i]*(1+inc);
             res.push_back(temp);
            //
            temp=vec;
            temp[i]=temp[i]*(1-inc);
             res.push_back(temp);
        }
        return res;
    }
    
    vector<vector<int>> vectorChangeElements_OneByOne(vector<int> vec,double inc ){
        vector<vector<int>> res;
        int N=vec.size();
        for(int i=0;i<N;i++){
            vector<int> temp=vec;
            temp[i]=temp[i]*(1+inc);
             res.push_back(temp);
            //
            temp=vec;
            temp[i]=temp[i]*(1-inc);
            res.push_back(temp);
        }
        return res;
    }
    
   
    
}

