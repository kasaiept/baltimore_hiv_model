//---------------------------------------------------
/* namespace FUNCTIONS
 *
 * namespace containing various functions used across the model
 
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#ifndef __HIVMODEL_FUNCTIONS_H
#define __HIVMODEL_FUNCTIONS_H

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "Person.h"
#include "GlobalParams.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <tgmath.h>
//Added by Jeff
#include <algorithm>
#include <iterator>
#include <memory> //for smart pointers

using namespace std;
using namespace GLOBALPARAMS;

namespace FUNCTIONS  {
    
    //private:
    //This is a helper for the outputVector function
    unique_ptr<ofstream> _setupOutputVectorFile ( const string filename, const bool overwrite) ;
    
    //public:
    //population creation-------------------------------
    int assignAge0(gsl_rng * rng,vector<double> vAgeGroups);
    int assignRace0(gsl_rng * rng, double bRatio);
    
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
    //HIV NATURAL HISTORY-------------------------------------------------------------------------------------------------------------------------------------------
    int return_stateDuration(gsl_rng * rng, vector<int> vD);
    int return_chronicDurationAfterART(gsl_rng * rng, vector<int>);
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //PARTNERSHIPS------------------------------------------------------------------------------------------------------------------------------------
    int findIndex(Person * p,vector<Person> vec);
    int findIndex(Person * p,vector<Person*> vec);
    
    Person * returnRandomMember(gsl_rng * rng,vector<Person*> vec);
    Person * returnRandomMember(gsl_rng * rng,vector<vector<Person*>> vec);
    
    int returnRandomMember(gsl_rng * rng,vector<int> vec);
    bool evaluateRaceAgeMixing(gsl_rng * rng, Person * p,Person * q, int);
    
    //---------------------------------
    int assignStbPartnershipDuration(gsl_rng * rng);
    int assignCslPartnershipDuration(gsl_rng * rng);
    
    //------------------------------------------------------------------------------------------------------------------------------------
    //TRANSMISSION ------------------------------------------------------------------------------------------------------------------------------------
    double  assign_pUnsafeAct (gsl_rng * rng, int partnershipType,double pUAIcsl, double pUAIstb);
    
    
    double vectorMedian( vector<double> vec);
    vector<double> vectorSummaryStat( vector<double> vec);
    //------------------------------------------------------------------------------------------------------------------------------------
    //HELPERS ------------------------------------------------------------------------------------------------------------------------------------
    
    template < typename I >
    void printVector(const vector<I> &vec){
        for (auto outer:vec){
            cout<<outer<<" ";
        }
        cout<<endl;
    }
    
    template < typename I >
    void printVector(const vector<vector<I>> &vec){
        for(auto outer:vec){
            for (auto inner:outer){
                cout<<inner<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
    }
    
    template < typename I >
    void printVector(const vector<I> &vec, const string &s){
        cout<<s<<endl;
        for (auto outer:vec){
            cout<<outer<<" ";
        }
        cout<<endl;
    }
    
    template < typename I >
    void printVector(const vector<vector<I>> &vec, const string &s){
        cout<<s<<endl;
        for(auto outer:vec){
            for (auto inner:outer){
                cout<<inner<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
    }
    
    //HELPERS ------------------------------------------------------------------------------------------------------------------------------------
    void writeFile(const string s, const string filename, const bool overwrite);
    
    
    
    
    
    template <typename I>
    void writeVector(const vector< vector <I> > &vec, const string &filename, const bool overwrite){
        unique_ptr<ofstream> ofile = _setupOutputVectorFile ( filename, overwrite );
        for (auto outer = vec.begin(); outer != vec.end(); outer ++) {
            if (outer != vec.begin())  //Eliminates the newline at the end of file
                *ofile << endl;
            ostream_iterator<I> output_iterator(*ofile , " ");
            copy(outer->begin(), outer->end(), output_iterator);
        }
        *ofile << endl;
    }
    
    template <typename I>
    void writeVector(const vector< vector <I> > &vec, const string &filename, vector<string> fistline){
        unique_ptr<ofstream> ofile = _setupOutputVectorFile ( filename, true );
        ostream_iterator<string> output_iterator1(*ofile, " ");
        copy(fistline.begin(), fistline.end(), output_iterator1);
        *ofile<<"\n";
        
        for (auto outer = vec.begin(); outer != vec.end(); outer ++) {
            if (outer != vec.begin())  //Eliminates the newline at the end of file
                *ofile << endl;
            ostream_iterator<I> output_iterator2(*ofile , " ");
            copy(outer->begin(), outer->end(), output_iterator2);
        }
    }
    
    template <typename I>
    void writeVector(const vector< vector< vector <I>>> &vec, const string &filename, vector<string> fistline){
        unique_ptr<ofstream> ofile = _setupOutputVectorFile ( filename, true );
        ostream_iterator<string> output_iterator1(*ofile, " ");
        copy(fistline.begin(), fistline.end(), output_iterator1);
        *ofile<<"\n";
        
        int N1=vec.size();
        for (int i=0;i<N1;i++) {
            vector<vector<I>> outer1=vec[i];
            if (i>0)  //Eliminates the newline at the end of file
                *ofile << endl;
            
            int N2=outer1.size();
            for (int j=0;j<N2;j++) {
                vector<I> outer2=outer1[j];
                if (j>0)  //Eliminates the newline at the end of file
                    *ofile << endl;
                
                ostream_iterator<I> output_iterator2(*ofile, " ");
                copy(outer2.begin(), outer2.end(), output_iterator2);
            }
        }
    }
    
    template <typename I>
    void writeVector(const vector< vector< vector <I>>> &vec, const string &filename, const bool overwrite){
        unique_ptr<ofstream> ofile = _setupOutputVectorFile ( filename, overwrite );
        int N1=vec.size();
        for (int i=0;i<N1;i++) {
            vector<vector<I>> outer1=vec[i];
            if (i>0)  //Eliminates the newline at the end of file
                *ofile << endl;
            
            int N2=outer1.size();
            for (int j=0;j<N2;j++) {
                vector<I> outer2=outer1[j];
                if (j>0)  //Eliminates the newline at the end of file
                    *ofile << endl;
                
                ostream_iterator<I> output_iterator(*ofile , " ");
                copy(outer2.begin(), outer2.end(), output_iterator);
            }
        }
    }
    
    template <typename I>
    void writeVector(const vector< I > &vec, const string &filename, const bool overwrite){
        
        unique_ptr<ofstream> ofile = _setupOutputVectorFile ( filename, overwrite );
        
        ostream_iterator<I> output_iterator(*ofile, " ");
        copy(vec.begin(), vec.end(), output_iterator);
        *ofile<<"\n";
    }
    
    
    
    
    //Vector operations----------------------------------------------------------------------------------------------------------------------------------------
    
    
    template <typename I>
    vector<double> vectorAveRows(const vector<vector<I>> &vec){//return type is double
        int rows=vec.size();
        if (rows==0) return {-1};
        int cols=vec[0].size();
        //sum all rows
        vector<double> vSum(cols,0);
        for (auto outer: vec){
            // transform(vSum.begin(), vSum.end(), outer.begin(),  plus<double>());
            for(int i=0;i<outer.size();i++){
                vSum[i] += outer[i] ;
            }}
        //  transform(vSum.begin(), vSum.end(), vSum.begin(), bind2nd(divides<double>(), rows) );
        vector<double>  res;
        for (auto num:vSum){
            res.push_back(num/rows);
        }
        return res;
    }
    
    template <typename I>
    vector<I> vectorSumRows(const vector<vector<I>> &vec){//return type is double
        int cols=vec[0].size();
        //sum all rows
        vector<I> vSum(cols,0);
        for (auto outer: vec){
            // transform(vSum.begin(), vSum.end(), outer.begin(), vSum.end(), plus<I>());
            for(int i=0;i<outer.size();i++){
                vSum[i] += outer[i] ;
            }
        }
        return vSum;
    }
    
    template <typename I>
    vector<double> vectorSumCols(const vector<vector<I>> &vec){
        auto rows=vec.size();
        auto cols=vec[0].size();
        //sum all cols
        vector<double> vSum;
        for (auto outer: vec){
            double sum=0;
            for (auto inner:outer){
                sum += inner;
            }
            vSum.push_back(sum);
        }
        return vSum;
    }
    
    
    template <typename I>
    vector<double> vectorAveCols(const vector<vector<I>> &vec){
        int rows=vec.size();
        if (rows==0) return {-1};
        int cols=vec[1].size();
        //sum all cols
        vector<double> vSum;
        for (auto outer: vec){
            double sum=0;
            for (auto inner:outer){
                sum += inner;
            }
            vSum.push_back(sum);
        }
        //averageing:
        //transform(vSum.begin(), vSum.end(), vSum.begin(), bind2nd(divides<double>(), cols) );
        vector<double>  res;
        for (auto num:vSum){
            res.push_back(num/cols);
        }
        return res;
    }
    
    template <typename I>
    double vectorAve( const vector<I> &vec){
        int s=vec.size();
        if (s==0) return -1;
        double sum=0;
        for (auto inner: vec){
            sum += inner;
        }
        return (double) sum/s;
    }
    
    template <typename I>
    vector<vector<double>> vectorAveEqualDim( const vector<vector<vector<I>>> &vec){
        double A=vec.size();
        double B=vec[0].size();
        double C=vec[0][0].size();
        if (A==0) return {{-1}};
        //sums
        vector<vector<I>> sum=vec[0];
        for (int i=1;i<vec.size();i++){
            auto outer=vec[i];
            for (int j=0;j<B;j++){
                for (int k=0;k<C;k++){
                    sum[j][k]+=outer[j][k];
                }
            }
        }
        //average
        vector<vector<double>> res;
        for (int j=0;j<B;j++){
            vector<double> temp;
            for (int k=0;k<C;k++){
                temp.push_back( sum[j][k]/A) ;
            }
            res.push_back(temp);
        }
        return res;
    }
    
    template <typename I>
    vector<vector<double>> vectorAveUneqDim( const vector<vector<vector<I>>> &vec){
        
        if (vec.size()==0) return {{-1}};
        //sums
        vector<vector<I>> sum=vec[0];
        for (int i=1;i<vec.size();i++){
            auto outer=vec[i];
            for (int j=0;j<vec[i].size();j++){
                for (int k=0;k<vec[i][j].size();k++){
                    sum[j][k]+=outer[j][k];
                }
            }
        }
        //average
        int R=vec.size();
        vector<vector<double>> res;
        for (int j=0;j<sum.size();j++){
            vector<double> temp;
            for (int k=0;k<sum[j].size();k++){
                temp.push_back( sum[j][k]/R) ;
            }
            res.push_back(temp);
        }
        return res;
    }
    
    template <typename I>
    double vectorSum( const vector<I> &vec){
        double sum=0;
        for (auto inner: vec){
            sum = sum+inner;
        }
        return  sum;
    }
    
    template <typename I>
    double vectorSum( const vector<I> &vec, int i, int j){
        double sum=0;
        for (int n=i; n<j;n++){
            sum = sum+vec[n];
        }
        return  sum;
    }
    
    template <typename I>
    double vectorSum( const vector<vector<I>> &vec){
        double sum=0;
        for (auto outer: vec){
            for (auto inner: outer){
                sum = sum+inner;
            }
        }
        return  sum;
    }
    
    template <typename I>
    void vectorReset( vector<I> &vec){
        fill(vec.begin(),vec.end(),0);
    }
    
    template <typename I>
    void vectorReset( vector<vector<I>>  &vec){
        vector<I> temp(vec[0].size(),0);
        int R=vec.size();
        vec.clear();
        for (int i=0;i<R;i++){
            vec.push_back(temp);
        }
    }
    
    template <typename I>
    vector<double> vectorPercentImp( const vector<I> &vec, double m){
        vector<double>  res;
        for (auto num:vec){
            res.push_back(100*(m-num)/m);
        }
        return res;
    }
    
    template <typename I>
    vector<double> vectorDivision( const vector<I> &vec, int dom){
        double n=dom;
        vector<double>  res;
        for (auto num:vec){
            res.push_back(num/n);
        }
        return res;
    }
    
    template <typename I>
    vector<vector<double>> vectorDivision( const vector<vector<I>> &vec, int dom){
        vector<vector<double>> res;
        double n=dom;
        for (auto outer:vec){
            vector<double> temp;
            for (auto num:outer){
                temp.push_back(num/n);
            }
            res.push_back(temp);
        }
        return res;
    }
    
    
    template <typename I>
    vector<double> vectorSubstraction( const vector<I> &vec1, const vector<I> &vec2){
        vector<double> res;
        if (vec1.size()!=vec2.size()){
            cout<<"Error: vectors don't have the same size!"<<endl;
            int x=cin.get();}
        
        int n=vec1.size();
        for(int i=0; i<n;i++){
            res.push_back(vec1[i]-vec2[i]);
        }
        return res;
    }
    
    template <typename I>
    vector<double> vectorRelDiffPercent( const vector<I> &vec1, const vector<I> &vec2){//computes the the Percent difference of vec1 and vec2 elemenst relative to vec2
        vector<double> res;
        if (vec1.size()!=vec2.size()){
            cout<<"Error: vectors don't have the same size!"<<endl;
            int x=cin.get();}
        int n=vec1.size();
        for(int i=0; i<n;i++){
            if(vec2[i]==0)
                res.push_back(0);
            else{
                res.push_back((vec1[i]-vec2[i])/vec2[i]);
            }
        }
        return res;
    }
    
    
    
    template <typename I>
    vector<I> vectorAddition( const vector<I> &vec1, const vector<I> &vec2){//computes the the Percent difference of vec1 and vec2 elemenst relative to vec2
        vector<double> res;
        if (vec1.size()!=vec2.size()){
            cout<<"Error: vectors don't have the same size!"<<endl;
            int x=cin.get();}
        int n=vec1.size();
        for(int i=0; i<n;i++){
            res.push_back((vec1[i]+vec2[i]));
        }
        return res;
    }
    
    template <typename I>
    vector<vector<I>> vectorAddition( vector<vector<I>> &vec1, const vector<vector<I>> &vec2){//computes the the Percent difference of vec1 and vec2 elemenst relative to vec2
        if (vec1.size()==0) return vec2;
        if (vec1.size()!=vec2.size())  {
            cout<<"Error: vectors don't have the same size!"<<endl;
            int x=cin.get();
        }
        int N=vec1.size();
        for(int i=0; i<N;i++){
            if (vec1[i].size()!=vec2[i].size()){
                cout<<"Error: vectors don't have the same size!"<<endl;
                int x=cin.get();
            }
            int J=vec1[i].size();
            for (int j=0;j<J;j++){
                vec1[i][j]=vec1[i][j]+vec2[i][j];
            }
        }
        return vec1;
    }
    
    template <typename I>
    vector<double> returnDist(vector<I> vec, vector<double> vBins){//bins[b0,b1,...bn]   counts[<=b0, b0<x<=b1,... bn-1 to bn, >bn]
        int bins= vBins.size();
        vector<double> counts(bins+1,0);
        for (auto val : vec){
            int idx=-1;
            for (int j=0;j<bins;j++){
                if (val<= vBins[j]) {
                    idx=j;
                    break;
                }}
            if (idx>-1)
                counts[idx]++;
            else
                counts.back()++;//biggest container last
        }
        int N=vec.size();
        for (int i=0;i<counts.size();i++)
            counts[i]=counts[i]/N;
        return counts;
    }
    
    template<typename I>
    double vectorStd(const vector<I> vec){
        double sigma_squared = 0.0;
        double ave = vectorAve(vec);
        double n=vec.size();
        for (auto val:vec)
            sigma_squared += (val - ave)*(val - ave);
        
        return sqrt(sigma_squared/(n-1));
    }
    
    
    template <typename I>
    vector<vector<double>> vectorAve(const vector<vector<vector<I>>> &vec){//return type is double
        vector<vector<double>> vSum;
        vSum=vec[0];
        int reps=vec.size();
        for (int i=1;i<reps;i++)
            vSum=vectorAddition(vSum, vec[i]);
        
        vector<vector<double>> res=vectorDivision(vSum,reps);
        return res;
        
    }
    
   // vector<vector<double>>  vectorChangeElements_Relative(vector<double> vec,int index,double inc);//take an index element and increment it, then inc all other values relative to that one
    //vector<vector<int>>  vectorChangeElements_Relative(vector<int> vec,int index,double inc);//take an index element and increment it, then inc all other values relative to that one
    vector<vector<double>> vectorChangeElements_OneByOne(vector<double> vec,double inc );
    vector<vector<int>> vectorChangeElements_OneByOne(vector<int> vec,double inc );
    vector<double> checkForNonZeroDifference(vector<double> vec);
 
    //=======================================================
    //New version of returnShuffledSequence, same parameters as the old one
    vector<int> returnShuffledSequence( gsl_rng *rng, int max) ;
    vector<double> vectorRelDiffPercent(vector<double> vec, double dom);
    
    
    double returnRand_triang(gsl_rng * rng, double mode);//retunrs a double random number between [0,1] from trang dist
    double returnRand_triang(gsl_rng * rng, double a, double c,double b);//triang[a.c.b]
    
    vector<double> zip_csa_conv ( const std::vector<double> zip_data) ; //converting zip-scale values to CSA-scale
};
#endif //__HIVMODEL_FUNCTIONS_H