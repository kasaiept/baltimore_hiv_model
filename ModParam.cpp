//----
//---------------------------------------------------
//
//  ModParam.cpp
//
//  Function definitions for reading mutable config
//  data from text file.
//
//  JPennington, June 2015
//
//
//---------------------------------------------------
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>
#include <stdexcept>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "ModParam.h"

ModParam::ModParam( const std::string &filename ) {
    
    if (!filename.empty()) {
        //Load from file "filename"
        std::cout << "--Loading Config Data from " << filename << "--" << std::endl;
        bool success = initFromFile( filename );
        if ( !success ) {
            std::cout << " Couldn't load data from file." << std::endl;
        }
    }
    
}

ModParam::ModParam( const ModParam &src ) {
    for (auto param : src.params_list) {
        switch (param->getT()) {
            case HeldTypes::Dbl : {
                
                CfgParam<double> *new_p =
                new CfgParam<double> (param->getN(),
                                      static_cast<CfgParam<double>*>(param)->get(),
                                      HeldTypes::Dbl);
                
                params_list.push_back(new_p);
                break;
            } case HeldTypes::Int : {
                
                CfgParam<int> *new_p =
                new CfgParam<int> (param->getN(),
                                   static_cast<CfgParam<int>*>(param)->get(),
                                   HeldTypes::Int);
                
                params_list.push_back(new_p);
                break;
            } case HeldTypes::Vec_d : {
                
                CfgParam<std::vector<double>> *new_p =
                new CfgParam<std::vector<double>> (param->getN(),
                                                   static_cast<CfgParam<std::vector<double>>*>(param)->get(),
                                                   HeldTypes::Vec_d);
                
                params_list.push_back(new_p);
                break;
            } case HeldTypes::Vec_i : {
                
                CfgParam<std::vector<int>> *new_p =
                new CfgParam<std::vector<int>> (param->getN(),
                                                static_cast<CfgParam<std::vector<int>>*>(param)->get(),
                                                HeldTypes::Vec_i);
                
                params_list.push_back(new_p);
                break;
            }
        }
    }
}

ModParam& ModParam::operator= (ModParam src) {
    std::swap (params_list, src.params_list);
    return *this;
}


ModParam::~ModParam () {
    for (auto param : params_list) {
        switch (param->getT()) {
            case HeldTypes::Dbl :
                delete static_cast<CfgParam<double>*>(param);
                break;
            case HeldTypes::Int :
                delete static_cast<CfgParam<int>*>(param);
                break;
            case HeldTypes::Vec_d :
                delete static_cast<CfgParam<std::vector<double>>*>(param);
                break;
            case HeldTypes::Vec_i :
                delete static_cast<CfgParam<std::vector<int>>*>(param);
                break;
        }
    }
}


bool ModParam::initFromFile ( const std::string &filename ) {
    //Load from file "filename"
    std::vector<std::string> lines;
    std::string line;
    std::ifstream cfg_file ( filename );
    if ( cfg_file.is_open() ) {
        
        while ( getline ( cfg_file, line) ) {
            if ( line[0] != '#' && line.find_first_not_of(' ') != std::string::npos )
                lines.push_back ( line );
        }
        
        cfg_file.close();
        
        //Process data into params_list object
        //for (unsigned int i = 0; i < lines.size(); i++ ) {
        for (auto line : lines) {
            //Find the = character, split line into two parts
            auto equals_pos = line.find("=");
            std::string p_name = line.substr ( 0, equals_pos );
            std::string p_value = line.substr ( equals_pos+1, line.size() );
            
            p_value = trim (p_value);
            p_name = trim (p_name);
            
            switch ( detectType (p_value) ) {
                case HeldTypes::Dbl : {
                    double dv = std::stod ( p_value );
                    CfgParam<double> *param = new CfgParam<double> ( p_name, dv, HeldTypes::Dbl );
                    params_list.push_back(param);
                    break;
                } case HeldTypes::Int : {
                    int iv = std::stoi ( p_value );
                    CfgParam<int> *param = new CfgParam<int> ( p_name, iv, HeldTypes::Int );
                    params_list.push_back(param);
                    break;
                } case HeldTypes::Vec_d : {
                    //Read the size of the vector (count spaces, size is + 1)
                    int v_size = std::count (p_value.begin(), p_value.end(), ' ') + 1;
                    std::stringstream ss (p_value);
                    std::vector<double> vec (v_size);
                    for (unsigned int i = 0; i < vec.size(); i ++ ) {
                        ss >> vec[i];
                    }
                    CfgParam<std::vector<double>> *param =
                    new CfgParam<std::vector<double>> (p_name, vec, HeldTypes::Vec_d);
                    params_list.push_back(param);
                    break;
                } case HeldTypes::Vec_i : {
                    //Read the size of the vector (count spaces, size is + 1)
                    int v_size = std::count (p_value.begin(), p_value.end(), ' ') + 1;
                    std::stringstream ss (p_value);
                    std::vector<int> vec (v_size);
                    for (unsigned int i = 0; i < vec.size(); i ++ ) {
                        ss >> vec[i];
                    }
                    CfgParam<std::vector<int>> *param =
                    new CfgParam<std::vector<int>> (p_name, vec, HeldTypes::Vec_i);
                    params_list.push_back(param);
                    break;
                } default:
                    return false;
            }
        }
        
    } else {
        return false;
    }
    return true;
}

void ModParam::viewLoaded () {
    int i_index = 0, d_index = 0, vd_index = 0, vi_index = 0;
    if (params_list.size() == 0) {
        std::cout << "No config data loaded" << std::endl;
    } else {
        std::cout << std::endl << "-----List of Parameters stored-----" << std::endl;
        for (auto param : params_list) {
            std::cout << std::endl << "Name : " << param->getN() << std::endl;
            std::cout << "\tValue : ";
            switch (param->getT()) {
                case HeldTypes::Dbl :
                    std::cout << static_cast<CfgParam<double>*>(param)->get();
                    std::cout << " Type : double" << std::endl;
                    std::cout << "\tType Index (use with index based getD()/setD()): " << d_index++
                    << std::endl;
                    break;
                case HeldTypes::Int :
                    std::cout << static_cast<CfgParam<int>*>(param)->get();
                    std::cout << " Type : int"  << std::endl;
                    std::cout << "\tType Index (use with index based getI()/setI()): " << i_index++
                    << std::endl;
                    break;
                case HeldTypes::Vec_d : {
                    std::vector<double> vec = static_cast<CfgParam<std::vector<double>>*>(param)->get();
                    for (auto index : vec) {
                        std::cout << index << " ";
                    }
                    std::cout << "Type : vector<double>" << std::endl;
                    std::cout << "\tType Index (use with index based getVD()/setVD()): " << vd_index++
                    << std::endl;
                    break;
                } case HeldTypes::Vec_i : {
                    std::vector<int> vec = static_cast<CfgParam<std::vector<int>>*>(param)->get();
                    for (auto index : vec) {
                        std::cout << index << " ";
                    }
                    std::cout << "Type : vector<int>" << std::endl;
                    std::cout << "\tType Index (use with index based getVI()/setVI()): " << vi_index++
                    << std::endl;
                    break;
                } default:
                    break;
            }
        }
    }
}

std::string ModParam::trim(const std::string& str,
                           const std::string& whitespace)
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content
    
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;
    
    return str.substr(strBegin, strRange);
}

HeldTypes ModParam::detectType ( const std::string &trimmed ) {
    if ( trimmed.find(" ") != std::string::npos ) {
        //There is a space, must be a vector
        if ( trimmed.find(".") != std::string::npos ) {
            //Vector of doubles
            return HeldTypes::Vec_d;
        } else {
            //Vector of ints
            return HeldTypes::Vec_i;
        }
    } else {
        if ( trimmed.find (".") != std::string::npos ) {
            //There is a period, must be a double
            return HeldTypes::Dbl;
        } else {
            //Integer
            return HeldTypes::Int;
        }
    }
}

//Index Based Getter/Setters

int ModParam::getI( const int index ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Int) {
            if (type_index == index)
                return static_cast<CfgParam<int>*>(param)->get();
            else
                type_index++;
        }
    }
    throw std::out_of_range("getI() index");
}

double ModParam::getD ( const int index ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Dbl) {
            if (type_index == index)
                return static_cast<CfgParam<double>*>(param)->get();
            else
                type_index++;
        }
    }
    throw std::out_of_range("getD() index");
}

std::vector<double> ModParam::getVD ( const int index ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Vec_d) {
            if (type_index == index)
                return static_cast<CfgParam<std::vector<double>>*>(param)->get();
            else
                type_index++;
        }
    }
    throw std::out_of_range("getVD() index");
}

std::vector<int> ModParam::getVI ( const int index ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Vec_i) {
            if (type_index == index)
                return static_cast<CfgParam<std::vector<int>>*>(param)->get();
            else
                type_index++;
        }
    }
    throw std::out_of_range("getVI() index");
}


void ModParam::setI( const int index, const int value ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Int) {
            if (type_index == index) {
                static_cast<CfgParam<int>*>(param)->set( value );
                return;
            } else
                type_index++;
        }
    }
    throw std::out_of_range("setI() index");
}

void ModParam::setD ( const int index, const double value ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Dbl) {
            if (type_index == index) {
                static_cast<CfgParam<double>*>(param)->set( value );
                return;
            } else
                type_index++;
        }
    }
    throw std::out_of_range("setD() index");
}

void ModParam::setVD ( const int index, const std::vector<double> &value ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Vec_d) {
            if (type_index == index) {
                static_cast<CfgParam<std::vector<double>>*>(param)->set( value );
                return;
            } else
                type_index++;
        }
    }
    throw std::out_of_range("setVD() index");
}

void ModParam::setVI ( const int index, const std::vector<int> &value ) {
    int type_index = 0;
    for (auto param : params_list) {
        if (param->getT() == HeldTypes::Vec_i) {
            if (type_index == index) {
                static_cast<CfgParam<std::vector<int>>*>(param)->set( value );
                return;
            } else
                type_index++;
        }
    }
    throw std::out_of_range("setVI() index");
}

//Name Based Getters/Setters

double ModParam::getD(const std::string &var_name) {
    for (auto param: params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Dbl) {
            return static_cast<CfgParam<double>*>(param)->get();
        }
    }
    throw std::invalid_argument("getD() var_name");
}

int ModParam::getI(const std::string &var_name) {
    for (auto param: params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Int) {
            return static_cast<CfgParam<int>*>(param)->get();
        }
    }
    throw std::invalid_argument("getI() var_name");
}

std::vector<double> ModParam::getVD(const std::string &var_name) {
    for (auto param: params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Vec_d) {
            return static_cast<CfgParam<std::vector<double>>*>(param)->get();
        }
    }
    throw std::invalid_argument("getVD() var_name");
}

std::vector<int> ModParam::getVI(const std::string &var_name) {
    for (auto param: params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Vec_i) {
            return static_cast<CfgParam<std::vector<int>>*>(param)->get();
        }
    }
    throw std::invalid_argument("getVI() var_name");
}

void ModParam::setI( const std::string &var_name, const int value ) {
    for (auto param : params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Int) {
            static_cast<CfgParam<int>*>(param)->set( value );
            return;
        }
    }
    throw std::invalid_argument("setI() var_name");
}

void ModParam::setD ( const std::string &var_name, const double value ) {
    for (auto param : params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Dbl) {
            static_cast<CfgParam<double>*>(param)->set( value );
            return;
        }
    }
    throw std::invalid_argument("setD() var_name");
}

void ModParam::setVD ( const std::string &var_name, const std::vector<double> &value ) {
    for (auto param : params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Vec_d) {
            static_cast<CfgParam<std::vector<double>>*>(param)->set( value );
            return;
        }
    }
    throw std::invalid_argument("setVD() var_name");
}

void ModParam::setVI ( const std::string &var_name, const std::vector<int> &value ) {
    for (auto param : params_list) {
        if (param->getN() == var_name && param->getT() == HeldTypes::Vec_i) {
            static_cast<CfgParam<std::vector<int>>*>(param)->set( value );
            return;
        }
    }
    throw std::invalid_argument("setVI() var_name");
}

std::vector<double> ModParam::returnValueList() {
    std::vector<double> return_list;
    for (auto param: params_list) {
        switch (param->getT()) {
            case HeldTypes::Int:
                return_list.push_back((double)static_cast<CfgParam<int>*>(param)->get());
                break;
            case HeldTypes::Dbl :
                return_list.push_back(static_cast<CfgParam<double>*>(param)->get());
                break;
            case HeldTypes::Vec_d :
                for (auto v : static_cast<CfgParam<std::vector<double>>*>(param)->get())
                    return_list.push_back(v);
                break;
            case HeldTypes::Vec_i :
                for (auto v : static_cast<CfgParam<std::vector<int>>*>(param)->get())
                    return_list.push_back((double)v);
                break;
        }
    }
    return return_list;
}
void ModParam::pickOWSensValues(const double step) {
    //For one way analysis; step up corresponds to >1, <1 to down
    for (auto param : params_list) {
        std::cout << param->getN() << std::endl;
        switch (param->getT()) {
            case HeldTypes::Int:
                static_cast<CfgParam<int>*>(param)->set( std::round(static_cast<CfgParam<int>*>(param)->get() * step) );
                break;
            case HeldTypes::Dbl:
                static_cast<CfgParam<double>*>(param)->set( static_cast<CfgParam<double>*>(param)->get() * step );
                break;
            case HeldTypes::Vec_d: {
                std::vector<double> vec = static_cast<CfgParam<std::vector<double>>*>(param)->get(), newV;
                newV.push_back( vec[0] * step );
                for (unsigned int i = 1; i < vec.size(); i++)
                    newV.push_back (vec[i] * newV[0] / vec[0]);
                static_cast<CfgParam<std::vector<double>>*>(param)->set ( newV );
                break;
            } case HeldTypes::Vec_i: {
                std::vector<int> vec = static_cast<CfgParam<std::vector<int>>*>(param)->get(), newV;
                newV.push_back ( std::round (vec[0] * step) );
                for (unsigned int i = 1; i < vec.size(); i++)
                    newV.push_back( std::round ( vec[i] * newV[0] / vec[0] ) );
                static_cast<CfgParam<std::vector<int>>*>(param)->set ( newV );
                break;
            }
                
        }
    } 
}


void ModParam::pickSensValues(const double threshold) {
    //Initializing the gsl_rng generator
    //threshold is in percent (0.25 = 25% above and below)
    gsl_rng *r = gsl_rng_alloc (gsl_rng_taus);
    gsl_rng_set (r, time (NULL));
    //time is in seconds, so if the function is called too quickly it will produce the same results.
    
    for (auto param : params_list) {
        /*if (std::find(prob_list.begin(), prob_list.end(), param->getN()) != prob_list.end()) {
         //prob list : [0, 1]
         //for vector types, set all but the last value, sums to 1
         switch (param->getT()) {
         case HeldTypes::Dbl: {
         double s_val, s_min, s_max, s_new;
         s_val = static_cast<CfgParam<double>*>(param)->get();
         s_min = (s_val - (s_val * threshold));
         s_min = s_min >= 0 ? s_min : 0;
         s_max = (s_val + (s_val * threshold));
         s_max = s_max <  1 ? s_max : 1;
         s_new = s_min + ((s_max - s_min) * gsl_rng_uniform(r));
         static_cast<CfgParam<double>*>(param)->set( s_new );
         break;
         } case HeldTypes::Int: {
         //Should never happen with probabilities
         std::cout << "Integer in the probability list" << std::endl;
         std::cout << "Critical Error" << std::endl;
         break;
         } case HeldTypes::Vec_d: {
         std::vector<double> v_val = static_cast<CfgParam<std::vector<double>>*>(param)->get();
         std::vector<double> v_new (v_val.size());
         double s_min, s_max, s_sum = 0;
         for (unsigned int i = 0; i < (v_val.size() - 1); i ++ ) {
         do {
         s_min = (v_val[i] - (v_val[i] * threshold));
         s_min = s_min >= 0 ? s_min : 0;
         s_max = (v_val[i] + (v_val[i] * threshold));
         s_max = s_max <  1 ? s_max : 1;
         v_new[i] = s_min + ((s_max - s_min) * gsl_rng_uniform(r));
         } while ((s_sum + v_new[i]) >= 1);
         s_sum += v_new[i];
         }
         v_new[v_new.size() - 1] = 1-s_sum;
         static_cast<CfgParam<std::vector<double>>*>(param)->set( v_new );
         break;
         } case HeldTypes::Vec_i: {
         //Should never happen with probabilities
         std::cout << "Vector of integers in the probability list" << std::endl;
         std::cout << "Critical Error" << std::endl;
         break;
         }
         }
         } else if (std::find(real_list.begin(), real_list.end(), param->getN()) != real_list.end()) {*/
        //real list : [0, infinite)
        switch (param->getT()) {
            case HeldTypes::Dbl: {
                double s_val, s_min, s_max, s_new;
                s_val = static_cast<CfgParam<double>*>(param)->get();
                s_min = (s_val - (s_val * threshold));
                s_min = s_min >= 0.0 ? s_min : 0.0;
                s_max = (s_val + (s_val * threshold));
                s_new = s_min + ((s_max - s_min) * gsl_rng_uniform(r));
                static_cast<CfgParam<double>*>(param)->set( s_new );
                break;
                
            } case HeldTypes::Int: {
                int s_val, s_min, s_max, s_new;
                s_val = static_cast<CfgParam<int>*>(param)->get();
                s_min = s_val - std::round(s_val * threshold);
                s_min = s_min >= 0 ? s_min : 0;
                s_max = s_val + std::round(s_val * threshold);
                s_new = std::round( gsl_rng_uniform(r) * (s_max - s_min + 1)) + s_min;
                static_cast<CfgParam<int>*>(param)->set( s_new );
                break;
            } case HeldTypes::Vec_d: {
                std::vector<double> v_val = static_cast<CfgParam<std::vector<double>>*>(param)->get();
                std::vector<double> v_new (v_val.size());
                double s_min, s_max;
                for (unsigned int i = 0; i < v_val.size(); i ++ ) {
                    s_min = (v_val[i] - (v_val[i] * threshold));
                    s_min = s_min >= 0.0 ? s_min : 0.0;
                    s_max = (v_val[i] + (v_val[i] * threshold));
                    v_new[i] = s_min + ((s_max - s_min) * gsl_rng_uniform(r));
                }
                //set value
                static_cast<CfgParam<std::vector<double>>*>(param)->set( v_new );
                break;
            } case HeldTypes::Vec_i: {
                //Vectors of ints in the real list
                //These values shoud be in ascending order. (from smallest to largest)
                std::vector<int> v_val = static_cast<CfgParam<std::vector<int>>*>(param)->get();
                std::vector<int> v_new (v_val.size());
                int s_min, s_max, prev = -1;
                for (unsigned int i = 0; i < v_val.size(); i++) {
                    do {
                        s_min = (v_val[i] - (v_val[i] * threshold));
                        s_min = s_min >= 0 ? s_min : 0;
                        s_max = (v_val[i] + (v_val[i] * threshold));
                        v_new[i] = std::round( gsl_rng_uniform(r) * (s_max - s_min + 1)) + s_min;
                    } while (v_new[i] <= prev);
                    prev = v_new[i];
                }
                static_cast<CfgParam<std::vector<int>>*>(param)->set ( v_new );
                break;
            }
        }
        
        /* } else {
         std::cout << param->getN() << " isn't in either list, CRITICAL ERROR" << std::endl;
         }*/
    }
    gsl_rng_free(r);
}

void ModParam::writeConfig( const std::string &filename, const std::string &text = "" ) {
    std::ofstream file (filename);
    file << "#HIV Model config file" << std::endl;
    
    if (text != "")
        file << "#" << text << std::endl;
    
    for (auto param : params_list) {
        file << std::endl << param->getN() << "=";
        switch (param->getT()) {
            case HeldTypes::Dbl :
                file << static_cast<CfgParam<double>*>(param)->get() << std::endl;
                break;
            case HeldTypes::Int :
                file << static_cast<CfgParam<int>*>(param)->get() << std::endl;
                break;
            case HeldTypes::Vec_d : {
                std::vector<double> vals = static_cast<CfgParam<std::vector<double>>*>(param)->get();
                for (auto val : vals)
                    file << val << " ";
                file << std::endl;
                break;
            } case HeldTypes::Vec_i : {
                std::vector<int> vals = static_cast<CfgParam<std::vector<int>>*>(param)->get();
                for (auto val : vals)
                    file << val << " ";
                file << std::endl;
                break;
            }
        }
    }
}
