//---------------------------------------------------
/* CSA.h
 *
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#ifndef __HIVMODEL_CSA_H
#define __HIVMODEL_CSA_H

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include "Person.h"
#include "ModParam.h"
#include "Stats.h"

using namespace std;

class CSA{
    
private:
    int _id;      //holds the cencus track id (location identifier)

    vector<Person*> _vMembers;
    Stats *stats;
    
    void swap ( CSA &src ) throw (); //Utility function for use with operator=, etc.

public:
    //costructor
    CSA(gsl_rng * rng, ModParam *mp, Stats *s, int id, int population, double raceBlackProp,vector<double> ageProp);//constructor for initial CSA
    CSA( const CSA& src ); //copy constructor
    CSA( CSA&& src ) noexcept; //move constructor
    CSA& operator= (CSA rhs);
    ~CSA();
   
    Person * operator [](int i) const {return _vMembers[i];}
    vector<Person *>::iterator begin()   {     return _vMembers.begin();}
    vector<Person *>::iterator end()   {     return _vMembers.end();}

    inline void setStats(Stats * s){ stats=s;}

    void  addMember(Person * p); //add p as a new member to the end of member list
    void deleteMember(int i);//kills member i (unlink, copy by back, destroy, popout)
    
    
//Initializing
    //void assignGroups(vector<int> vGroups); //assigns Groups id from a Group network (expect it's own id)
    void formInitialStbPartnerships(gsl_rng * rng, double propPopStbPartnerships);//assigns initial stable partnerships

    //Access Functions
    inline int getId(){return _id;}
    inline int getPopSize(){return _vMembers.size();}
    inline Person * getMember(int i ){return _vMembers[i];}
    vector<int> returnDiseaseStateSizes();
    
    //PARTNERSHIP ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    vector<double> returnCSADemogInfo();  //returns a vector containing population size, proportion of blacks, and size of each age-group[8]
   
};


#endif
