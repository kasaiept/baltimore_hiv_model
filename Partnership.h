//---------------------------------------------------
/* Partnership.h
 *

 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------


#ifndef __HIVMODEL_PARTNERSHIP_H
#define __HIVMODEL_PARTNERSHIP_H

class Person;//since person also references the Partnership, we forward-declare Person here first.
/*hen using forward declarations of objects in C++,
You cannot allocate an incomplete type, as the compiler doesn't know how big it is yet. This is solved by only using pointers to incomplete types.
You cannot reference the members of an incomplete type before it's definition is seen by the compiler. This is solved by separating your interface and implementation in to header and code files.
If you are in a situation where you have to use an incomplete type, you're probably doing something wrong. Try to avoid circular references wherever possible.

 That's why wee keep the method definition in the cpp file and can't use a static method design here;
 */

class Partnership {
private:
    Person * _partner;
    int _type;          //0 stable, 1 casual
    int _duration;      //duration of partnership
    int _tStart;
    void swap( Partnership &src ) throw (); //utility function for use with operator=, etc. 
    
public:
    Partnership();
    Partnership(Person * prs, int type, int duration ,int startTime);
    Partnership( const Partnership &src ); //copy constructor
    Partnership( Partnership &&src ) noexcept; //move constructor
    Partnership& operator= ( Partnership rhs );
    ~Partnership();
    void dissolve();
    
    inline Person * getPartner(){return _partner; } inline void setPartner(Person* p) { _partner = p; }
    inline int getType() { return _type; } inline void setType(int type){_type=type;}
    inline int getDuration(){return _duration;}
    inline void setDuration(int t){_duration=t;}
    inline void reduceDuration(){_duration--;}
    inline int getTStart(){return _tStart;}
    inline void setTStart(int t){_tStart=t;}
};

#endif /* defined(__HIVMODEL_PARTNERSHIP_H) */


