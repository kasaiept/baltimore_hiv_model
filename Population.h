//---------------------------------------------------
/* Population.h
 *
 *
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------



#ifndef baltimoreHIV_Population_h
#define baltimoreHIV_Population_h
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include "CSA.h"
#include "Person.h"
#include "ModParam.h"
#include "Stats.h"

using namespace std;
class Population{
    
    
private:
    int _nCSAs;     //number of defined CSAs
    vector<CSA * > _vCSAs;
    ModParam * mp_local;
    
    
    //ModParam variables
    double cTRANS;
    
    //prep parameters
    int nPREP_SCENARIO;
    double pPREP_COVERAGE;
    int nPREP_UNITS;
    int _tPREP_start;//recording the starting year of prep in the model
    double _pPREP_lastCoverage;//last recorded coverage
    
public:
    Population();
    Population(gsl_rng * rng, ModParam * mp, int nCSAs, vector<int> vCSAPop, vector<double> vCSARaceBlackProp, vector<vector<double>> vvCSAAgeProp);
    Population(  Population& src );
    void  copyPartnerships( Population& src);
    void copyStats(Stats * s);
    void copyParamSettings(gsl_rng * rng,ModParam * mp);
    
    ~Population();
    
    Stats *stats;
    CSA * operator [](int i) const {return _vCSAs[i];}
    vector<CSA *>::iterator begin()   {     return _vCSAs.begin();}
    vector<CSA *>::iterator end()   {     return _vCSAs.end();}
    
    inline int getNumCSAs(){return _nCSAs;}
    int getNumPeople(); double getPrpBlack();
    Person * returnPerson(int i);
    

    //Initialize epidemic---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    vector<double>  setEpidemic(gsl_rng * rng);
    void  setInitialDSCS(gsl_rng * rng);
    //NaturalDynamics------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void modelNaturalDynamics(gsl_rng * rng);////models natural dynamic processes among CSAs
    
    //PARTNERSHIP------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void modelPartnerships(gsl_rng * rng,bool modelStb, bool modelCsl, bool modelDissolution);
    
    //HIV NATURAL HISTORY-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void modelDiseaseProgress(gsl_rng * rng);
    bool progressState1(gsl_rng * rng, Person * p);
    bool progressState2(gsl_rng * rng, Person * p);
    bool progressState3(gsl_rng * rng, Person * p);
    bool progressState4(gsl_rng * rng, Person * p);
    bool progressState5(gsl_rng * rng, Person * p);
    
    //TRANSMISSION-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void modelTranmsission(gsl_rng * rng);
    
    //PREP------------------------------------------------------------------------------------------
    void updatePrepParameters(gsl_rng * rng,vector<double> prepInfo);
    void  modelPrEP(gsl_rng * rng,vector<double> prepInfo);//{scenario, duration, coverage, adherence}
    vector<double> prepAdminsitration(gsl_rng * rng,int nScenario, int nUnitis);

    
    //Reporting----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //DEMOGRAPHICS:
    vector<double> returnCSADiseaseDist(int cId);//returns DiseaseDist vector CSA cId {"popSize", "D1","D2","D3","D4","D5","D6"}
    vector<double> returnCSAdisaseDist();//retunr a vector of numInfected for all CSAs
    
    //PARTNERSHIPS-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void resetCumPartnerships();
    
    vector<vector<int>> returnFreqPartAgeGroup_all();
    vector<vector<int>> returnFreqPartAgeGroup_stb();
    //HIV stats-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    vector<double> returnDSsizes();//
    vector<double> returnCSsizes();
    vector<double> returnCSprops();///prop of infected population in each care state at this time { 0 1 2 3}
    vector<vector<double>> returnDSCSprops();
    int returnPrevalence();
    vector<double> returnPrev_ageRace();

    double returnPrEPprops();
    
    //OUTPUTS-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    vector<double> returnPrevInfo(); //returns prev and pro in B
    
    vector<double> returnOutput_DemographicsPop(bool printNames,bool ageDist);//{"popSize", "bProp", "Age1","Age2","Age3","Age4","Age5","Age6","Age7","Age8"}
    vector<vector<double>> returnOutput_DemographicsCSA(bool printNames );//{"CSAId size nB Age1 Age2 Age3 Age4 Age5 Age6 Age7 Age8 Age9 Age10 Age11"}
    
    vector<double> returnOutput_Partnerships(bool printNames,bool reset);

    //annual HIV measures
    vector<double> returnOutputs_HIVmeasures(bool printNames );
    vector<vector<double>> returnOutputs_HIVmeasures_ageStructured(bool printNames);//for two age-strata
    
    vector<double> returnOutputs_PrevDemog(bool printNames);
    
    //
    vector<double> returnSAoutputs();
    
    
    void debug_races();
    
    void debug();
    
};



#endif
