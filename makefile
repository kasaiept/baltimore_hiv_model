CC=g++
DEBUG=-g -Og
OPT=-ftree-vectorize -msse2 -O3#-Ofast
CFLAGS=-c -std=c++0x -I/usr/local/include/gsl -I. #-Wall
LDFLAGS=-L/usr/lib -Wl,--no-as-needed -lgsl -lgslcblas -lm -pthread

all: main.o

debug: main_debug.o

main_debug.o: CSA_D.o Functions_D.o Partnership_D.o Person_D.o Population_D.o Driver_D.o ModParam_D.o Stats_D.o Main_D.o
	$(CC) $(DEBUG) CSA_D.o Functions_D.o Partnership_D.o Person_D.o Population_D.o Driver_D.o ModParam_D.o Stats_D.o Main_D.o -o main_debug.o $(LDFLAGS)

Functions_D.o: Functions.cpp Functions.h
	$(CC) $(CFLAGS) $(DEBUG) Functions.cpp -o Functions_D.o

Partnership_D.o: Partnership.cpp Partnership.h
	$(CC) $(CFLAGS) $(DEBUG) Partnership.cpp -o Partnership_D.o

CSA_D.o: CSA.cpp CSA.h
	$(CC) $(CFLAGS) $(DEBUG) CSA.cpp -o CSA_D.o

Population_D.o: Population.cpp Population.h
	$(CC) $(CFLAGS) $(DEBUG) Population.cpp -o Population_D.o

Person_D.o: Person.cpp Person.h
	$(CC) $(CFLAGS) $(DEBUG) Person.cpp -o Person_D.o

Stats_D.o: Stats.cpp Stats.h
	$(CC) $(CFLAGS) $(DEBUG) Stats.cpp -o Stats_D.o

ModParam_D.o: ModParam.cpp ModParam.h
	$(CC) $(CFLAGS) $(DEBUG) ModParam.cpp -o ModParam_D.o

Driver_D.o: Driver.cpp Driver.h
	$(CC) $(CFLAGS) $(DEBUG) Driver.cpp -o Driver_D.o

Main_D.o: Main.cpp 
	$(CC) $(CFLAGS) $(DEBUG) Main.cpp -o Main_D.o

main.o: CSA.o Functions.o Partnership.o Person.o Population.o Driver.o ModParam.o Stats.o Main.o
	$(CC) CSA.o Functions.o Partnership.o Person.o Population.o Driver.o ModParam.o Stats.o Main.o -o main.o $(LDFLAGS)

Functions.o: Functions.cpp Functions.h
	$(CC) $(CFLAGS) $(OPT) Functions.cpp

Partnership.o: Partnership.cpp Partnership.h
	$(CC) $(CFLAGS) $(OPT) Partnership.cpp

CSA.o: CSA.cpp CSA.h
	$(CC) $(CFLAGS) $(OPT) CSA.cpp

Population.o: Population.cpp Population.h
	$(CC) $(CFLAGS) $(OPT) Population.cpp

Person.o: Person.cpp Person.h
	$(CC) $(CFLAGS) $(OPT) Person.cpp

Stats.o: Stats.cpp Stats.h
	$(CC) $(CFLAGS) $(OPT) Stats.cpp

ModParam.o: ModParam.cpp ModParam.h
	$(CC) $(CFLAGS) $(OPT) ModParam.cpp

Driver.o: Driver.cpp Driver.h
	$(CC) $(CFLAGS) $(OPT) Driver.cpp

Main.o: Main.cpp
	$(CC) $(CFLAGS) $(OPT) Main.cpp

clean:
	rm -rf *.o
