//---------------------------------------------------
/* Stats.h
 *

 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#ifndef __HIVMODEL_STATS_H
#define __HIVMODEL_STATS_H

#include <vector>
#include "Person.h"

using namespace std;

class Stats {
private:
    int TNOW ; //time counter variable (based on weeks)  TNOW shared among all classes
    int LASTID;//unique id counter for population members
    //Debug modes---------
    int DBG_LVL; // 0 - No debug text, 1 - debug text
    bool STAT_MIXING;
    int STAT_DEMOG;
    
    
    
    //CSA MIXING------------------------------------------------------------
    vector<int>  __vCSAMixing_dist;//[x,x,x] records number of succ partnerships formed in each domain
    vector<vector<double>> __vvRaceMixing_dist;//[x,x][x,x]records number of succ partnerships formed between each race
    vector<double> __vADSR;//vector of "age1,age2" in all successful partnerships for further investigarion
    vector<vector<double>>__vvADSR_Race;
   
    //care stats
    //count, REPORT ANNUALLY
    int __nIncHIV; //incidence
    int __nIncHIV_B;//num inc in blacks
    int __nDiagHIV;//number diagnosed with HIV
    int __nDiagAIDS;//number diagnosed wit AIDS
    int __nHIVmortality;
     
    vector<double> __vIncRaceAge;
    //GLOBAL STATS---------------
    //repositoried to record information on people entering that state
    vector<vector<double>> __vvInfo_atInf;//incidence
    vector<vector<double>> __vvInfo_atDiag;//diagnosis
    vector<vector<double>> __vvInfo_atART0;//art
    vector<vector<double>> __vvInfo_atDeath;
    vector<vector<double>> __vvInfo_atTrans;

    //PREP
    vector<vector<double>> __vvPrepStats;//prep stats for each year
    

 
    
public:
    
    Stats(); //Constructor
    Stats(const Stats &src); //Copy constructor
    //Access functions
    int getDBG_LVL() noexcept;
    void setTime( const int ) noexcept;
    int getTime() noexcept;
    void incTime() noexcept;
    int getYear() noexcept;
    int getID() noexcept;
    void incID() noexcept;
    void incAgeAtInf( const int );
    
    
    //CSA MIXING----------------------------------------------------------------------
    bool recordStat_CSAMixing(int c1,int c2);//records CSA mixings of successful partnerships [Person::formCslPartnership()]
    vector<double> returnStat_CSAMixings();
    
    //AGE/RACE MIXING---------------------------------------------------------------
    void recordStat_AgeRaceMixing(int age1,int age2, int race1, int race2);//records age/race mixings for successful partnerships [Person::formCslPartnership()]
    vector<double> returnStat_AgeMixingDiffs();
    vector<vector<double>> returnStat_RaceMixing();
    vector<vector<double>>  returnStat_ADSR_Race();

  
    //DISEASE STATES------------------------------------------------------------
    inline void recordStat_nIncIV(){__nIncHIV++;}    inline int returnStat_nIncHIV(){return __nIncHIV;}
    inline void recordStat_nIncIV_B(){__nIncHIV_B++;}   inline int returnStat_nIncHIV_B(){return __nIncHIV_B;};

    void recordStat_incRaceAge(int race, int ageGroup);
    inline vector<double> returnStat_incRaceAge(){return __vIncRaceAge;}
    //CARE STATES--------------------------------------------------------------------------
    inline void recordStat_nDiagHIV(){__nDiagHIV++;}   inline int returnStat_nDiagHIV(){return __nDiagHIV;}
    inline void recordStat_nDiagAIDS(){__nDiagAIDS++;}    inline int returnStat_nDiagAIDS(){return __nDiagAIDS;}
    inline void recordStat_HIVmortality(){__nHIVmortality++;};    inline int returnStat_nHIVmortality(){ return __nHIVmortality;}


    
    //Global info registeries
    void recordStat_InfoAtInf(Person * p); vector<vector<double>> returnInfo_atInf();
    void recordStat_InfoAtDiag(Person * p); vector<vector<double>> returnInfo_atDiag();
        void recordStat_InfoAtART(Person * p); vector<vector<double>> returnInfo_atART();
        void recordStat_InfoAtDeath(Person * p); vector<vector<double>> returnInfo_atDeath();
    void recordStat_InfoAtTrans(Person * p,Person * q);vector<vector<double>> returnInfo_atTrans();

    
    
    double returnStat_deadNotLinked( );//10
    
    
 

    
    
    //PREP
    void recordStat_PrepInfo(vector<double> vec);
    vector<vector<double>> returnStat_PrepInfo();
     
     vector<double> returnStat_CS_ALL( );
    
    
    void resetAnnualStats();
    void resetAllStats();//resets all population and global stats after the burrn in period
    

    
};
#endif /* defined(__HIVMODEL_STATS_H) */
