//---------------------------------------------------
/* class Stats
 *
* this class has been designed to gather and hold required informations for output generation
 * <linked to popualtion>
 
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#include "Population.h"
#include "Functions.h"
#include "GlobalParams.h"
#include "Person.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include "Stats.h"

using namespace std; //for std::vector
using namespace GLOBALPARAMS;
using namespace FUNCTIONS;


Stats::Stats() : TNOW(0), LASTID(0),  DBG_LVL(0), STAT_MIXING(true), STAT_DEMOG(0) {//pre-set vector
    
    __vCSAMixing_dist = vector<int> (3,0);
    __vvRaceMixing_dist=vector<vector<double>> (2,vector<double>(2,0));//a 2by2 vector
    vector<double> temp;__vvADSR_Race=vector<vector<double>> (2,temp);
    
    __nIncHIV=0;
    __nIncHIV_B=0;
    __nDiagHIV=0;
    __nDiagAIDS=0;
    __nHIVmortality=0;
    
    __vIncRaceAge=vector<double> (vAGE_GROUP_LIMITS.size()*2,0);
    
    
    }

Stats::Stats(const Stats &src)
: TNOW(src.TNOW), LASTID(src.LASTID),  DBG_LVL(src.DBG_LVL),
STAT_MIXING(src.STAT_MIXING), STAT_DEMOG(src.STAT_DEMOG),__nHIVmortality(src.__nHIVmortality) {
    
    //CSA MIXING------------------------------------------------------------
    __vCSAMixing_dist= src. __vCSAMixing_dist;///vector of mixing freq in each group for succ partnerships
    __vADSR = src.__vADSR;
    __vvRaceMixing_dist = src.__vvRaceMixing_dist;
    __vvADSR_Race=src.__vvADSR_Race;
    //DISEASE STATES------------------------------------------------------------
    __nIncHIV=src.__nIncHIV;
    __nIncHIV_B=src.__nIncHIV_B;
    __nDiagHIV=src.__nDiagHIV;
    __nDiagAIDS=src.__nDiagAIDS;
    __vIncRaceAge=src.__vIncRaceAge;
    
    __nHIVmortality=src.__nHIVmortality;
    __vvPrepStats=src.__vvPrepStats;
    
    __vvInfo_atInf=src.__vvInfo_atInf;
    __vvInfo_atART0=src.__vvInfo_atART0;
    __vvInfo_atDeath=src.__vvInfo_atDeath;
    __vvInfo_atDiag=src.__vvInfo_atDiag;
    __vvInfo_atTrans=src.__vvInfo_atTrans;
   }


//Access functions:

int Stats::getDBG_LVL() noexcept {
    return DBG_LVL;
}

void Stats::setTime( const int t ) noexcept {
    TNOW = t;
}

int Stats::getTime() noexcept {
    return TNOW;
}

void Stats::incTime() noexcept {
    TNOW++;
}

int Stats::getYear() noexcept {
    return floor(TNOW/SIMWEEKS);
}

int Stats::getID() noexcept {
    return LASTID;
}

void Stats::incID() noexcept {
    LASTID++;
}

//CSA MIXING--------------------------------------------------------------------

bool Stats::recordStat_CSAMixing(int c1,int c2){//for successful partnership between two people from csa c1 and c2
    if (c1==c2) {
        __vCSAMixing_dist[0]++;
        return true;
    }
    //else: is c2 one of c1's group members?
    vector<int> group= vvGROUP_MEMBERS[c1];
    for (auto c:group){
        if (c2==c ){
            __vCSAMixing_dist[1]++;
            return true;
        }    }
    //else
    __vCSAMixing_dist[2]++;
    return true;
}

vector<double> Stats::returnStat_CSAMixings(){ //returns average { "pCSA0", "pCSA1", "pCSA2",
    int sum= vectorSum(__vCSAMixing_dist);
    return vectorDivision(__vCSAMixing_dist,sum);
}

//Age Race mixing ------------------------------------------------------------------
void Stats::recordStat_AgeRaceMixing(int age1,int age2, int race1, int race2){
    __vvRaceMixing_dist[race1][race2]++;
    __vADSR.push_back(abs(sqrt(age1)   -sqrt(age2)  ));//recording ages to compute ADSR later on
    __vADSR.push_back(abs(sqrt(age1)   -sqrt(age2)  ));//recording ages to compute ADSR later on
    
    //recording ADRS seperately for each race
    __vvADSR_Race[race1].push_back(abs(sqrt(age1)   -sqrt(age2)  ));
    __vvADSR_Race[race2].push_back(abs(sqrt(age1)   -sqrt(age2)  ));
}

vector<double> Stats::returnStat_AgeMixingDiffs(){
    return __vADSR;
}

vector<vector<double>> Stats::returnStat_ADSR_Race(){
    return __vvADSR_Race;
}

vector<vector<double>> Stats::returnStat_RaceMixing(){
    return __vvRaceMixing_dist;
}


void Stats::recordStat_incRaceAge(int race,int ageGroup){
    __vIncRaceAge[race*12+ ageGroup]++;
}

//GLOBAL STATS----------------------------------------------------------
void Stats::recordStat_InfoAtTrans(Person * p,Person * q){//source target
    vector<double> vec; //{"t","p_race","p_age","p_CSA","p_stb","p_csl","p_SA","q_race","q_age","q_CSA","q_stb","q_csl","q_SA"
    vec.push_back(TNOW/52);//0
    vec.push_back(p->getRace());
    vec.push_back(p->getAgeGroup());//2
    vec.push_back(p->getCSAId());
    vec.push_back(p->getNumStbPartCum());//4
    vec.push_back(p->getNumCslPartCum());
    vec.push_back((double) p->getSexualActivityClass());//6

    vec.push_back(q->getRace());
    vec.push_back(q->getAgeGroup());//2
    vec.push_back(q->getCSAId());
    vec.push_back(q->getNumStbPartCum());//4
    vec.push_back(q->getNumCslPartCum());
    vec.push_back((double) q->getSexualActivityClass());//6
   
    __vvInfo_atTrans.push_back(vec);
}
vector<vector<double>> Stats::returnInfo_atTrans(){
    return __vvInfo_atTrans;
}

void Stats::recordStat_InfoAtInf(Person * p){
    vector<double> vec; //{"t","race","age","CSA","stb","csl","SA"
    vec.push_back(TNOW/52);//0
    vec.push_back(p->getRace());
    vec.push_back(p->getAgeGroup());//2
    vec.push_back(p->getCSAId());
    vec.push_back(p->getNumStbPartCum());//4
    vec.push_back(p->getNumCslPartCum());
    vec.push_back((double) p->getSexualActivityClass());//6
  
    __vvInfo_atInf.push_back(vec);
}
vector<vector<double>> Stats::returnInfo_atInf(){
    return __vvInfo_atInf;
}
//------------
void Stats::recordStat_InfoAtDiag(Person * p){
    vector<double> vec; //{"t","race","age","Ds","CS","tSinceInf"
    vec.push_back(TNOW/52);
    vec.push_back(p->getRace());
    vec.push_back(p->getAge());
    vec.push_back(p->getDiseaseState());
    vec.push_back(p->getCareState());
    vec.push_back(TNOW - p->getEn(1));
    
    __vvInfo_atDiag.push_back(vec);
}
vector<vector<double>> Stats::returnInfo_atDiag(){
    return __vvInfo_atDiag;
}
//------------
void Stats::recordStat_InfoAtART(Person * p){
    vector<double> vec; //{"t","race","age","Ds","CS","tSinceInf","tSinceDiag"
    vec.push_back(TNOW/52);
    vec.push_back(p->getRace());
    vec.push_back(p->getAge());
    vec.push_back(p->getDiseaseState());
    vec.push_back(p->getCareState());
    vec.push_back(TNOW - p->getEn(1));
    vec.push_back(TNOW - p->getTDiagnosis());
    
    __vvInfo_atART0.push_back(vec);
}
vector<vector<double>> Stats::returnInfo_atART(){
    return __vvInfo_atART0;
}
//------------
void Stats::recordStat_InfoAtDeath(Person *p){
    vector<double> vec; //{"t","race","age","CSA","Ds","CS","tSinceInf","tSinceDiag","nTrans"
    vec.push_back(TNOW/52);//0
    vec.push_back(p->getRace());
    vec.push_back(p->getAge());
    vec.push_back(p->getCSAId());
    vec.push_back(p->getDiseaseState());
    vec.push_back(p->getCareState());
    vec.push_back(TNOW - p->getEn(1));
    vec.push_back(TNOW - p->getTDiagnosis());
    vec.push_back(p->getNumTrans());
     __vvInfo_atDeath.push_back(vec);
}
vector<vector<double>> Stats::returnInfo_atDeath(){
    return __vvInfo_atDeath;
}

//PREP------------------
void Stats::recordStat_PrepInfo(vector<double> vec){//{totalEligibles, onPrep, toEnrol }
    __vvPrepStats.push_back( vec);
}

vector<vector<double>> Stats::returnStat_PrepInfo(){
    vector<vector<double>> temp=__vvPrepStats;
    if (temp.size()<1) temp= {{0}};
    return temp;
}

void Stats:: resetAnnualStats(){
    __nIncHIV=0;
    __nIncHIV_B=0;
    __nDiagHIV=0;
    __nDiagAIDS=0;
    __nHIVmortality=0;
    vectorReset(__vIncRaceAge);

 }

void Stats::resetAllStats(){
    vectorReset(__vCSAMixing_dist);
    vectorReset(__vvRaceMixing_dist);
    __vADSR.clear();
    __vvADSR_Race[0].clear();
    __vvADSR_Race[1].clear();
     
    __nIncHIV=0;
    __nIncHIV_B=0;
    __nDiagHIV=0;
    __nDiagAIDS=0;
    __nHIVmortality=0;
    vectorReset(__vIncRaceAge);
    
   __vvInfo_atART0.clear();
    __vvInfo_atDeath.clear();
    __vvInfo_atDiag.clear();
    __vvInfo_atInf.clear(); 
    __vvInfo_atTrans.clear();
    
    __vvPrepStats.clear();
    

}

