//---------------------------------------------------
/* class CSA
 *
 *this class contains attributes and fucntions for managing the CSA class
 *we have defines the structure of a population CSA as a simple, unweighed and undirected graph.

 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------


#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include "CSA.h"
#include "Person.h"
#include "Functions.h"
#include "ModParam.h"
#include "Stats.h"
using namespace std;
using namespace FUNCTIONS;

CSA::CSA(gsl_rng * rng,ModParam *mp, Stats *s, int id, int population, double raceBlackProp,vector<double> ageProp)
: _id(id)
{
    stats = s;
    if (stats->getDBG_LVL())
        cout << 'o';
    //populate the arrays
    for (int i=0;i<population;i++){
        int age      =  assignAge0(rng,ageProp);
        int race     =  assignRace0(rng,raceBlackProp);
    
        //create a susceptible person
        Person * p=new Person(rng, mp, stats, id, age,race, 0);
        _vMembers.push_back( p );
    }
}

CSA::CSA( const CSA& src )
: _id(src._id)
{
    stats = src.stats;
    for (auto p : src._vMembers) {
        _vMembers.push_back ( new Person (*p) );
    }
}

CSA& CSA::operator= ( CSA rhs ) {
    rhs.swap( *this );
    return *this;
}

void CSA::swap ( CSA &src) throw () {
    std::swap( this->_id, src._id );
    std::swap( this->_vMembers, src._vMembers );
    std::swap( this->stats, src.stats );
}

CSA::CSA ( CSA&& src ) noexcept
: _id(0)
{
    _vMembers.clear();
    (*this) = std::move(src);
}

CSA::~CSA(){
    //delete all memebers of the population//delete p;
    for ( auto it : _vMembers ) {
        if ( it == nullptr ) {
            if (stats->getDBG_LVL())
                cout << "Person Already Deleted" << endl;
        } else
            delete (it);
    }
}

void CSA::addMember(Person * p){
    _vMembers.push_back(p);
}

void CSA::deleteMember(int i){
    delete( _vMembers[i]);
    _vMembers.erase(_vMembers.begin()+i);
}

vector<int> CSA::returnDiseaseStateSizes(){
    vector<int> DD(6,0);
    for(auto p: _vMembers){
        DD[p->getDiseaseState()]++;
    }
    return DD;
}


void CSA::formInitialStbPartnerships(gsl_rng * rng,double propPopStbPartnerships0){//explecitly forms initial stable partnerships in a CSA
}




