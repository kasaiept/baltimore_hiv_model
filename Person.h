//---------------------------------------------------
/* Person.h
 *
 
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#ifndef __HIVMODEL_PERSON_H
#define __HIVMODEL_PERSON_H

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "Partnership.h"
#include "ModParam.h"
#include "Stats.h"
#include <vector>

using namespace  std;



class Person {
private:
    int _id;    //unique identifier  
    int _CSAId;
    int _age;   //age in years
    int _race;  //0 for W; 1 for B;
    int _ageGroup;
    int _tBorn;
    bool _markedDead;//true if dying
    
    //Partnerships
    vector<Partnership*> _vPartnerships;//vector of active partnerships
    int _nStb;            //num stable partnerships at each time//8
    int _nCsl;
    int _nStbCum;   //cumulative num stable partnerships
    int _nCslCum;
    bool _markedForPartnership; //set to true if the person is marked for partnership (partnership formation loop)
    int _saClass;//sexual activity class (0,1,2)
    double _cSexualActivity;//personal coefficient of sociability as afunction of social class, people born with it and it stays fixed through life (0-100)
    double _cPartBasedAge;//relative coefficient of partnership based on age //between 0-1//updates with age and based on socialtendecy each year  //14
    
    
    
    //Disease
    vector<int> _vRd;//vector of remaining duration of stay in each disease state (1 to 4)
    vector<int> _vEn;//vector of entry times to each health state (0 to 5)
    int _diseaseState;     //disease state 0(Sus),  1(acute),   2(chronic), 3(AIDS), 4(partiallySupp),  5(fullSupp) //15
    int _lastDiseaseState;//last Disease state
    int _careState;//care states 0(unaware),    1(diagnosed/notLinked), 2(OnART),   3(OffART)
    bool _bGotAIDS;//true if ever got AIDS
    double _pMortality;//p hiv mortality depending on the state
    //diagnosis
    int _tDiagnosis;//time of diagnosis
    int _dsDiagnosis;//disease state at diagnosis
    //ART
    int _tART_begin;
    int _tART_end;
    int _nART_trials;
    //reinitiate
    double _rdLose_adh_ART;//time to lose adh to ART
   //transmission
    double _immunity;
    double _infectiousness;
    int _nTransmission; //number of hiv trans to others //35
    bool _markedForTrans;
    //care
    double _pAccessCare;//prob of access to care

    //prep
    bool _bOnPrep;//true if on PrEP
    double _pPrepAdh;//adherence to prep
    double _pPrepDropout;//liklihood of dropout from prep
    
   

    
    //ModParam config values----------
    double pENGAGE_CSL;//prob of engaging in a csl part. if there is no limit
    double cENGAGE_CSL_BASED_STB1;//coef lowring probCsl based on current stb partnership
    double pENGAGE_STB;
    vector<double> vPROP_CSA_MIXING;
    //New config values
    double cAGE_DIFF;
    vector<double> vRACE_MIXING_COEF;
    vector<double> vSEXUALACTIVITY_COEF;
    vector<double> vPART_AGE_COEF;
    //
    vector<int> vD1;
    vector<int> vD2;
    vector<int> vD3;
    vector<int> vD4;
    double pHIV_MORTALITY;
    double pHIV_MORTALITY_ART;
    vector<double> vVL;
    vector<int> vCHRONIC_DURATION_AFTER_ART;
    
    
    double pTESTING;
    double pLINK_CARE_FIRST;
    double pLINK_CARE_WEEKLY;
    double pINITIATE_ART;
    double pREINITIATE_ART;
    
    vector<double> vACCESS_CARE_COEF;
    double nGAP_IN_CARE;
    vector<double> vLOSE_ADH_ART_PROB;
    double cLOSE_ADH_ART_COEF;

        
    Stats *stats;
    void _initConfigValues( ModParam * mp );
    void _initParams( gsl_rng * rng );
    void _initVariables( gsl_rng * rng);
    void swap ( Person &src ) throw (); //utility function for use with operator=, etc.
    
public:
    Person();
    Person(int CSAID, ModParam * mp, Stats *s);
    Person( gsl_rng * rng, ModParam * mp, Stats *s, int CSAID, int age, int race,int HIVState);
    Person( const Person& src ); //copy constructor
    Person (Person&& src) noexcept; //move constructor
    Person& operator= ( Person rhs );
    ~Person();
    void copyParamSettings(gsl_rng * rng,ModParam * mp);

    //PERSONAL ------------------------------------------------------
    inline int getId(){return _id;} inline void setId(int id){ _id=id;}
    inline int getCSAId(){return _CSAId;}
    inline int getAge(){return _age;}; inline void addAge(){_age++;}
    inline int getRace(){return _race;};
    inline int getAgeGroup(){return _ageGroup;}
    
    bool willDieNaturally(gsl_rng * rng);//retunrs true if person is dying from natural mortality causes or old age >55
    void unlink();
    void kill();
    void reborn(gsl_rng * rng,ModParam *mp);
    void reborn(gsl_rng * rng, int age,int race,int lifeExpc,int HIVState);
    
    inline void setStats(Stats * s){ stats=s;}
    
    
    //MIXING //PARTNERSHIP------------------------------------------------------
    int returnPartnershipCSAId(gsl_rng * rng);
    
    Partnership * operator [](int i) const {return _vPartnerships[i];}
    vector<Partnership *>::iterator begin()   {     return _vPartnerships.begin();}
    vector<Partnership *>::iterator end()   {     return _vPartnerships.end();}
    
    void setInitialAgeGroup();//sets individual's ageGroup based on their current age
    void updateAgeGroup(); //updates ageGroup based on current age and ageGroup
    void updatePartAgeCoef();
    inline double getSexualActivityCoef(){return _cSexualActivity;}
    inline int getSexualActivityClass(){return _saClass;}
    
    inline int getNumCslPart(){return _nCsl;}                               inline void setNumCslPart(int n){_nCsl=n;}
    inline int getNumStbPart(){return _nStb;}                               inline void setNumStbPart(int n){_nStb=n;}
    inline int getNumCslPartCum(){return _nCslCum;}                         inline void setNumCslPartCum(int n){_nCslCum=n;}
    inline int getNumStbPartCum(){return _nStbCum;}                         inline void setNumStbPartCum(int n){_nStbCum=n;}
    inline bool isMarkedForPartnership(){return _markedForPartnership;}     inline void setMarkedForPartnership(bool b){_markedForPartnership=b;}
    inline int getNumPartnerships(){ return _vPartnerships.size();}
    inline Person * getPartner(int index){ return _vPartnerships[index]->getPartner();}
    
    
    inline int getPartneshipDuration(int idx){return _vPartnerships[idx]->getDuration();}
    inline void setPartneshipDuration(int idx, int d){ _vPartnerships[idx]->setDuration(d);}
    inline int getPartneshipType(int idx){return _vPartnerships[idx]->getType();}
    inline void setPartneshipType(int idx, int i){ _vPartnerships[idx]->setType(i);}
    inline int getPartneshipStartTime(int idx){return _vPartnerships[idx]->getTStart();}
    inline void setPartneshipStartTime(int idx, int i){ _vPartnerships[idx]->setTStart(i);}
    inline void reducePartneshipDuration(int idx){_vPartnerships[idx]->reduceDuration();}
    int findPartnerIndex(Person * p);
    
    bool evaluateRaceAgeMixing(gsl_rng * rng,Person * q, int cAGE_DIFF);
    bool isAvailableCsl(gsl_rng * rng);//returns true if the person is available and interested in forming a csl partnership
    bool isAvailableStb(gsl_rng * rng);//returns true if the person is available and interested in forming a stb partnership
    bool evaluateCslPartnership(gsl_rng * rng, Person * q);
    bool evaluateStbPartnership(gsl_rng * rng, Person * q);
    void formCslPartnership(gsl_rng * rng, Person * p);
    void formStbPartnership(gsl_rng * rng, Person * p);
    void dissolvePartnershipMutually(int index);//dissolves the partnership for both people (it's enough to call if for just one person)
    void removeMyPartnership(int index);//removes the specified partnership from my list and deletes it
    void addPartnership(Partnership * r) ;//add this partnerhip to the last of my list
    
    
    
    //HIV NATURAL HISTORY-----------------------------------------------------
    inline int getDiseaseState(){return _diseaseState;}         inline void setDiseaseState(int num){_diseaseState=num;}
    inline int getLastDiseaseState(){return _lastDiseaseState;}         inline void setLastDiseaseState(int num){_lastDiseaseState=num;}
    inline int getCareState(){return _careState;}               inline void setCareState(int num){_careState=num;}
    inline bool isMarkedDead(){return _markedDead;}             inline void setMarkedDead(bool b){_markedDead=b;}
    inline bool getGotAIDS(){return _bGotAIDS;}                inline void setGotAIDS(bool b){_bGotAIDS=b;}
    inline int getRd(int i){ return _vRd[i];}                   inline void reduceRd(int i) { _vRd[i]--;}
    inline int getEn(int i){ return _vEn[i];}                   inline void setEn(int i, int n){_vEn[i]=n;}
    inline double getVolnurability(){return _pAccessCare;}   inline void setVolnurability(double d){_pAccessCare=d;}
    inline bool isMarkedForTrans(){return _markedForTrans;} inline void setMarkedForTrans(bool b){_markedForTrans=b;}
    
    inline int getTDiagnosis(){return _tDiagnosis;}
    inline int getDsDiagnosis(){return _dsDiagnosis;}
    inline int getARTBeginTime(){return _tART_begin;}
    inline int getARTEndTime(){return _tART_end;}
    inline int getARTTrials(){return _nART_trials;}
    //DS
    void enterDS0();
    void enterDS1(gsl_rng * rng );
    void enterDS2(gsl_rng * rng,bool resetDuration);
    void enterDS3(gsl_rng * rng,bool resetDuration);
    void enterDS4(gsl_rng * rng );
    void enterDS5(gsl_rng * rng );
    void exitDS();
    //CS
    bool willTestForHIV(gsl_rng * rng);
    bool willLinkToCare(gsl_rng * rng,bool firstTry);//linking to care for diagnosed individuals //returns true if changes the Disease Sate
    bool willGoOnART(gsl_rng * rng);
    bool willLoseAdhART(gsl_rng * rng); //going off ART
    bool willReinitiateART(gsl_rng * rng); // going back on ART
    bool willDieHIV(gsl_rng * rng);//true if dye of HIV mortatlity
    bool willDSProgress(gsl_rng * rng);
    
    double assignDurationOnART(gsl_rng * rng);
    //TRANSMISSION
    void assign_infectiousness(gsl_rng * rng);
    inline double getInfectiousness(){return _infectiousness;}      inline void setInfectiousness(double num){_infectiousness=num;}
    inline double getImmunity(){return _immunity;}      inline void setImmunity(double num){_immunity=num;}
    inline int getNumTrans(){return _nTransmission;}   inline void addNumTrans(){_nTransmission++;}
    
    
    //Prep
    void goOffPrep();
    void goOnPrep();
    inline bool isOnPrep(){return _bOnPrep;}
    void  assign_prepParams(gsl_rng * rng, double ADH, double DROPPUT);
    bool isPrepEligible(int scenario);
    bool willDropoutPrep(gsl_rng * rng);
    
    
    
};

#endif
