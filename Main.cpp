//----
//---------------------------------------------------
//
/* class Main
 *
 *this class calls the main functions for creating of baseline simulations, PrEP scenarios, and sensitivity analysis.
 *
 *Parastu Kasaie, Feb 2016
 *
 */

//
//-----------------------------------------------------------------
#include "GlobalParams.h"
#include "Functions.h"
#include "Population.h"
#include "ModParam.h"
#include "Driver.h"
#include "Stats.h"
#include <iostream>
#include <sstream>
#include <thread>
#include <mutex>

using namespace std;
using namespace GLOBALPARAMS;
using namespace FUNCTIONS;

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}
//CALIBRATION-----------------------//-----------------------//-----------------------//-----------------------//-----------------------//-----------------------//-----------------------
void single_calibrateEpidemic(ModParam * mp){//Runs a single replication
    vector<string> colNames={"year nPop nInc nPrev nDiagHIV nDiagAIDS nMortHIV pEverGotAIDS pDS1 pDS2 pDS3 pDS4 pDS5  pDiag_inf_inst pLinked_Diag_inst pOnART_Linked_inst pSupp_onART_inst pOnART_cum popWhite prevHIVWhite propPrevByWhite pDS123_W pDS4_W pDS5_W pDiag_W pOnART_W pSupp_W popBlack prevHIVBlacks propPrevByBlack pDS123_B pDS4_B pDS5_B pDiag_B pOnART_B pSupp_B prep1 prep2 prep3 prep4 prep5"};//TALLY: cumulative through last year TS: at the end of each week through last year (averaged)
    writeVector(colNames,"calibration_outputs0_sing",true);
    //PREV by AGE RACE
    colNames={"PrevW1","PrevW2","PrevW3","PrevW4","PrevW5","PrevW6","PrevW7","PrevW8","PrevW9","PrevW10","PrevW11","PrevW12",
        "PrevB1","PrevB2","PrevB3","PrevB4","PrevB5","PrevB6","PrevB7","PrevB8","PrevB9","PrevB10","PrevB11","PrevB12"};
    writeVector(colNames,"calibration_outputs1_sing",true);
    //INC by AGE RACE
    colNames={"IncW1","IncW2","IncW3","IncW4","IncW5","IncW6","IncW7","IncW8","IncW9","IncW10","IncW11","IncW12",
        "IncB1","IncB2","IncB3","IncB4","IncB5","IncB6","IncB7","IncB8","IncB9","IncB10","IncB11","IncB12"};
    writeVector(colNames,"calibration_outputs2_sing",true);
    //PArtnerships
    colNames={"year freq0 freq1 freq2 freq3 freq4 freq5+ pStbOnlyL12M pCslOnlyL12M pStbCslL12M pStbNow"};
    writeVector(colNames,"calibration_outputs3_sing",true);
    
    vector<vector<double>> temp;
    vector<vector<vector<double>>> vvvRes( 3 ,temp);
    
    Driver * d=new Driver(0, mp);
    d->burnin(50,50,100);
    vector<vector<double>> vec= d->calibrateModel_summaryOutputs(50);
    vvvRes[0].push_back(vec[0]);
    vvvRes[1].push_back(vec[1]);
    vvvRes[2].push_back(vec[2]);
    vvvRes[3].push_back(vec[3]);
    //write
    writeVector(vvvRes[0], "calibration_outputs0_sing", false);//all
    writeVector(vvvRes[1], "calibration_outputs1_sing", false);//prev
    writeVector(vvvRes[2], "calibration_outputs2_sing", false);//inc
    writeVector(vvvRes[3], "calibration_outputs3_sing", false);//partnerships
    
}
void thread_calibrateEpidemic (int tID, ModParam *mp,std::mutex &simLock){
    Driver * d=new Driver(tID, mp);
    d->burnin(50,50,150);
    vector<vector<double>> vec= d->calibrateModel_summaryOutputs(50);
    //write
    simLock.lock();
    writeVector(vec[0], "calibration_outputs0_reps", false);
    writeVector(vec[1], "calibration_outputs1_reps", false);
    writeVector(vec[2], "calibration_outputs2_reps", false);
    writeVector(vec[3], "calibration_outputs3_reps", false);
    
    
    simLock.unlock();
    delete (d);
}
void replicate_calibrateEpidemic(ModParam * mp, int numModels){//RUNS MULTIPLE REPS OF PREP
    vector<string> colNames={"year nPop nInc nPrev nDiagHIV nDiagAIDS nMortHIV pEverGotAIDS pDS1 pDS2 pDS3 pDS4 pDS5  pDiag_inf_inst pLinked_Diag_inst pOnART_Linked_inst pSupp_onART_inst pOnART_cum popWhite prevHIVWhite propPrevByWhite pDS123_W pDS4_W pDS5_W pDiag_W pOnART_W pSupp_W popBlack prevHIVBlacks propPrevByBlack pDS123_B pDS4_B pDS5_B pDiag_B pOnART_B pSupp_B prep1 prep2 prep3 prep4 prep5"};//TALLY: cumulative through last year TS: at the end of each week through last year (averaged)
    writeVector(colNames,"calibration_outputs0_reps",true);
    //PREV by AGE RACE
    colNames={"PrevW1","PrevW2","PrevW3","PrevW4","PrevW5","PrevW6","PrevW7","PrevW8","PrevW9","PrevW10","PrevW11","PrevW12",
        "PrevB1","PrevB2","PrevB3","PrevB4","PrevB5","PrevB6","PrevB7","PrevB8","PrevB9","PrevB10","PrevB11","PrevB12"};
    writeVector(colNames,"calibration_outputs1_reps",true);
    //INC by AGE RACE
    colNames={"IncW1","IncW2","IncW3","IncW4","IncW5","IncW6","IncW7","IncW8","IncW9","IncW10","IncW11","IncW12",
        "IncB1","IncB2","IncB3","IncB4","IncB5","IncB6","IncB7","IncB8","IncB9","IncB10","IncB11","IncB12"};
    writeVector(colNames,"calibration_outputs2_reps",true);
    //PArtnerships
    colNames={"year freq0 freq1 freq2 freq3 freq4 freq5+ pStbOnlyL12M pCslOnlyL12M pStbCslL12M pStbNow"};
    writeVector(colNames,"calibration_outputs3_reps",true);
    
    
    
    vector<std::thread> mdl_threads;
    mutex simLock;
    for (int m=0;m<numModels;m++){
        mdl_threads.push_back ( std::thread( thread_calibrateEpidemic, m, mp, std::ref(simLock) ) );//spawn new thread that calls model_thread(...rest of params...)
    }
    std::for_each(mdl_threads.begin(),mdl_threads.end(),std::mem_fn(&std::thread::join)); //for loop
    
}
//========================================================================================================================================================
//<<SA_ONEWAY>>==== Take parmeters one at a time. If single value, vary to 0.75/1.25 * original value. If vector, use element 0 as index,and vary other elements relative to that
//========================================================================================================================================================
vector<ModParam *> generateMPs_SAoneway(string configname){//first line is the baseline mp, then take params one at a time and change to +/- side
    //copy the original mp file into a new local one
    //uses the seedConfig file
    ModParam * mp0=new ModParam(configname);//initiate  a new modparam
    // mp0->viewLoaded();
    //0-BASELINE
    vector<ModParam *> vMPs;
    vMPs.push_back(mp0);
      //2-INT VECTOR VALUE PARAMETERS (ALL VECTOR MUTAITON)
    /* //<<Vectors>> we choose the first element as the reference value, change it and then change all the other elements relative to that one.
     vector<string> vParamIntVec= {"vD1","vD2","vD3","vD4","vCHRONIC_DURATION_AFTER_ART"};//5
     for (string name:vParamIntVec){
     vector<int> vVal=mp0->getVI(name);
     //
     vector<vector<int>> vvSA=vectorIncrementRelative_int(vVal, 0, .25);
     for (vector<int> v:vvSA){
     ModParam * mp1=new ModParam(*mp0);//copy from mp0
     mp1->setVI(name, v);
     vMPs.push_back(mp1);
     }
     }
     //3-DOUBLE VECTOR VALUE PARAMETERS (ALL VECTOR MUTAITON)
     vector<string> vParamDoubleVec= {"vVL_COEF","vSEXUALACTIVITY_COEF","vPART_AGE_COEF","vPROP_CSA_MIXING_PROP","vACCESS_CARE_COEF","vLOSE_ADH_ART_PROB","vRACE_MIXING_COEF"};//7
     for (string name:vParamDoubleVec){
     vector<double> vVal=mp0->getVD(name);
     //
     vector<vector<double>> vvSA=vectorIncrementRelative_double(vVal, 0, .25);
     for (vector<double> v:vvSA){
     ModParam * mp1=new ModParam(*mp0);//copy from mp0
     mp1->setVD(name, v);
     vMPs.push_back(mp1);
     }
     }*/
    //1-<<SINGLE VALUE PARAMETERS
    vector<string> vParamSingles= {"pENGAGE_STB","pENGAGE_CSL","cENGAGE_CSL_BASED_STB1","cAGE_DIFF","cTRANS","pHIV_MORTALITY_YEARLY","pHIV_MORTALITY_ART_COEF","pTESTING_YEARLY","pLINK_CARE_FIRST","pLINK_CARE_YEARLY","pINITIATE_ART","pREINITIATE_ART_YEARLY","nGAP_IN_CARE","cLOSE_ADH_ART_COEF"}; //14
    for (string name:vParamSingles){
        double val=mp0->getD(name);
        //
        ModParam * mp2=new ModParam(*mp0);
        mp2->setD(name, 1.25* val);
        vMPs.push_back(mp2);
        //
        ModParam * mp1=new ModParam(*mp0);//copy from mp0
        mp1->setD(name, 0.75* val);
        vMPs.push_back(mp1);
    }

    //#changing vector values one at a time
    //4-INT VECTOR VALUE PARAMETERS (SINGLE MUTATION)
    vector<string> vParamIntVec= {"vD1","vD2","vD3","vD4","vCHRONIC_DURATION_AFTER_ART"};//5
    for (string name:vParamIntVec){
        vector<int> vVal=mp0->getVI(name);
        //
        vector<vector<int>> vvSA=vectorChangeElements_OneByOne(vVal, .25); //change the element but keep the differnce to1 at minimum
        for (vector<int> v:vvSA){
            ModParam * mp1=new ModParam(*mp0);//copy from mp0
            mp1->setVI(name, v);
            vMPs.push_back(mp1);
        }
    }
    //5-DOUBLE VECTOR VALUE PARAMETERS (SINGLE MUTATION)
    vector<string> vParamDoubleVec= {"vVL_COEF","vSEXUALACTIVITY_COEF","vPART_AGE_COEF","vPROP_CSA_MIXING_PROP","vACCESS_CARE_COEF","vLOSE_ADH_ART_PROB","vRACE_MIXING_COEF"};//7
    for (string name:vParamDoubleVec){
        vector<double> vVal=mp0->getVD(name);
        //
        vector<vector<double>> vvSA=vectorChangeElements_OneByOne(vVal, .25);
        for (vector<double> v:vvSA){
            ModParam * mp1=new ModParam(*mp0);//copy from mp0
            mp1->setVD(name, v);
            vMPs.push_back(mp1);
        }
    }
    return vMPs;
}

void single_SAoneway(vector<ModParam *> vMP,vector<double> prepInfo, vector<string> colnames){
    //output file
    stringstream s; s<<"SA-M0-single-"<<currentDateTime(); string filename=s.str();
    //vector<double> vNames; for (int i=0;i<vMP.size();i++) vNames.push_back(i);
    writeVector(colnames,filename,true);
    //
    ModParam *mp=vMP[0];//baseline
    Driver * d=new Driver(0, mp);
    d->burnin(50,50,100);
    //<<SA>>
    vector<vector<double>> vvRes = d->SA_oneway(25,25,vMP,prepInfo);
    //I lock the simlock here so that only onethread can access the output file at each point of time
    writeVector(vvRes,filename, false);
    
    delete (d);
    
}
void thread_SAonway (int tID,string filename, vector<ModParam *> vMP,vector<double> prepInfo,std::mutex &simLock){
    ModParam *mp=vMP[0];//baseline
    Driver * d=new Driver(tID, mp);
    d->burnin(50,50,100);
    //<<SA>>
    vector<vector<double>> vvRes = d->SA_oneway(25,25,vMP,prepInfo);
    //I lock the simlock here so that only onethread can access the output file at each point of time
    simLock.lock();
    writeVector(vvRes, filename, false);
    simLock.unlock();
    delete (d);
}
void replicate_SAoneway(int reps,vector<ModParam *> vMP,vector<double> prepInfo,vector<string> colnames){//RUNS MULTIPLE REPS OF PREP
    //output file
    stringstream s; s<<"SA-M0-reps-"<<currentDateTime(); string filename=s.str();
    //vector<double> vNames; for (int i=0;i<vMP.size();i++) vNames.push_back(i);
    writeVector(colnames,filename,true);
    //
    vector<std::thread> mdl_threads;
    mutex simLock;
    for (int r=0;r<reps;r++){
        mdl_threads.push_back ( std::thread( thread_SAonway, r,filename, vMP,prepInfo, std::ref(simLock)) );//spawn new thread that calls model_thread(...rest of params...)
    }
    std::for_each(mdl_threads.begin(),mdl_threads.end(),std::mem_fn(&std::thread::join)); //for loop
}


//========================================================================================================================================================
//<<PREP>>====
//========================================================================================================================================================
void single_prep(ModParam * mp,vector<double> vScenarios,vector<double> vCov,vector<double> vAdh,vector<double> vDuration,vector<double> vDropout){//Runs a single replication
    vector<double> temp={};
    stringstream s; s<<"output-M0-single-"<<currentDateTime(); string filename=s.str();
    writeVector(temp,filename,true);
    
    vector<vector<vector<double>>> vvRes;
    Driver * d=new Driver(0, mp);
    d->burnin(50,50,100);
    vvRes.push_back( d->runPrepScenarios(vScenarios,vCov,vAdh,vDuration,vDropout));
    //write
    writeVector(vvRes, filename, false);
}
void thread_prep (int tID, string outputname, ModParam *mp,std::mutex &simLock,vector<double> vScenarios,vector<double> vCov,vector<double> vAdh,vector<double> vDuration,vector<double> vDropout){
    vector<vector<double>> vvRes;
    Driver * d=new Driver(tID, mp);
    d->burnin(50,50,100);
    vvRes=d->runPrepScenarios(vScenarios,vCov,vAdh,vDuration,vDropout);
    cout<<tID<< " finished";
    //write
    simLock.lock();
    writeVector(vvRes, outputname, false);
    simLock.unlock();
    delete (d);
}
void replicate_prep(ModParam * mp, int numModels,vector<double> vScenarios,vector<double> vCov,vector<double> vAdh,vector<double> vDuration,vector<double> vDropout){//RUNS MULTIPLE REPS OF PREP
    vector<double> temp={};
    stringstream s; s<<"output-M0-reps-"<<currentDateTime(); string filename=s.str();
    writeVector(temp,filename,true);
    
    vector<std::thread> mdl_threads;
    mutex simLock;
    for (int m=0;m<numModels;m++){
        mdl_threads.push_back ( std::thread( thread_prep, m, filename, mp, std::ref(simLock),vScenarios,vCov,vAdh,vDuration,vDropout) );//spawn new thread that calls model_thread(...rest of params...)
    }
    std::for_each(mdl_threads.begin(),mdl_threads.end(),std::mem_fn(&std::thread::join)); //for loop
}

//
//========================================================================================================================================================
//========================================================================================================================================================
//========================================================================================================================================================

//CALIBRATED MODEL
int main() {
   
    //<< ONEWAY SA>>
    //generate a vector of those params that we wanna do the SA for
    //fee to sa-method and run
//    vector<int> counts={2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	4,	4,	4,	4,	4,	12,	6,	10,	4,	4,	6,	6,};
    vector<string> colNames={
        "pEngage_Casual",  "pEngage_Stable",  "cEngage_Casual_Based_Stable",	"cAge-Diff",	"cTransmission",	"pHIV_Mortality",	"pHIV_Mortality_ART_Coef",	"pTesting",	"pLinkCare_Immediately",	"pLinkCare_later",	"pStart_ART_Immediately",	"pRestart_ART",	"dGap_In_Care",	"cLose_Adh_ART",
        "dAcute_L","dAcute_H",	"dChronic_L","dChronic_H",	"cAIDS_L","cAIDS_H",	"dPartialSupp_L","dPartialSupp_H",	"dChronic_after_ART_L","dChronic_after_ART_H",
        "cVL1","cVL2","cVL3","cVL4","cVL5","cVL6",
        "cSexualActivity1","cSexualActivity2","cSexualActivity3",
        "cPArtnership_With_Age1","cPArtnership_With_Age2","cPArtnership_With_Age3","cPArtnership_With_Age4","cPArtnership_With_Age5",
        "pCSA_Mixing1","pCSA_Mixing2",
        "cAccess_Care1",	 "cAccess_Care2",
        "pLose_Adh_ART1","pLose_Adh_ART2","pLose_Adh_ART3",
        "cRace_Mixing1","cRace_Mixing2","cRace_Mixing3",
        "ADH","DRP", "0",	"prev",	"prevB",	"pSupp",	"pPart",	"0",
        "scenario",	"dur",	"units",	"adh",	"drp",	"0",
        "inc0",	"0",	"imp1",	"imp2",	"imp3",	"imp4",	"imp5",	"imp6",	"imp7",	"imp8",	"imp9",	"imp10",	"0",	"recipe"};
    
    vector<double> prepInfo={1,5,5000,0.8,0};// "scenario Duration Cov Adh dropout"
    
    vector<ModParam *> vMPs=generateMPs_SAoneway("seedConfig.cfg");
    single_SAoneway(vMPs,prepInfo,colNames);
    replicate_SAoneway(24,vMPs,prepInfo,colNames);
    
    
    
    //<<PREP>>
    
     ModParam * mp=new ModParam("seedConfig.cfg");
    vector<double> vCov= {500,2000,5000};//{100,500,1000,1500,2000,4000,6000,8000};//{500, 2000, 4000, 6000, 8000};
    vector<double> vAdh={.2,.8,1};//{.2,.4,.6,.8,1};//.25,.5,.75,1};
    vector<double> vScenarios={0,1};//{0,1,2,3};
    vector<double> vDuration={5,10};
    vector<double> vDropout={0,.5};//{0,.1,.25,.5};
    replicate_prep(mp, 5, vScenarios,vCov,vAdh,vDuration,vDropout);
    //single_prep(mp, vScenarios,vCov,vAdh,vDuration,vDropout);
    
    // <<CALIBRATION>>
    // <<FULL CALIBRATION>>
    //  ModParam * mp=new ModParam("defaults.cfg");
    //mp->viewLoaded(); //view mp file
    //  Driver * d=new Driver(mp);
    //monitoring pop demographics in a single run over time
    //1------
    // d->CalibrateDemographics(700, 1, 100); //runs the mode for x years and outputs the results for the pop and CSAs
    //2------
    //d->calibratePartnership_annualOutputs(50,100,50);
    //3------
    //calibrating prevelance and proportion of black at steady state
    /*vector<vector<double>> vec;
     for (int i=0;i<50;i++){
     Driver * d0=new Driver(mp);
     d0->burnin(50,50,100);
     vec.push_back(d0->getPop()->returnPrevInfo());
     delete d0;
     }
     printVector(  vec);
     writeVector(vec, "calib-prevAtSteadyState",true);
     cout<<endl;
     */
    //4------
    //  Driver * d=new Driver(mp);
    // d->burnin(50,50,100);
    //d->calibrateEpidemic_annualOutputs(100);
    
    
    
    cout<<"Sim end." << endl;
    return 0;
}
