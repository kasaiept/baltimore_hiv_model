//----
//---------------------------------------------------
//
//  ModParam.h
//
//  Header for reading mutable config data from text
//  file.
//
//  JPennington, June 2015
//
//
//---------------------------------------------------


#ifndef __HIVMODEL_MODPARAM_H
#define __HIVMODEL_MODPARAM_H

#include <vector>
#include <string>

enum class HeldTypes { Dbl, Int, Vec_d, Vec_i };

class CfgBase {
public:
    virtual std::string getN() = 0;
    virtual HeldTypes& getT() = 0;
};

template <class T>
class CfgParam : public CfgBase {
private:
    T stored_value;
    std::string name;
    HeldTypes type;
    
public:
    CfgParam ( const std::string str, const T val, const HeldTypes t );
    virtual ~CfgParam() = default;
    T get();
    std::string getN();
    HeldTypes& getT();
    void set ( const T new_val );
};

template <class T>
CfgParam<T>::CfgParam( const std::string str, const T val, const HeldTypes t )
: stored_value(val), name(str), type(t) {}

template <class T>
T CfgParam<T>::get() {
    return stored_value;
}

template <class T>
std::string CfgParam<T>::getN() {
    return name;
}

template <class T>
HeldTypes& CfgParam<T>::getT() {
    return type;
}

template <class T>
void CfgParam<T>::set ( const T new_val ) {
    stored_value = new_val;
}

class ModParam {
private:
    bool initFromFile ( const std::string& );
    void typeListInit ();
    HeldTypes detectType ( const std::string& );
    std::string trim(const std::string& str,
                     const std::string& whitespace = " \t");
    
    std::vector <CfgBase*> params_list;
    
public:
    ModParam() = default;
    ModParam(const std::string&);
    ModParam(const ModParam &); //copy constructor
    ModParam& operator= (ModParam); //copy assignment operator
    ~ModParam();
    void writeConfig( const std::string&, const std::string& );
    
    
    
    //Index based getters
    int getI(const int);
    double getD(const int);
    std::vector<double> getVD(const int);
    std::vector<int> getVI(const int);
    //Index based setters
    void setI (const int, const int);
    void setD (const int, const double);
    void setVD (const int, const std::vector<double> &);
    void setVI (const int, const std::vector<int> &);
    
    //Name based getters
    int getI(const std::string &);
    double getD(const std::string &);
    std::vector<double> getVD(const std::string &);
    std::vector<int> getVI(const std::string &);
    //Name based setters
    void setI (const std::string &, const int);
    void setD (const std::string &, const double);
    void setVD (const std::string &, const std::vector<double> &);
    void setVI (const std::string &, const std::vector<int> &);
    //use this function to see the state/index/value of the loaded parameters
    void viewLoaded();
    //returns a single list of all values
    std::vector<double> returnValueList();
    //for sensitivity analysis purposes; pick from 25% below and 25% above
    void pickSensValues(const double);
    void pickOWSensValues(const double step) ;

};


#endif
