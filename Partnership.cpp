//---------------------------------------------------
/* class Partnerships
 *
 *this class contains attributes and fucntions for managing the partnership class
 *

 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------


#include <iostream>
#include "Partnership.h"
#include "Person.h"

Partnership::Partnership(){};
Partnership::Partnership(Person * prs, int type, int duration,int startTime){
    _partner = prs;//partner
    _type = type;//0 CSL 1 Stb
    _duration = duration;//duration in weeks
    _tStart=startTime;
}

Partnership::Partnership( const Partnership &src )
: _type(src._type), _duration(src._duration) ,_tStart(src._tStart)
{
    _partner = new Person ( *(src._partner) );
}

Partnership& Partnership::operator= ( Partnership rhs ) {
    rhs.swap ( *this );
    return *this;
}

void Partnership::swap ( Partnership &src ) throw () {
    std::swap ( this->_type, src._type );
    std::swap ( this->_duration, src._duration);
     std::swap ( this->_tStart, src._tStart);
    std::swap ( this->_partner, src._partner);
}

Partnership::Partnership ( Partnership &&src ) noexcept
: _type(-1), _duration(-1), _tStart(-1), _partner(nullptr)
{
   (*this) = std::move(src);         
}

Partnership::~Partnership(){
 //  cout<<"calling the deconstructor";
}

