//---------------------------------------------------
/* class Person
 *
 *this class contains attributes and fucntions for managing the Person class
 *each person represents a MSM in Baltimore city 
 
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------


#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "GlobalParams.h"
#include "Person.h"
#include "Functions.h"
#include "Stats.h"
#include "ModParam.h"

using namespace std;
using namespace GLOBALPARAMS;
//using namespace GLOBALSTATS;
using namespace FUNCTIONS;
Person::Person(){};

Person::Person(int CSAID, ModParam * mp, Stats *s){
    _CSAId=CSAID;
    stats = s;
    _initConfigValues ( mp );
}

Person::Person(gsl_rng * rng, ModParam *mp, Stats *s, int CSAID,int age, int race,int HIVState)
{
    stats = s;
    _id = stats->getID();    stats->incID();
    _CSAId=CSAID;
    _age=age;
    _race=race;
    _diseaseState=HIVState;
    //
    _initConfigValues ( mp );//read parameters from file
    _initVariables( rng );//set local variables
    _initParams(rng);//set local params
}

void Person::reborn(gsl_rng * rng,ModParam *mp){    //give rebirth! halelooya!
    _id=stats->getID();    stats->incID();
    //_CSAID is already set
    _age =MINAGE+gsl_rng_uniform_int(rng, 5);
    _race=  assignRace0(rng,vCSA_RACE_BLACK_PROP0[_CSAId] );
    _diseaseState =  0;//assignRandomHIVState(rng,vCSA_HIVPREV0[_CSAId]);
    //
    _initConfigValues(mp);
    _initVariables( rng );
    _initParams(rng);
}

void Person::_initConfigValues ( ModParam * mp ) {//read values from input file
    pENGAGE_CSL = mp->getD("pENGAGE_CSL");//mp.getVD(0);
    cENGAGE_CSL_BASED_STB1 = mp->getD("cENGAGE_CSL_BASED_STB1");//mp.getVD(0);
    pENGAGE_STB  = mp->getD("pENGAGE_STB");//mp.getVD(0);
    
    cAGE_DIFF=mp->getD("cAGE_DIFF");
    vRACE_MIXING_COEF = mp->getVD("vRACE_MIXING_COEF");
    vSEXUALACTIVITY_COEF=mp->getVD("vSEXUALACTIVITY_COEF");
    vPART_AGE_COEF=mp->getVD("vPART_AGE_COEF");
    vPROP_CSA_MIXING=mp->getVD("vPROP_CSA_MIXING_PROP");
    //durations (weeks)
    vD1 = mp->getVI("vD1");
    vD2 = mp->getVI("vD2");
    vD3 = mp->getVI("vD3");
    vD4 = mp->getVI("vD4");
    vCHRONIC_DURATION_AFTER_ART = mp->getVI("vCHRONIC_DURATION_AFTER_ART");
    //
    pHIV_MORTALITY = mp->getD("pHIV_MORTALITY_YEARLY")/SIMWEEKS;
    pHIV_MORTALITY_ART = mp->getD("pHIV_MORTALITY_ART_COEF")*pHIV_MORTALITY;
    //
    vVL = mp->getVD("vVL_COEF");
    
    //
    pTESTING= mp->getD("pTESTING_YEARLY")/SIMWEEKS;
    pLINK_CARE_FIRST = mp->getD("pLINK_CARE_FIRST");
    pLINK_CARE_WEEKLY = mp->getD("pLINK_CARE_YEARLY")/SIMWEEKS;
    pINITIATE_ART = mp->getD("pINITIATE_ART");
    pREINITIATE_ART = mp->getD("pREINITIATE_ART_YEARLY")/SIMWEEKS;
    //
    vACCESS_CARE_COEF = mp->getVD("vACCESS_CARE_COEF");
    nGAP_IN_CARE=mp->getD("nGAP_IN_CARE");
    vLOSE_ADH_ART_PROB= mp->getVD("vLOSE_ADH_ART_PROB");
    cLOSE_ADH_ART_COEF=mp->getD("cLOSE_ADH_ART_COEF");
    //
    _pPrepAdh=mp->getD("pPREP_ADH");
    _pPrepDropout=mp->getD("pPREP_DROPOUT");
    
}

void Person::_initVariables ( gsl_rng * rng ) { //initiate local variables
    _tBorn = stats->getTime();
    _markedDead=false;
    
    setInitialAgeGroup();//based on current age
    _saClass=gsl_rng_uniform_int(rng, 3);//0,1,2 with equal prop
    //
    _vPartnerships.clear();
    _nStb = _nCsl = _nStbCum = _nCslCum = 0;
    _markedForPartnership=false;
    //
    _vRd.assign(6,0);
    _vEn.assign(6,0);
    _vEn[0]=stats->getTime();
    if ( _diseaseState > 0 ) {
        switch (_diseaseState) {
            case 1:
                enterDS1(rng); break;
            case 2:
                enterDS2(rng, true); break;
            case 3:
                enterDS3(rng, true); break;
            case 4:
                enterDS4(rng); break;
            default:
                enterDS5(rng);
        }
    }
    else enterDS0();
    _lastDiseaseState=0;
    _careState=0;
    _bGotAIDS=false;
    _pMortality=0;
    //
    _tDiagnosis=0;
    _dsDiagnosis=0;
    _tART_begin=0;
    _tART_end=0;
    _nART_trials=0;
    _rdLose_adh_ART=1000;
    //
    _infectiousness=0;
    _immunity=0;
    _nTransmission=0;
    _markedForTrans=false;
    //
    _bOnPrep=false;
   
}

void Person::_initParams(gsl_rng * rng ){//initiates local params based on mp files and other values
    updatePartAgeCoef();// _cPartBasedAge
    //
    _cSexualActivity=vSEXUALACTIVITY_COEF[_saClass];
    //
    if (_race == 1 )_pAccessCare=vACCESS_CARE_COEF[1];
    else _pAccessCare=vACCESS_CARE_COEF[0];
}


void Person::copyParamSettings(gsl_rng * rng,ModParam * mp){//reading fix values from mp file and updating local values
    _initConfigValues(mp);
    _initParams(rng);
}



Person::Person ( const Person& src )
: _id(src._id), _CSAId(src._CSAId), _age(src._age), _race(src._race), _ageGroup(src._ageGroup),_tBorn(src._tBorn),_markedDead(src._markedDead),
_nStb(src._nStb),_nCsl(src._nCsl), _nStbCum(src._nStbCum), _nCslCum(src._nCslCum),_markedForPartnership(src._markedForPartnership),_saClass(src._saClass),_cSexualActivity(src._cSexualActivity),_cPartBasedAge(src._cPartBasedAge),
_vRd(src._vRd.size()),_vEn(src._vEn.size()),
_diseaseState(src._diseaseState), _lastDiseaseState(src._lastDiseaseState), _careState(src._careState),_bGotAIDS(src._bGotAIDS),_pMortality(src._pMortality),
_tDiagnosis(src._tDiagnosis),_dsDiagnosis(src._dsDiagnosis),
_tART_begin(src._tART_begin),_tART_end(src._tART_end),_nART_trials(src._nART_trials),_rdLose_adh_ART(src._rdLose_adh_ART),
_immunity(src._immunity), _infectiousness(src._infectiousness),_nTransmission(src._nTransmission),_pAccessCare(src._pAccessCare),_markedForTrans(src._markedForTrans),
_bOnPrep(src._bOnPrep),_pPrepAdh(src._pPrepAdh),_pPrepDropout(src._pPrepDropout),
pENGAGE_CSL(src.pENGAGE_CSL),cENGAGE_CSL_BASED_STB1(src.cENGAGE_CSL_BASED_STB1),pENGAGE_STB(src.pENGAGE_STB),
cAGE_DIFF(src.cAGE_DIFF),vRACE_MIXING_COEF(src.vRACE_MIXING_COEF.size()),vSEXUALACTIVITY_COEF(src.vSEXUALACTIVITY_COEF.size()),vPART_AGE_COEF(src.vPART_AGE_COEF.size()),vPROP_CSA_MIXING(src.vPROP_CSA_MIXING.size()),
vD1(src.vD1.size()),vD2(src.vD2.size()),vD3(src.vD3.size()),vD4(src.vD4.size()),vCHRONIC_DURATION_AFTER_ART(src.vCHRONIC_DURATION_AFTER_ART.size()),
pHIV_MORTALITY(src.pHIV_MORTALITY),pHIV_MORTALITY_ART(src.pHIV_MORTALITY_ART),
vVL(src.vVL.size()),
pTESTING(src.pTESTING),pLINK_CARE_FIRST(src.pLINK_CARE_FIRST),pLINK_CARE_WEEKLY(src.pLINK_CARE_WEEKLY),pINITIATE_ART(src.pINITIATE_ART),pREINITIATE_ART(src.pREINITIATE_ART),
vACCESS_CARE_COEF(src.vACCESS_CARE_COEF.size()),nGAP_IN_CARE(src.nGAP_IN_CARE),vLOSE_ADH_ART_PROB(src.vLOSE_ADH_ART_PROB.size()),cLOSE_ADH_ART_COEF(src.cLOSE_ADH_ART_COEF)
{
    // for (auto p : src._vPartnerships)
    //    _vPartnerships.push_back ( new Partnership(*p) ); will be done seperately
    _vPartnerships.clear();
    copy (src._vRd.begin(), src._vRd.end(), _vRd.begin());
    copy (src._vEn.begin(), src._vEn.end(), _vEn.begin());
    //
    copy (src.vPROP_CSA_MIXING.begin(),src.vPROP_CSA_MIXING.end(), vPROP_CSA_MIXING.begin());
    copy (src.vRACE_MIXING_COEF.begin(),src.vRACE_MIXING_COEF.end(), vRACE_MIXING_COEF.begin());
    
    copy (src.vSEXUALACTIVITY_COEF.begin(), src.vSEXUALACTIVITY_COEF.end(), vSEXUALACTIVITY_COEF.begin());
    copy (src.vPART_AGE_COEF.begin(), src.vPART_AGE_COEF.end(), vPART_AGE_COEF.begin());
    copy (src.vD1.begin(), src.vD1.end(), vD1.begin());
    copy (src.vD2.begin(), src.vD2.end(), vD2.begin());
    copy (src.vD3.begin(), src.vD3.end(), vD3.begin());
    copy (src.vD4.begin(), src.vD4.end(), vD4.begin());
    copy (src.vVL.begin(), src.vVL.end(), vVL.begin());
    copy (src.vCHRONIC_DURATION_AFTER_ART.begin(), src.vCHRONIC_DURATION_AFTER_ART.end(), vCHRONIC_DURATION_AFTER_ART.begin());
    copy (src.vACCESS_CARE_COEF.begin(), src.vACCESS_CARE_COEF.end(), vACCESS_CARE_COEF.begin());
    copy (src.vLOSE_ADH_ART_PROB.begin(),src.vLOSE_ADH_ART_PROB.end(), vLOSE_ADH_ART_PROB.begin());
    stats = src.stats;
}

Person& Person::operator= (Person rhs) {
    rhs.swap ( *this );
    return *this;
}

void Person::swap ( Person &src ) throw () {//<<recheck>>
    std::swap( this->_id, src._id );
    std::swap( this->_CSAId, src._CSAId );
    std::swap( this->_age, src._age );
    std::swap( this->_race, src._race );
    std::swap( this->_ageGroup, src._ageGroup );
    std::swap( this->_tBorn, src._tBorn );
    std::swap( this->_markedDead, src._markedDead );
    
    std::swap( this->_vPartnerships, src._vPartnerships );
    std::swap( this->_nStb, src._nStb );
    std::swap( this->_nCsl, src._nCsl );
    std::swap( this->_nStbCum, src._nStbCum );
    std::swap( this->_nCslCum, src._nCslCum );
    std::swap( this->_markedForPartnership, src._markedForPartnership );
    std::swap(this->_saClass, src._saClass);
    std::swap(this->_cSexualActivity, src._cSexualActivity);
    std::swap(this->_cPartBasedAge, src._cPartBasedAge);
    
    std::swap( this->_vRd, src._vRd );
    std::swap( this->_vEn, src._vEn );
    std::swap( this->_diseaseState, src._diseaseState );
    std::swap( this->_lastDiseaseState, src._lastDiseaseState );
    std::swap( this->_careState, src._careState );
    std::swap( this->_bGotAIDS, src._bGotAIDS );
    std::swap( this->_pMortality, src._pMortality );
    
    std::swap( this->_tDiagnosis, src._tDiagnosis );
    std::swap( this->_dsDiagnosis, src._dsDiagnosis );
    std::swap( this->_tART_begin, src._tART_begin);
    std::swap( this->_tART_end, src._tART_end);
    std::swap( this->_nART_trials, src._nART_trials);
    std::swap( this->_rdLose_adh_ART, src._rdLose_adh_ART);
    
    std::swap( this->_immunity, src._immunity );
    std::swap( this->_infectiousness, src._infectiousness );
    std::swap( this->_nTransmission, src._nTransmission );
    std::swap(this->_markedForTrans,src._markedForTrans);
    
    std::swap( this->_pAccessCare, src._pAccessCare );
    
    std::swap( this->_bOnPrep, src._bOnPrep);
    std::swap( this->_pPrepAdh, src._pPrepAdh);
    std::swap( this->_pPrepDropout, src._pPrepDropout);
    
    
    std::swap( this->pENGAGE_CSL, src.pENGAGE_CSL);
    std::swap( this->cENGAGE_CSL_BASED_STB1, src.cENGAGE_CSL_BASED_STB1);
    std::swap( this->pENGAGE_STB, src.pENGAGE_STB);
    std::swap( this->vPROP_CSA_MIXING, src.vPROP_CSA_MIXING);
    
    std::swap( this->cAGE_DIFF, src.cAGE_DIFF);
    std::swap( this->vRACE_MIXING_COEF, src.vRACE_MIXING_COEF);
    
    std::swap( this->vSEXUALACTIVITY_COEF, src.vSEXUALACTIVITY_COEF);
    std::swap( this->vPART_AGE_COEF, src.vPART_AGE_COEF);
    std::swap( this->vD1, src.vD1);
    std::swap( this->vD2, src.vD2);
    std::swap( this->vD3, src.vD3);
    std::swap( this->vD4, src.vD4);
    std::swap( this->pHIV_MORTALITY, src.pHIV_MORTALITY);
    std::swap( this->pHIV_MORTALITY_ART, src.pHIV_MORTALITY_ART);
    std::swap( this->vVL, src.vVL);
    std::swap( this->vCHRONIC_DURATION_AFTER_ART, src.vCHRONIC_DURATION_AFTER_ART);
    
    std::swap( this->pTESTING, src.pTESTING);
    std::swap( this->pLINK_CARE_WEEKLY, src.pLINK_CARE_WEEKLY);
    std::swap( this->pLINK_CARE_FIRST, src.pLINK_CARE_FIRST);
    std::swap( this->pINITIATE_ART, src.pINITIATE_ART);
    std::swap( this->pREINITIATE_ART, src.pREINITIATE_ART);
    std::swap( this->vACCESS_CARE_COEF, src.vACCESS_CARE_COEF);
    std::swap( this->nGAP_IN_CARE, src.nGAP_IN_CARE);
    std::swap( this->vLOSE_ADH_ART_PROB, src.vLOSE_ADH_ART_PROB);
    std::swap( this->cLOSE_ADH_ART_COEF, src.cLOSE_ADH_ART_COEF);
    
    std::swap( this->stats, src.stats );
    
}

Person::Person ( Person&& src ) noexcept //move constructor
: _id(0), _CSAId(0), _age(0), _race(0), _ageGroup(0),_tBorn(0),_markedDead(false),
_nStb(0), _nCsl(0), _nStbCum(0), _nCslCum(0),_markedForPartnership(false),_saClass(-1),_cSexualActivity(0),_cPartBasedAge(0),
_diseaseState(0), _lastDiseaseState(0),_careState(0), _bGotAIDS(false),_pMortality(0),
_tDiagnosis(0), _dsDiagnosis(0), _tART_begin(0), _tART_end(0), _nART_trials(0), _rdLose_adh_ART(0),
_immunity(0), _infectiousness(0), _nTransmission(0),_pAccessCare(0),_markedForTrans(0),
_pPrepAdh(0), _bOnPrep(false),_pPrepDropout(0)
{
    _vPartnerships.clear();
    _vRd.clear();
    _vEn.clear();
    
    (*this) = std::move(src);//cast
}

Person::~Person(){
    //  cout<<"Person "<<_id<<"age "<<_age<<" deleted."<<endl;
    for (auto part : _vPartnerships)
        delete (part);
}

bool Person::willDieNaturally(gsl_rng * rng ){//retunrs true if person is dying from natural mortality causes or old age >55
    if ((gsl_rng_uniform(rng)< (vAGE_SPECIFIC_MORTALITY[_ageGroup])) || (_age > MAXAGE)){
        _markedDead=true;
        return true;
    }
    return false;
}

void Person::unlink(){
    //unlink and remove from all partners' lists
    int num=_vPartnerships.size();
    for(int i=0;i<num;i++){
        dissolvePartnershipMutually(i);
    }
};

void Person::kill(){
    // if(_diseaseState>0) cout << _nART_trials<< " ";
    //save statitics
    //remove him from everyone's life!
    unlink();
}


//-------------------------------------------------------------------------------------------------------------
void Person::setInitialAgeGroup(){//assigns the ageGroups based on current age (initially called for population creation)
    int M=vAGE_GROUP_LIMITS.size();
    for (int i=M-1;i>=0;i--){//search groups from last to first
        if (_age>=vAGE_GROUP_LIMITS[i]){
            _ageGroup=i;
            break;
        }}
}

void Person::updateAgeGroup(){//updates individual's current age group
    //if you current age is greater than the limit of your age group, add one agroup
    if (_age<vAGE_GROUP_LIMITS.back()+1){
        if (_age>=vAGE_GROUP_LIMITS[_ageGroup+1])
            _ageGroup++;
    }
    
}

void Person::updatePartAgeCoef(){
    if (_ageGroup<2)
        _cPartBasedAge=vPART_AGE_COEF[0];
    else {
        if (_ageGroup<4)
            _cPartBasedAge=vPART_AGE_COEF[1];
        else {
            if (_ageGroup<6)
                _cPartBasedAge=vPART_AGE_COEF[2];
            else {
                if (_ageGroup<9)
                    _cPartBasedAge=vPART_AGE_COEF[3];
                else {
                    _cPartBasedAge=vPART_AGE_COEF[4];
                }}}}
}

void Person::assign_infectiousness(gsl_rng * rng){
    //returns infectiousness as a function of current state VL
    double VL=vVL[_diseaseState];
    _infectiousness=pow(2.45, VL-4.5);
}

void Person::assign_prepParams(gsl_rng * rng, double ADH, double DROPPUT){
    _pPrepAdh=ADH;
    _pPrepDropout=DROPPUT;
    
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------
//MIXING-------------------------//--------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
int Person::returnPartnershipCSAId(gsl_rng * rng){//returns CSA's for the partnership
    //Choose a CSA for partnership selection (return CSA's id): 1. one owns CSA, 2. Random CSA from group, 3. random CSA from all others
    vector<int> vec;
    double r=gsl_rng_uniform(rng);
    if(r<vPROP_CSA_MIXING[0]){
        return _CSAId;
    }
    //else
    if(r<vPROP_CSA_MIXING[1]+vPROP_CSA_MIXING[0]){
        vector<int> vGroupMembers= vvGROUP_MEMBERS[_CSAId];//vecor of my group members
        if(vGroupMembers.size()>0){
            //return a random CSA
            int num=gsl_rng_uniform_int(rng,vGroupMembers.size());
            return (vGroupMembers[num]);
        }
        //else If vGroupMembers.size() <= 0
        return _CSAId;
        //if there is no group member
    }
    vector<int> vNonGroupMembers= vvNONGROUP_MEMBERS[_CSAId];
    if (vNonGroupMembers.size()>0){
        int num=gsl_rng_uniform_int(rng,vNonGroupMembers.size());
        return (vNonGroupMembers[num]);
    }
    //else If vNonGroupMembers.size <= 0)
    return _CSAId;
    //if there is no nongroup member
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------
//PARTNERSHIPS-----------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------

bool Person:: evaluateRaceAgeMixing(gsl_rng * rng,Person * q, int cAGE_DIFF){
    double pAgeMixing=0;
    pAgeMixing= 1- (abs(sqrt(_age)-sqrt(q->getAge()))/cAGE_DIFF ) ;
    
    double pRaceMixing=0;
    if (_race==q->getRace())
        pRaceMixing=vRACE_MIXING_COEF[_race]; //0 for ww, 1 for bb
    else
        pRaceMixing= vRACE_MIXING_COEF[2];//2 for wb
    
    if ((gsl_rng_uniform(rng)< pAgeMixing)&&(gsl_rng_uniform(rng)< pRaceMixing) ){
        return true;
    }
    
    return false;
}


void Person::addPartnership(Partnership * r){//add this partnerhip to the last of my list
    _vPartnerships.push_back(r);
}

bool Person::isAvailableCsl(gsl_rng * rng){//returns true if the person is available and interested in forming a casual partnership
    double cStablePart= _nStb* cENGAGE_CSL_BASED_STB1 + (1-_nStb); //coef based on current stable partnership status: 1 if no stable partner or  pENGAGE_CSL_BASED_STB
    double prob= pENGAGE_CSL*    cStablePart*   _cSexualActivity * _cPartBasedAge;
    if (gsl_rng_uniform(rng)<prob)
        return true;
    //else
    return false;
}

bool Person::isAvailableStb(gsl_rng * rng){//returns true if the person is available and interested in forming a stb partnership
    if (gsl_rng_uniform(rng)< (0+pENGAGE_STB * (1-_nStb)) * _cPartBasedAge )
        return true;
    //else
    return false;
}

bool Person::evaluateCslPartnership(gsl_rng * rng, Person * q){//make sure if I can partner with q
    // p is not me or one of my current partners
    if (q==this ) return false;
    //else
    for (Partnership * s:_vPartnerships)
        if (q==s->getPartner())
            return false;
    //else    //age and race mixing probs
    return evaluateRaceAgeMixing(rng,  q, cAGE_DIFF);
}

bool Person::evaluateStbPartnership(gsl_rng * rng, Person * q){//returns true if the given casual partnership can be evolved to a stb one
    // p is not me or one of my current partners
    if (q==this ) return false;
    //else
    for (Partnership * s:_vPartnerships)
        if (q==s->getPartner())
            return false;
    //else    //age and race mixing probs
    return evaluateRaceAgeMixing(rng,  q, cAGE_DIFF);
}

void Person::formCslPartnership(gsl_rng * rng, Person * q){//creates a new casual partnership
    stats->recordStat_CSAMixing(_CSAId, q->getCSAId());
    stats->recordStat_AgeRaceMixing(_age,q->_age,_race,q->_race);
    
    int duration= assignCslPartnershipDuration(rng );
    //for me
    Partnership * r=new Partnership(q,0,duration,stats->getTime());
    _vPartnerships.push_back(r);//add the partnership to the end of array
    _nCsl++;    _nCslCum++;
    _markedForPartnership=true;
    
    //for my partner
    Partnership * r1=new Partnership(this,0,duration,stats->getTime());
    q->_vPartnerships.push_back(r1);//add the partnership to the end of array
    q->_nCsl++;    q->_nCslCum++;
    q->_markedForPartnership=true;
}

void Person::formStbPartnership(gsl_rng * rng, Person * q){//creates a new casual partnership
    stats->recordStat_CSAMixing(_CSAId, q->getCSAId());
    stats->recordStat_AgeRaceMixing(_age,q->_age,_race,q->_race);
    
    int duration= assignStbPartnershipDuration(rng );
    
    //for me
    Partnership * r=new Partnership(q,1,duration,stats->getTime());
    _vPartnerships.push_back(r);//add the partnership to the end of array
    _nStb++;    _nStbCum++;
    _markedForPartnership=true;
    
    //for my partner
    Partnership * r1=new Partnership(this,1,duration,stats->getTime());
    q->_vPartnerships.push_back(r1);//add the partnership to the end of array
    q->_nStb++;    q->_nStbCum++;
    q->_markedForPartnership=true;
}


void Person::dissolvePartnershipMutually(int index){   //Ends a partnership, and removes for BOTH people
    Person * q=_vPartnerships[index]->getPartner();
    int myIndex=q->findPartnerIndex(this);//finds the partnership index of q with p;
    removeMyPartnership(index);
    q->removeMyPartnership(myIndex);
}

void Person::removeMyPartnership(int index){//removes (deletes) partnership at the index
    if (_vPartnerships[index]->getType()==0)
        _nCsl--;
    else
        _nStb--;//update stats
    delete(_vPartnerships[index]);
    _vPartnerships[index]=_vPartnerships.back();//replace the pointer with last element
    _vPartnerships.pop_back();//reduce the vec size
}

int Person::findPartnerIndex(Person * p){//finds the partneship index with person p
    int N=_vPartnerships.size();
    for(int i =0;i<N;i++){
        if(_vPartnerships[i]->getPartner() == p) return i;
    }
    cout<<"partner was not found! error in findPartnerIndex!";
    int x= cin.get();
    return -1;
}






//-----------------------------------------------------------------------------------------------------------------------------------------------------------
//HIV NATURAL HISTORY----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
void Person::enterDS0(){
}

void Person::enterDS1(gsl_rng * rng){//enter ACUTE infection
    _lastDiseaseState=_diseaseState;
    _diseaseState=1;
    _vRd[1]= return_stateDuration(rng, vD1);
    _vEn[1]=stats->getTime();
    _pMortality=pHIV_MORTALITY;
    assign_infectiousness(rng);//_infectiousness
    //record incidence
    stats->recordStat_nIncIV();
    if (_race==1)     stats->recordStat_nIncIV_B();
    stats->recordStat_incRaceAge(_race,_ageGroup);
    
}

void Person::enterDS2(gsl_rng * rng ,bool resetDuration){//enter CHRONIC infection
    _lastDiseaseState=_diseaseState;
    _diseaseState=2;
    if (resetDuration){
        _vRd[2]= return_stateDuration(rng, vD2);
    }
    _vEn[2]=stats->getTime();//recheck????
    _pMortality=pHIV_MORTALITY;
    assign_infectiousness(rng);//infectiousness
}

void Person::enterDS3(gsl_rng * rng,bool resetDuration){//enter LATE infection
    _lastDiseaseState=_diseaseState;
    _diseaseState=3;
    if (resetDuration){
        _vRd[3]= return_stateDuration(rng, vD3);
    }
    _vEn[3]=stats->getTime();//recheck????
    _pMortality=0;
    _bGotAIDS=true;
    assign_infectiousness(rng);//_infectiousness
}

void Person::enterDS4(gsl_rng * rng){// PARTIAL SUPP
    _lastDiseaseState=_diseaseState;
    _diseaseState=4;
    _vRd[4]= return_stateDuration(rng,  vD4);
    _vEn[4]=stats->getTime();
    _pMortality=pHIV_MORTALITY_ART;
    assign_infectiousness(rng);//_infectiousness=
    
    //time to lose adherence to ART
    _rdLose_adh_ART=assignDurationOnART(rng);
}

void Person::enterDS5(gsl_rng * rng){//FULL SUPP
    _diseaseState=5;
    assign_infectiousness(rng);//_infectiousness=
    //    _vD[5]= infinit
    //    _vRd[5]=infinit
    _vEn[5]=stats->getTime();
    _pMortality=pHIV_MORTALITY_ART;
}

void Person::exitDS(){
}

double Person::assignDurationOnART(gsl_rng * rng){
    double r=gsl_rng_uniform(rng);
    double d=0;
    if (r<vLOSE_ADH_ART_PROB[0] *cLOSE_ADH_ART_COEF) d=gsl_rng_uniform_int(rng,52); //24% in the first year
    else {
        if (r<vLOSE_ADH_ART_PROB[1]   *cLOSE_ADH_ART_COEF) d=gsl_rng_uniform_int(rng,SIMWEEKS)+ SIMWEEKS; //by second year
        else{
            if (r<vLOSE_ADH_ART_PROB[2]  *cLOSE_ADH_ART_COEF)  d= (gsl_rng_uniform_int(rng,6)+2)*SIMWEEKS+gsl_rng_uniform_int(rng,SIMWEEKS); //by 8th year
            else
                d=(gsl_ran_geometric(rng, .5)+8)*SIMWEEKS; //8-x years
        }
    }
    /*    if (gsl_rng_uniform(rng)< 0.3)
     d=gsl_rng_uniform_int(rng,26);
     else {
     d=gsl_ran_geometric(rng, 0.00961)+26; //8-x years}
     }*/
    return d;
}

//CARE PROCESSES-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

bool Person:: willTestForHIV(gsl_rng * rng){//those with CS=0 --> CS=1
    if(gsl_rng_uniform(rng)< (pTESTING  * _pAccessCare ) ){
        _careState=1;
        _tDiagnosis=stats->getTime();
        _dsDiagnosis=_diseaseState;
        //records:
        if (_diseaseState==3) //if diagnosed with AIDS
            stats->recordStat_nDiagAIDS();
        else stats->recordStat_nDiagHIV();
        
        //if (stats->getTime()>RECORD_STAT_TIME)      stats->recordStat_InfoAtDiag(this);
        return true;
    }
    return false;
}

bool Person::willLinkToCare(gsl_rng * rng,bool firstTry){//
    if (firstTry) {//immediately after diagnosis
        if(gsl_rng_uniform(rng)< (pLINK_CARE_FIRST* _pAccessCare) )//linked
            willGoOnART(rng);
        else
            _careState=1;//notLinked
    }
    else{ //later after diagnosis, but not linked yet
        if (gsl_rng_uniform(rng)< (pLINK_CARE_WEEKLY* _pAccessCare))
            willGoOnART(rng);
        else
            _careState=1;//notLinked
    }
    return false;
    
}

bool Person::willGoOnART(gsl_rng *rng){
    if(gsl_rng_uniform(rng)< (pINITIATE_ART * _pAccessCare)){//linked & ART
        exitDS();
        _careState=2;//linkedOnART
        _tART_begin= stats->getTime();
        _nART_trials++;
        //  if (stats->getTime()>RECORD_STAT_TIME)  stats->recordStat_InfoAtART(this);
        enterDS4(rng);
        return true;
    }
    //else //linked & offART
    _careState=3;//linkedOnART
    return false;
}

bool Person::willReinitiateART(gsl_rng * rng){//models the proces of going back on ART after the previous withdrawal
    if ((stats->getTime()-_tART_end) < nGAP_IN_CARE) //if experiencing gap in care
        return false;
    //
    if(gsl_rng_uniform(rng)< (pREINITIATE_ART * _pAccessCare )){
        exitDS();
        _careState=2;//linkedOnART
        _tART_begin= stats->getTime();
        _nART_trials++;
        enterDS4(rng);
        return true;
    }
    return false;
    
}

bool Person::willLoseAdhART(gsl_rng * rng){//Models the process of going of ART: End: stay
    if(_rdLose_adh_ART>0){
        _rdLose_adh_ART--;
        return false;
    }
    //        else: lose adherence
    exitDS();
    _careState=3;//linkedOnART
    _tART_end= stats->getTime();
    //The next state depends on where we came from
    //if partially suppressed, person returns to the previous diseaseState before starting ART
    //we won't change the remaining duration for that state as if they never went on full suppression
    if (_diseaseState==4){//partial suppression will go back to original state
        if (_lastDiseaseState==3)
            enterDS3(rng,false);
        else //if came from 1 or 2
            enterDS2(rng,false);
    }
    //if fully suppressed, those started off from chronic state go back there, but those started from AIDS state will go back to chronic for a while before hitting AIDS again
    else{ //if (_diseaseState==5){
        enterDS2(rng,false);//don't reset original duration
        _vRd[2]=_vRd[2]+return_chronicDurationAfterART(rng, vCHRONIC_DURATION_AFTER_ART);
    }
    //
    return true;
    
}

bool Person::willDieHIV(gsl_rng * rng){
    if (gsl_rng_uniform(rng)<_pMortality){ //dying from disease
        exitDS();
        _markedDead=true;
        return true;
    }
    return false;
    
}

bool Person::willDSProgress(gsl_rng * rng){
    switch (_diseaseState){
        case (1): {
            if (_vRd[1]==0){//proceed to next
                exitDS();
                enterDS2(rng,true);
                return true;
            }
            _vRd[1]--;
            return false;
        }
        case (2):{
            if (_vRd[2]==0){//proceed to next
                exitDS();
                enterDS3(rng,true);
                return true;
            }
            _vRd[2]--;
            return false;
        }
        case (3):{
            if (_vRd[3]==0){//proceed to next
                exitDS();
                setMarkedDead(true);//AIDS death
                return true;
            }
            _vRd[3]--;
            return false;
        }
        case (4):{
            if (_vRd[4]==0){//proceed to next
                exitDS();
                enterDS5(rng);
                return true;
            }
            _vRd[4]--;
            return false;
        }
        default:
        { cout<<"wrong call for willDSProgress!"<<endl;
            int x= cin.get();
        }
    }
    return false;
}



//PREP----------------------------------------------------------------------------------------------

void Person::goOffPrep( ){
    _bOnPrep=false;
    _immunity=0;
}

void Person::goOnPrep(){
    _bOnPrep=true;
    _immunity=_pPrepAdh;
}

bool Person::isPrepEligible(int scenario){
    switch (scenario) {
        case 0:{
            if (_diseaseState==0) return true;
            else        return false;
            break;
        }
        case 1:{
            if ((_diseaseState==0)&&(_nCslCum+_nStbCum > 1)) return true;
            else        return false;
            break;
        }
        case 2:{
            if ((_diseaseState==0)&&(_nCslCum+_nStbCum > 5)) return true;
            else        return false;
            break;
        }//
            
        case 3:{
            if ((_diseaseState==0)&&(_ageGroup<4)) return true;//age [15-30]
            else        return false;
            break;
        }
        case 4:{
            if ((_diseaseState==0)&&(_ageGroup<4)&&(_race==1 )) return true;//age [15-30] black
            else        return false;
            break;
        }
           
        default:
            return false;
            break;
    }
    
}

bool Person::willDropoutPrep(gsl_rng * rng){
    if (gsl_rng_uniform(rng)< _pPrepDropout)
        return true;
    else
        return false;
    
}
