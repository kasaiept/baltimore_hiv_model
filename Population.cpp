//---------------------------------------------------
/* class Population
 *
 *this class contains attributes and fucntions for managing the population class, as well as outputs generation
 *
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------


#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include <vector>
#include <tgmath.h> //pow
#include <algorithm>
#include "Population.h"
#include "Stats.h"
#include "Functions.h"
#include "CSA.h"
#include "ModParam.h"

using namespace std;
//using namespace GLOBALSTATS;
using namespace FUNCTIONS;

//costructor:
Population::Population(){ }

Population::Population(gsl_rng * rng, ModParam * mp, int nCSAs, vector<int> vCSAPop, vector<double> vCSARaceBlackProp, vector<vector<double>> vvCSAAgeProp){
    stats = new Stats();
    if (stats->getDBG_LVL())        cout << "P";
    mp_local = mp;
    //params
    cTRANS = mp->getD("cTRANS");
    
    //variables
    nPREP_SCENARIO=0;
    pPREP_COVERAGE = 0;
    nPREP_UNITS=0;
    _tPREP_start=-1;
    _pPREP_lastCoverage=-1;
    
    _nCSAs=nCSAs;
    for (int i=0;i<nCSAs;i++){
        _vCSAs.push_back( new CSA(rng, mp, stats, i,vCSAPop[i],vCSARaceBlackProp[i],vvCSAAgeProp[0]));
        //_nPeople ++??;
    }
    
}

Population::Population(  Population& src )
: _nCSAs( src._nCSAs),
cTRANS(src.cTRANS),
nPREP_SCENARIO(src.nPREP_SCENARIO),pPREP_COVERAGE(src.pPREP_COVERAGE),nPREP_UNITS(src.nPREP_UNITS),
_tPREP_start(src._tPREP_start),_pPREP_lastCoverage(src._pPREP_lastCoverage)
{
    //The population holds a pointed to mp_local and stat, so when we copy the populatio to a new population, we have to update these pointers to new files (just copying the same pointer will refernce the mp-file by both population)
    mp_local= new ModParam(*src.mp_local);
    
    for (auto c : src._vCSAs) { //copy all CSA and people in them, except partnerships
        _vCSAs.push_back ( new CSA (*c) );
    }
    stats = new Stats(*(src.stats));//replace previous by this
    copyStats(stats);
    
    copyPartnerships(src);
    
}
void Population::copyStats(Stats * s){
    for (auto *c : _vCSAs){
        c->setStats(s);
        for (auto * p: *c){
            p->setStats(s);
        }}
}

void Population::copyParamSettings(gsl_rng * rng,ModParam * mp){//copies the mp file instead into the population and all of its members
    //0-mp file
    mp_local=mp;
    //1-copy pop params
    cTRANS = mp->getD("cTRANS");
    
    //2-copy person params
    for (auto *c : _vCSAs){
        for (auto * p: *c){
            p->copyParamSettings(rng,mp);
        }}
    
}

void Population::copyPartnerships( Population& src){//in order to map all existing partnerships of a source population into this one
    int i=0;
    int j=0;
    for (auto * c1: src){
        for(auto * p1:*c1){//source case
            Person * p2= _vCSAs[i]->getMember(j);//his counterpart in the new population
            
            for (auto * r1: *p1){//for each partnership of original person
                int qId= r1->getPartner()->getId();
                //now find this person in this population
                Person * q= returnPerson(qId);
                Partnership * r2=new Partnership(q, r1->getType(),r1->getDuration(),r1->getTStart());
                p2->addPartnership(r2);//a new partnership witll be added to the other partner in his turn
            }
            j++;//next person
        }
        i++;//next csa
        j=0;
    }
}

Person * Population::returnPerson(int i){
    for (auto * c:_vCSAs){
        for(auto * p:*c){
            if (p->getId()==i) return p;
        }}
    cout<<"Person's id not found";
    return (new Person());
}

Population::~Population(){
    //delete all CSA members
    for ( auto it : _vCSAs ) {
        if ( it != nullptr ) {
            if (stats->getDBG_LVL())
                cout << "x";
            delete (it);
        }}
    delete stats;
    // cout << endl;
}

int Population::getNumPeople(){
    int N=0;
    for (auto c:_vCSAs)
        N += c->getPopSize();
    return N;
}

double Population::getPrpBlack(){
    int N=0;
    double b=0;
    for (auto c:_vCSAs){
        N += c->getPopSize();
        for(auto p:*c){
            if(p->getRace()==1) b++;
        }}
    return (b/N);
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//INITIAL EPIDEMIC------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<double>  Population::setEpidemic(gsl_rng * rng){//sets initial epidemic to calibrate the inputs
    if (stats->getDBG_LVL())
        cout << "<<<<SET EPIDEMIC>>>>" <<endl;
    
    vector<vector<int>> vvRes(2,vector<int>(nCSAs,0)); //freq of infected people in each race[] and CSA[]
    //prop of hiv in each CSA
    int i=0;
    for (auto * c: _vCSAs){
        double r1=vHIV_PROP_CSA0[i];//inf coef for this CSA
        for (auto * p : *c ){
            if (p->getRace()==0){
                if (gsl_rng_uniform(rng) < r1 *cINF_POP){
                    p->setDiseaseState(2);
                    vvRes[p->getRace()][i]++;
                }}
            else{
                if (gsl_rng_uniform(rng) < r1 *cINF_POP*cINF_RACE){
                    p->setDiseaseState(2);
                    vvRes[p->getRace()][i]++;
                }}}
        i++;
    }
    double prevAll= vectorSum(vvRes);
    double prevBlack= vectorSum(vvRes[1]);
    vector<double> vec= {prevAll, prevBlack/prevAll};
    return vec;
}

void  Population::setInitialDSCS(gsl_rng * rng ){//sets initial epidemic to calibrate the inputs
    double pDiagnoed= pDIAGNOSED0;
    double pLinkedCare_diag= pLINKED0;
    double pOnART_Linked= pONART0;
    double pSupp_onART= pSUPP0;
    double pAIDS= pAIDS0;
    int i=0;
    for (auto * c: _vCSAs){
        double r1=vHIV_PROP_CSA0[i];//inf coef for this CSA
        for (auto * p : *c ){
            if (p->getDiseaseState()>0) {//infected
                if (gsl_rng_uniform(rng)<pDiagnoed){//diagnosed?
                    if (gsl_rng_uniform(rng)<pLinkedCare_diag){
                        if(gsl_rng_uniform(rng)<pOnART_Linked){
                            p->setCareState(2);
                            if(gsl_rng_uniform(rng)<pSupp_onART)
                                p->enterDS5(rng);
                            else
                                p->enterDS4(rng);
                        }
                        else{//off art but linked
                            p->setCareState(3);
                            if(gsl_rng_uniform(rng)<pAIDS)
                                p->enterDS3(rng,true);
                            else
                                p->enterDS2(rng, true);
                        }}
                    else{//diag but not linked
                        p->setCareState(1);
                        if(gsl_rng_uniform(rng)<pAIDS)
                            p->enterDS3(rng, true);
                        else
                            p->enterDS2(rng,true);
                    }}
                else {//not diagnosed
                    p->setCareState(0);
                    if(gsl_rng_uniform(rng)<pAIDS)
                        p->enterDS3(rng, true);
                    else
                        p->enterDS2(rng, true);
                }}}
        i++;}
};

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//DEMOGRAPHIC MODULE: POPULATION NATURAL DYNAMICS---------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Population::modelNaturalDynamics(gsl_rng * rng){ //models natural dynamic processes among CSAs
    for (auto c:_vCSAs){
        //1-MODEL AGING:
        //Add age and update the age group
        for(auto p: *c){
            p->addAge();
            p->updateAgeGroup();
            p->updatePartAgeCoef();
        }
        //2-MODEL DEATHS:
        //Check everyone and record index of those who've died or are dying now
        int indx=0;
        vector<int> vDeadInx;//temporary vector of index of those dying in this CSA
        for(auto p: *c){
            if (p->willDieNaturally(rng)){
                vDeadInx.push_back(indx);
                //  if (stats->getTime()>RECORD_STAT_TIME) stats->recordStat_InfoAtDeath(p);
            }
            indx++;
        }
        int nNaturalDeaths=vDeadInx.size() ; //number of deaths at this timestep
        //3-MODEL BIRTHS---cheating!!!
        int temp= vCSA_POP0[c->getId()]-c->getPopSize() + nNaturalDeaths;//initial pop- now
        int nBirths=0;
        if (temp>0)
            nBirths=gsl_ran_poisson(rng,temp); //poisson draw for number of births
        //for efficiency, we don't explicitly kill peaple, just restart (reborn) them so that we use the previously allocated memory spot for the new person
        if(nBirths>=nNaturalDeaths){//reborn all dead people, add extra newborns
            for (auto indx: vDeadInx){//kill/reborn
                (*c)[indx]->kill();
                (*c)[indx]->reborn(rng,mp_local);
            }
            for(int i=0;i<(nBirths-nNaturalDeaths);i++){//new borns
                //addNewMember();
                Person* p=new Person(c->getId(), mp_local, stats);
                p->reborn(rng,mp_local);
                c->addMember(p);
            }
        }
        else { //kill/reborn as many needed, delete others
            for (int i=0;i<nBirths;i++){
                int n=vDeadInx[i];
                (*c)[n]->kill();
                (*c)[n]->reborn(rng,mp_local);
            }
            //for deleting people here, we should start from the end of the list and move backward since in each itteration, we reduce the list's size by one and otherwise it wil change the indexes of dead people
            for(int i=nNaturalDeaths;i>nBirths;i--){
                int n=vDeadInx[i-1];
                (*c)[n]->kill();
                c->deleteMember(n);//deletes this member completely
            }}}
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//MIXING & PARTNERSHIP------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Population::modelPartnerships(gsl_rng * rng,bool modelStb, bool modelCsl, bool modelDissolution){//managing partnerships dessolution, creation of new csl and stb ones at each call
    vector<Person * > vTemp;
    
    //<<PARTNERSHIP DESOLUTION>>  check durations, continue or end partnerships
    for (auto c:_vCSAs){
        for (auto p: *c){
            int numPartnerships=p->getNumPartnerships();
            if (modelDissolution){
                for(int index=0;index<numPartnerships;index++){//loop over all p's partnerships, either update them or dissolve
                    if(p->getPartneshipDuration(index)==1) {
                        p->dissolvePartnershipMutually(index);
                    }
                    else
                        p->reducePartneshipDuration(index);
                }}}}
    
    //<<STABLE PARTNERSHIP FORMATION>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if (modelStb){
        vector<vector<Person * >> vvAvailabilityListCSAs(_nCSAs,  vTemp); //vector of available people for partnership in each CSA (filling an empty vec)
        vector<Person*> vAvailabilityListPop;//vector of available people in the whole pop
        //1-finding eligible people
        for (auto c:_vCSAs){
            for (auto p: *c){
                if(p->isAvailableStb(rng)==true) //add the person to list of available people for that CSA
                    vvAvailabilityListCSAs[p->getCSAId()].push_back(p);
                p->setMarkedForPartnership(false);//<<reset the status from last step>>
            }}
        //2-filling a 1-Dim vector of all available people for random selection
        for (auto outer:vvAvailabilityListCSAs)
            vAvailabilityListPop.insert(vAvailabilityListPop.end(),outer.begin(),outer.end());
        //3-Looping:
        while(vAvailabilityListPop.size()>0){
            //1-get a random member of the list
            int r1=gsl_rng_uniform_int(rng,vAvailabilityListPop.size());
            Person * p=vAvailabilityListPop[r1];
            //2- remove him from the list, shorten the list
            vAvailabilityListPop[r1]=vAvailabilityListPop.back(); vAvailabilityListPop.pop_back();
            //3- if still available:
            if (!p->isMarkedForPartnership()){//if p is still available and not previously marked for partnership
                //1-choose partnership CSA:
                int g= p->returnPartnershipCSAId(rng);//(index of selected CSA
                //2-list available people for partnership in that CSA
                vector<Person*> vPotentialPartners;//populate these with currently available members of this CSA (who are not partnered yet)
                for(auto * q: vvAvailabilityListCSAs[g]){
                    if (!q->isMarkedForPartnership())
                        vPotentialPartners.push_back(q);
                }
                //3-Search through potentialPartner list
                int N=vPotentialPartners.size();
                if (N>0){
                    vector<int> vRandIndx=  returnShuffledSequence(rng,N);//a random sequence of indexes
                    for(int i=0;i<N;i++){
                        Person * q = vPotentialPartners[vRandIndx[i]];
                        if (p->evaluateStbPartnership(rng, q)){
                            p->formStbPartnership(rng, q);//forms the partnership and mark people
                            break;
                        }}}
            }
            //next person
        }
    }
    //<<CASUAL PARTNERSHIP FORMATION>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if (modelCsl){
        vector<vector<Person * >> vvAvailabilityListCSAs(_nCSAs,  vTemp); //vector of available people for partnership in each CSA (filling an empty vec)
        vector<Person*> vAvailabilityListPop;
        //1-<<CHECK AVAILABILITY>>
        for (auto c:_vCSAs){
            for (auto p: *c){
                if(p->isAvailableCsl(rng)==true) //add the person to list of available people for that CSA
                    vvAvailabilityListCSAs[p->getCSAId()].push_back(p);
                p->setMarkedForPartnership(false);
            }}
        //2-<<MAKE NEW CSL PARTNERSHIPS>>
        //Make a single (1 dim) list of all available people and loop randomly through the list  Everytime shorten the list size for the next search. remove selected people so that they don't get recounted:
        for (auto outer:vvAvailabilityListCSAs)
            vAvailabilityListPop.insert(vAvailabilityListPop.end(),outer.begin(),outer.end());
        //Looping:
        while(vAvailabilityListPop.size()>0){
            //1-get a random member of the list
            int r1=gsl_rng_uniform_int(rng,vAvailabilityListPop.size());
            Person * p=vAvailabilityListPop[r1];
            //2- remove him from the list, shorten the list
            vAvailabilityListPop[r1]=vAvailabilityListPop.back(); vAvailabilityListPop.pop_back();
            //3- if available:
            if (!p->isMarkedForPartnership()){//if p is still available and not previously marked for partnership
                //1-choose partnership CSA:
                int g= p->returnPartnershipCSAId(rng);//(index of selected CSA
                //2-list available people for partnership in that CSA
                vector<Person*> vPotentialPartners;//populate these with currently available members of this CSA (who are not partnered yet)
                for(auto * q: vvAvailabilityListCSAs[g]){
                    if (!q->isMarkedForPartnership())
                        vPotentialPartners.push_back(q);
                }
                //3-Search through potentialPartner list
                int N=vPotentialPartners.size();
                if (N>0){
                    vector<int> vRandIndx=  returnShuffledSequence(rng,N);//a random sequence of indexes
                    for(int i=0;i<N;i++){
                        Person * q = vPotentialPartners[vRandIndx[i]];
                        if (p->evaluateCslPartnership(rng, q)){
                            p->formCslPartnership(rng, q);//forms the partnership and mark people
                            break;
                        }}}
            }}
    }
};

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// HIV NATURAL HISTORY------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Population::modelDiseaseProgress(gsl_rng * rng){//checks HIV mortality, test&Care possibilities, or progress
    for (auto c: _vCSAs){
        vector<int> vDeadInx;        //temporary vector of index of those dying in this CSA
        int idx=0;
        for(auto p: *c){
            int dState=p->getDiseaseState();
            switch (dState){
                case 0://Susceptible
                    break;
                case 1://Acute
                    progressState1(rng,p);
                    break;
                case 2://Chronic
                    progressState2(rng,p);
                    break;
                case 3://AIDS
                    progressState3(rng, p);
                    break;
                case 4://ON ART-partial Suppression
                    progressState4(rng,p);
                    break;
                case 5://ON ART-partial Suppression
                    progressState5(rng,p);
                    break;
                default:
                    cout<<"Unrecocgnized health state in modelDiseaseProgress";
                    int x=cin.get();
                    break;
            }
            if (p->isMarkedDead()){  // marked dead due to HIV/AIDS
                vDeadInx.push_back(idx);
                // if (stats->getTime()>RECORD_STAT_TIME) stats->recordStat_InfoAtDeath(p);
                //   cout<<stats->getTime()-p->getEn(1)<<" , '";
                stats->recordStat_HIVmortality();
            }
            idx++;
        }
        int nDeaths=vDeadInx.size(); //number of deaths at this timestep
        
        //for killing people, we should start from the end of the list and move backward since in each itteration, we reduce the list's size by one and otherwise it wil change the indexes of dead people
        for(int i=nDeaths;i>0;i--){
            int n=vDeadInx[i-1];
            (*c)[n]->kill();
            c->deleteMember(n);//deletes this member completely
        }
    }
}

bool Population::progressState1(gsl_rng * rng, Person * p){//Progress acute infection: model mortality, testing, link to care, or progress to chronic
    //1- Disease mortality
    bool b = p->willDieHIV(rng);
    if (b)
        return true; //stop here
    
    //else:
    int cState=p->getCareState();
    switch (cState){
        case 0: {            //undiagnosed
            if (p->willTestForHIV(rng)){                //will get tested for HIV and is diagnosed//                //will immediately link to care ?
                if(p->willLinkToCare(rng,true)){ //true if linkedToCare (go on partial suppression State4), false if stayed unlinked
                    bool b =p->willGoOnART(rng);
                    if (b)
                        return b; //stop here (DS=>4,CS=>2)
                    //else: dignosed but didn't link to care so stays here (CS=>1)
                }}
            //Same DS:  didn't test for HIV, or tested but didn't linked, or linked but is Off ART
            break;
        }
        case 1: {            //diag & Notlink
            if(p->willLinkToCare(rng,false)){//true if linkedToCare(go on partial suppression State4), false if stayed unlinked
                bool b=p->willGoOnART(rng);
                if (b)
                    return b; //stop here (DS=>4,CS=>2)
            }
            //Same DS: didn't link to care, or linked but not on ART
            break;
        }
        case 3: {            //linked & offART
            bool b=p->willReinitiateART(rng);
            if (b)
                return b; //stop here (DS=>4,CS=>2)
            //stays off ART
            break;
        }
        default:{
            cout<<"bad call in progressState1!"<<endl;  //case 2,3 dose not apply
            int x=cin.get();
            break;
        }
    }
    //Else: if there was no change in state , check duration: stay or progress to next state
    return p->willDSProgress(rng);
}

bool Population::progressState2(gsl_rng * rng, Person * p){//Progress chronic infection: model mortality
    bool b = p->willDieHIV(rng);
    if (b)
        return true; //stop here
    //Else://if still alive
    int cState=p->getCareState();
    switch (cState){
        case 0: {            //undiagnosed
            if (p->willTestForHIV(rng)){                //will get tested for HIV and is diagnosed//                //will immediately link to care ?
                if(p->willLinkToCare(rng,true)){ //true if linkedToCare (go on partial suppression State4), false if stayed unlinked
                    bool b =p->willGoOnART(rng);
                    if (b)
                        return b; //stop here (DS=>4,CS=>2)
                    //else: dignosed but didn't link to care so stays here (CS=>1)
                }}
            //Same DS:  didn't test for HIV, or tested but didn't linked, or linked but is Off ART
            break;
        }
        case 1: {            //diag & Notlink
            if(p->willLinkToCare(rng,false)){//true if linkedToCare(go on partial suppression State4), false if stayed unlinked
                bool b=p->willGoOnART(rng);
                if (b)
                    return b; //stop here (DS=>4,CS=>2)
            }
            //Same DS: didn't link to care, or linked but not on ART
            break;
        }
        case 3: {            //linked & offART
            bool b=p->willReinitiateART(rng);
            if (b)
                return b; //stop here (DS=>4,CS=>2)
            //stays off ART
            break;
        }
        default:{
            cout<<"bad call in progressState2!"<<endl;   //case 2 is not chronic
            int x=cin.get();
            break;
        }
    }
    //Else//if still in the same state
    return p->willDSProgress(rng);
}

bool Population::progressState3(gsl_rng * rng, Person * p){//Progress late infection: model mortality
    bool b = p->willDieHIV(rng);
    if (b)
        return true; //stop here
    //Else //if still alive
    int cState=p->getCareState();
    switch (cState){
        case 0: {            //undiagnosed
            if (p->willTestForHIV(rng)){                //will get tested for HIV and is diagnosed//                //will immediately link to care ?
                if(p->willLinkToCare(rng,true)){ //true if linkedToCare (go on partial suppression State4), false if stayed unlinked
                    bool b =p->willGoOnART(rng);
                    if (b)
                        return b; //stop here (DS=>4,CS=>2)
                    //else: dignosed but didn't link to care so stays here (CS=>1)
                }}
            //Same DS:  didn't test for HIV, or tested but didn't linked, or linked but is Off ART
            break;
        }
        case 1: {            //diag & Notlink
            if(p->willLinkToCare(rng,false)){//true if linkedToCare(go on partial suppression State4), false if stayed unlinked
                bool b=p->willGoOnART(rng);
                if (b)
                    return b; //stop here (DS=>4,CS=>2)
            }
            //Same DS: didn't link to care, or linked but not on ART
            break;
        }
        case 3: {            //linked & offART
            bool b=p->willReinitiateART(rng);
            if (b)
                return b; //stop here (DS=>4,CS=>2)
            //stays off ART
            break;
        }
        default:{
            cout<<"bad call progressState3!"<<endl;  //case 3 is not chronic
            int x=cin.get();
            break;}
    }
    //else //if still in the same state
    return p->willDSProgress(rng);
}

bool Population::progressState4(gsl_rng * rng, Person * p){//Progress partial suppression
    bool b = p->willDieHIV(rng);
    if (b)
        return true; //stop here
    //else //if still alive
    b=p->willLoseAdhART(rng);
    if (b)
        return true; //changes the DS
    //else //if still in the same state
    return p->willDSProgress(rng);
}

bool Population::progressState5(gsl_rng * rng, Person * p){//Progress full suppression
    bool b = p->willDieHIV(rng);
    if (b)
        return true; //stop here
    //else //if still alive
    return p->willLoseAdhART(rng);
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//EPIDEMIOLOGICAL MODULE: TRANMISSION ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Population::modelTranmsission (gsl_rng * rng){
    vector<Person *> vMarkedForTransmission;
    for (auto c:_vCSAs){
        for (auto p: *c){
            if (p->getDiseaseState()>0){ // if the person is infectious (state 1,2,3,4, and 5?)
                for (auto s: *p){//s == Partnership
                    Person *q = s->getPartner();
                    if((q->getDiseaseState()==0)&& (!(q->isMarkedForTrans() ))){//partner is susceptible and not marked
                        double ptrans =
                        p->getInfectiousness() *
                        (1- q->getImmunity())*
                        cTRANS;
                        
                        if (gsl_rng_uniform(rng)<ptrans){  //if successful, mark q for tranmission and change his status at the end of this process
                            vMarkedForTransmission.push_back(q);
                            q->setMarkedForTrans(true);
                            // if (stats->getTime()>RECORD_STAT_TIME) {
                            //     stats->recordStat_InfoAtInf(q);
                            //     stats->recordStat_InfoAtTrans(p,q);
                            // }
                            p->addNumTrans();
                        }}}
            }}}
    //Model Infection for those marked for transmission
    for (auto p: vMarkedForTransmission){
        p->enterDS1(rng);
    }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//PREP-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Population::updatePrepParameters(gsl_rng * rng,vector<double> prepInfo){
    //set population params
    nPREP_SCENARIO=prepInfo[0];
    //duration is defined as a variable inside loops
    nPREP_UNITS=prepInfo[2];
    double pPREP_ADH=prepInfo[3];
    double pPREP_DROPOUT=prepInfo[4];
 //set mp: for new people
    mp_local->setD("pPREP_ADH", pPREP_ADH);
        mp_local->setD("pPREP_DROPOUT", pPREP_DROPOUT);
    //set person params: for current people
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            p->assign_prepParams(rng, pPREP_ADH,pPREP_DROPOUT);
        }}
}



void Population::modelPrEP(gsl_rng * rng,vector<double> prepInfo){ //{scenario, duration, units, adherence,dropout}
    int duration=prepInfo[1];
    //###### PREP STARTS
    if (_tPREP_start<0) {
        updatePrepParameters(rng,prepInfo);
        _tPREP_start=stats->getYear();
        }
    
    //###### PREP HAS ENDED
    if (stats->getYear()>_tPREP_start+duration){
    }
    
    //###### STOP PREP HERE
    else{
        if (stats->getYear()==_tPREP_start+duration){ //Last year: prep stops here
            for (auto * c:_vCSAs){
                for(auto * p: *c){
                    p->goOffPrep();
                }}
            //vector<double> vec={0,0,0};//totalEligibles onPrep toEnrol pCoverage
            //stats->recordStat_PrepInfo(vec);
        }
        
        //###### PREP STARTS HERE OR CONTINUES
        else{
            //enrollment based on scenario, units available
            vector<double> vec=  prepAdminsitration(rng, nPREP_SCENARIO, nPREP_UNITS);
            stats->recordStat_PrepInfo(vec);
        }}
}

vector<double> Population::prepAdminsitration(gsl_rng * rng,int nScenario, int nUnitis){
    vector<Person *> vOnPrep; //vector of those staying on PrEP
    vector<Person *> vEligibles;
    //1-evaluating eligibility for starting or staying on prep
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            if (p->isOnPrep()){//will dropout? if not, still eligible to stay on it?
                if (p->willDropoutPrep(rng))
                    p->goOffPrep();
                else{
                    if (p->isPrepEligible(nScenario))
                        vOnPrep.push_back(p);
                    else //not eligible anymore
                        p->goOffPrep();
                }
            }
            else{//if p is not currently on prep
                if (p->isPrepEligible(nScenario))
                    vEligibles.push_back(p);
            }}}
    // 2-modeling new enrollments--------------------
    int nElig0=vEligibles.size();
    int nOnPrep0=vOnPrep.size();
    int nOnPrep1=0;
    int nRemain= nPREP_UNITS-nOnPrep0;
    if (nRemain>0){//more units available:
        if (nRemain<nElig0){ //a subgroup of eligibles go on prep
            for ( int i=0;i<nRemain;i++){
                int r=gsl_rng_uniform_int(rng, vEligibles.size());
                vEligibles[r]->goOnPrep();
                vEligibles[r]=vEligibles.back();
                vEligibles.pop_back();
            }
            nOnPrep1=nOnPrep0+nRemain;
            nRemain=0;
        }
        else {//enroll all
            for (Person * p: vEligibles)
                p->goOnPrep();
            nOnPrep1=nOnPrep0+nElig0;
            nRemain=nRemain-nElig0;
        }
    }
    vector<double> vec={(double) nElig0+nOnPrep0,(double)nOnPrep0,(double)nOnPrep1};//
    //printVector(vec);
    return vec;
}

double Population::returnPrEPprops(){//pro of total pop on PrEP
    double p=0;
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            if (p->isOnPrep()){
                p++;
            }}}
    int n=getNumPeople();
    return (p/n);
}


//PUTPUT HELPER FUNCTIONS==========================================================================================================================================
//-------------
vector<double> Population:: returnCSADiseaseDist(int cId){//returns DiseaseDist vector CSA cId {"popSize", "D1","D2","D3","D4","D5","D6"}
    vector<double> vRes;//7
    vector<int> vDis(6,0);
    
    CSA * c= (_vCSAs)[cId];
    for (auto * p: *c){
        vDis[p->getDiseaseState()]++;
    }
    double nPop=c->getPopSize();//population size
    vRes.insert(vRes.end(),vDis.begin(),vDis.end());
    //convert to proportions
    for (int i=1;i<7;i++) vRes[i]=vRes[i]/nPop;
    
    return vRes;
}

vector<double> Population::returnCSAdisaseDist(){
    vector<double> res(nCSAs,0);
    int i=0;
    int N=0;
    for (auto * c: *this){
        for(auto * p: *c){
            if (p->getDiseaseState()>0) {
                res[i]++;
                N++;
            }}
        i++;}
    return vectorDivision(res,N);
}
//-------------
vector<double> Population::returnDSsizes(){//{"nDS0", "nDS1", "nDS2", "nDS3", "nDS4", "nDS5" }
    vector<double>  res(6,0);
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            res[p->getDiseaseState()]++;
        }}
    return res;
}
vector<double> Population::returnCSsizes(){//{"nCS0", "nCS1", "nCS2", "nCS3"}
    vector<double>  res(4,0);
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            if (p->getDiseaseState()>0)
                res[p->getCareState()]++;
        }
    }
    return res;
}

vector<double> Population::returnCSprops(){///prop of infected population in each care state  {"pCS0", "pCS1", "pCS2", "pCS3"}
    vector<double>  freq=returnCSsizes();
    double inf=vectorSum(freq);
    return vectorDivision(freq,inf );
}

vector<vector<double>> Population::returnDSCSprops(){
    vector<vector<int>>  res(4,vector<int>(6,0));
    int n=0;
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            if (p->getDiseaseState()>0){
                n++;
                res[p->getCareState()][p->getDiseaseState()]++;
            }}}
    return vectorDivision(res,n);
}

//-------------
int Population::returnPrevalence(){//
    int i=0;
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            if(p->getDiseaseState()>0) i++;
        }}
    return i;
}
vector<double> Population::returnPrev_ageRace(){
    vector<double> res=vector<double> (24,0);//12 ages for white, 12 for blacks
    for(auto c:_vCSAs){
        for(auto p:*c){
            if (p->getDiseaseState()>0){
                res[p->getRace()*12+ p->getAgeGroup()]++;
            }}}
    return res;
}

vector<double> Population::returnPrevInfo(){ //returns prev and pro in B
    double i=0;
    int b=0;
    for (auto * c:_vCSAs){
        for(auto * p: *c){
            if(p->getDiseaseState()>0) {
                i++;
                if (p->getRace()==1) b++;
            }
        }}
    vector<double > res={i, b/i};
    return res;
}

//--------------
vector<vector<int>> Population::returnFreqPartAgeGroup_stb( ){//STB: returns a 2D vector containing the number of STB partners cumulative for each person falling in each age-group
    vector<vector<int>> res;
    vector<int> temp;
    res.assign(vAGE_GROUP_LIMITS.size(),temp);//like an array
    for (auto * c:_vCSAs){
        for(auto p:*c){
            res[p->getAgeGroup()].push_back(p->getNumStbPartCum());
        }
    }
    return res;
}
vector<vector<int>> Population::returnFreqPartAgeGroup_all( ){//ALL: returns a 2D vector containing the number of ALL partners cumulative for each person falling in each age-group
    vector<vector<int>> res;
    vector<int> temp;
    res.assign(vAGE_GROUP_LIMITS.size(),temp);//like an array
    for (auto * c:_vCSAs){
        for(auto p:*c){
            res[p->getAgeGroup()].push_back(p->getNumCslPartCum()+p->getNumStbPartCum());
        }
    }
    return res;
}

void Population::resetCumPartnerships(){
    for (auto * c:_vCSAs){
        for(auto p:*c){
            p->setNumCslPartCum(p->getNumCslPart());
            p->setNumStbPartCum(p->getNumStbPart());
        }}
}

//=================================================-=============================================================================================================================================
//OUTPUT FUNCTIONS: Generating output templates=========================================================-============================================================================================
//=================================================-=============================================================================================================================================
vector<double> Population::returnOutput_DemographicsPop(bool printNames ,bool ageDist){//{"popSize", "bProp", "Age1","Age2","Age3","Age4","Age5","Age6","Age7","Age8","Age9","Age10","Age11"}
    if (printNames) cout<<"year popSize bProp Age1 Age2 Age3 ge4 Age5 Age6 Age7 Age8"<<endl;
    vector<double> vRes;//(10,0);
    vector<int> vAge(vAGE_GROUP_LIMITS.size(),0);
    int nB=0;
    //loop through whole pop, record info
    for (auto * c: _vCSAs){
        for (auto * p: *c){
            nB = nB + p->getRace();
            if (ageDist)   vAge[p->getAgeGroup()]++;
        }}
    double nPop=getNumPeople();
    vRes.push_back(stats->getYear());
    vRes.push_back(nPop);
    vRes.push_back(nB);
    vRes.insert(vRes.end(),vAge.begin(),vAge.end());
    //convert to proportions
    for (int i=2;i<vRes.size();i++) vRes[i]=vRes[i]/nPop;
    return vRes;
}
vector<vector<double>> Population::returnOutput_DemographicsCSA(bool printNames ){//{"CSAId size nB Age1 Age2 Age3 Age4 Age5 Age6 Age7 Age8 Age9 Age10 Age11"}
    if (printNames) cout<<"CSAId size nB Age1 Age2 Age3 Age4 Age5 Age6 Age7 Age8 Age9 Age10 Age11 "<< endl;
    vector<vector<double>> vvRes;
    for (CSA * c:*this){
        vector<int> vAge(vAGE_GROUP_LIMITS.size(),0);
        double N=c->getPopSize();
        int nB=0;
        int sumAge=0;
        for (auto * p: *c){
            nB = nB + p->getRace();
            vAge[p->getAgeGroup()]++;
        }
        vector<double> temp;
        temp.push_back(c->getId());
        temp.push_back(N);
        temp.push_back(nB);
        temp.insert(temp.end(),vAge.begin(),vAge.end());
        vvRes.push_back(temp);
    }
    
    return vvRes;
}

//============================================================================================================================================================================
vector<double> Population::returnOutput_Partnerships(bool printNames,bool reset){ //"freq0 freq1 freq2 freq3 freq4 freq5+ pStbOnlyL12M pCslOnlyL12M pStbCslL12M pSingleEachWeek pStbOnlyEachWeek pCslOnlyEachWeek pStbCslEachWeek propInStb"
    //1- dist of total partnerships [0,...5+]
    //2-prp of those in a partnerhsip and in stbOnly, cslOnly , stb/csl partnership through last year
    //3-prop in stb,cs,stb/csl now
    //4-4mean number of partnership in each age group
    if (printNames) cout<<"year freq0 freq1 freq2 freq3 freq4 freq5+ pStbOnlyL12M pCslOnlyL12M pStbCslL12M pStbNow "<<endl;
    
    vector<int> freq(6,0);
    int n=0;
    int stbonly=0;
    int cslonly=0;
    int stbCsl=0;
    int stbnow=0;
    for(auto c:_vCSAs){
        for(auto p:*c){
            int s=p->getNumStbPartCum();
            int c=p->getNumCslPartCum();
            n=s+c;//total partnerships in L12M
            //freq
            if (n<5) freq[n]++;
            else freq[5]++;
            //type
            if (s>0 && c>0) stbCsl++;
            else{
                if (s==0 && c>0 ) cslonly++;
                else
                    if (s>0 && c==0) stbonly++;
            }
            //stbnow
            if (p->getNumStbPart()>0) stbnow++;
        }}
    vector<double> res;
    res.push_back(stats->getYear());
    //frequencies
    double pop=getNumPeople();
    vector<double> temp=vectorDivision(freq,pop);
    res.insert(res.end(),temp.begin(),temp.end());
    //partnership types
    pop=(stbonly+cslonly+stbCsl); //dom changes: i.e; prop of those repoted stable-only partnership in pool of everyone who's reported any partnership through last year
    res.push_back(stbonly/pop);
    res.push_back(cslonly/pop);
    res.push_back(stbCsl/pop);
    //
    res.push_back(stbnow/pop);
    
    if (reset) resetCumPartnerships();
    return res;
}

//============================================================================================================================================================================
vector<double> Population::returnOutputs_HIVmeasures(bool printNames){//a vector of stats reported annually
    if (printNames)
        cout<<"year nPop nInc nPrev nDiagHIV nDiagAIDS nMortHIV pEverGotAIDS pDS1 pDS2 pDS3 pDS4 pDS5  pDiag_inf_inst pLinked_Diag_inst pOnART_Linked_inst pSupp_onART_inst pOnART_cum popWhite prevHIVWhite propPrevByWhite pDS123_W pDS4_W pDS5_W pDiag_W pOnART_W pSupp_W popBlack prevHIVBlacks propPrevByBlack pDS123_B pDS4_B pDS5_B pDiag_B pOnART_B pSupp_B prep1 prep2 prep3 prep4 prep5"<<endl;
    
    int nPop=getNumPeople();//population size
    int nPopB=0;//population black
    int nEverGotAIDS=0;//cum number of those with a history of AIDS (not necessarily diagnosed)
    vector<double> temp(6,0);    vector<vector<double>> vvDS(2,temp);//DS sizes in each race
    temp={0,0,0,0,0};     vector<vector<double>> vvCS(2,temp);//CS sizes in each race //last element for those onARTduring last year
    
    for(auto c:_vCSAs){
        for(auto p:*c){
            if (p->getRace()==1) nPopB++;
            //
            if(p->getDiseaseState()>0){//for infected population!
                if(p->getGotAIDS()) nEverGotAIDS++;//the true value (not only for those who're diagnosed with AIDS)!!!
                //
                if (p->getRace()==1) {//record for black
                    vvDS[1][p->getDiseaseState()]++;
                    vvCS[1][p->getCareState()]++;
                    if ((p->getCareState()==2)||((stats->getTime() - p->getARTBeginTime()) <SIMWEEKS))
                        vvCS[1][4]++;//has been on ART during this year
                }
                else{
                    vvDS[0][p->getDiseaseState()]++;
                    vvCS[0][p->getCareState()]++;
                    if ((p->getCareState()==2)||((stats->getTime() - p->getARTBeginTime()) <SIMWEEKS))
                        vvCS[0][4]++;
                }
            }
        }}
    //inidence prev:
    int inc= stats->returnStat_nIncHIV();//incidence this year
    double prev= vectorSum(vvDS);//number living with HIV
    double prev0= vectorSum(vvDS[0]);//white
    double prev1= vectorSum(vvDS[1]);//black
    //conditionals
    double pDiag_Inf_inst=  (vvCS[0][1]+vvCS[0][2]+vvCS[0][3]+vvCS[1][1]+vvCS[1][2]+vvCS[1][3])/prev;
    double pLinked_Diag_inst= (vvCS[0][2]+vvCS[0][3]+vvCS[1][2]+vvCS[1][3])/prev ;
    double pOnART_Linked_inst= (vvCS[0][2]+vvCS[1][2])/prev;
    double pSupp_onART_inst= (vvDS[0][5]+vvDS[1][5])/prev;
    double pOnART_cum= (vvCS[0][4]+vvCS[1][4])/prev;
    //whites
    double pDiag0=  (vvCS[0][1]+vvCS[0][2]+vvCS[0][3])/prev0;
    double pSupp0= (vvDS[0][5] )/prev0;
    double pOnART0= (vvCS[0][4] )/prev0;
    //blacks
    double pDiag1=  (vvCS[1][1]+vvCS[1][2]+vvCS[1][3])/prev1;
    double pSupp1= (vvDS[1][5] )/prev1;
    double pOnART1= (vvCS[1][4] )/prev1;
    //----------------------
    vector<double> res;
    res.push_back(stats->getYear());//0
    res.push_back(nPop);
    res.push_back(inc);//2
    res.push_back(prev);
    res.push_back(stats->returnStat_nDiagHIV());//4
    res.push_back(stats->returnStat_nDiagAIDS());
    res.push_back(stats->returnStat_nHIVmortality());//6
    res.push_back(nEverGotAIDS/prev);//7
    // res.push_back(-1);
    //prp of infected populaiton in each DS
    temp= vectorDivision(vectorSumRows(vvDS),prev);
    res.insert(res.end(),temp.begin()+1,temp.end());//excluding DS0 //8-12
    //res.push_back(-1);
    //  res.push_back(-1);
    //
    res.push_back(pDiag_Inf_inst);//13
    res.push_back(pLinked_Diag_inst);
    res.push_back(pOnART_Linked_inst);
    res.push_back(pSupp_onART_inst);
    res.push_back(pOnART_cum );//17
    
    //whites
    res.push_back(nPop-nPopB);//population //18
    res.push_back((prev0)/(nPop-nPopB));//prevelance in white population
    
    res.push_back(prev0/prev);//propor of total prv by whites
    res.push_back((vvDS[0][1]+vvDS[0][2]+vvDS[0][3])/prev0);
    res.push_back(vvDS[0][4]/prev0);
    res.push_back(vvDS[0][5]/prev0);
    res.push_back(pDiag0);
    res.push_back(pOnART0);
    res.push_back(pSupp0);//26
    //blacks
    res.push_back(nPopB);//27
    res.push_back((prev1)/(nPopB));//prevelance in whites
    
    res.push_back(prev1/prev);//propor of prv in blacks
    res.push_back((vvDS[1][1]+vvDS[1][2]+vvDS[1][3])/prev1);
    res.push_back(vvDS[1][4]/prev1);
    res.push_back(vvDS[1][5]/prev1 );
    res.push_back(pDiag1);
    res.push_back(pOnART1);
    res.push_back(pSupp1);//35
    //prep
    temp=stats->returnStat_PrepInfo().back();
    res.insert(res.end(),temp.begin(),temp.end());//40
    
    
    return res;
}

vector<double> Population::returnOutputs_PrevDemog(bool printNames){
    if (printNames) cout<<" pAge1 pAge2 pAge3 pAge4 pAge5 pAge6 pAge7 pAge8 pRaceB"<<endl;
    
    vector<double> res;
    //demog-prev age structure-race
    vector<vector<double>> demog(2,vector<double> (8,0));
    double prev=0;
    for(auto c:_vCSAs){// "prevelance:  pAge1 pAge2 pAge3 pAge4 pAge5 pAge6 pAge7 pAge8 pRaceB
        //nPrev dAge1 dAge2 dAge3 dAge4 dAge5 dAge6 dAge7 dAge8 ds0Diag ds1Diag ds2Diag ds3Diag tDiag-Inf iAge1 iAge2 iAge3 iAge4 iAge5 iAge6 iAge7 iAge8"
        for(auto p:*c){
            if(p->getDiseaseState()>0){
                prev++;
                demog[p->getRace()][p->getAgeGroup()]++;
            }}}
    demog= vectorDivision(demog, prev);
    res= vectorSumRows(demog);//prop prev in each age-group
    res.push_back( vectorSum(demog[1]))  ; //prop of prev among black
    res.push_back(prev);
    return res;
}

//============================================================================================================================================================================
vector<double> Population::returnSAoutputs(){ //prev, prev in b, pSupp, numPartnerships>1
    vector<double> res;
    
    double prev=0;
    double prevB=0;
    double sup=0;
    double pop=0;//number of people
    double part1=0;//num people with more than one partner in the past year
    for(auto c:_vCSAs){
        for(auto p:*c){
            //disease
            if(p->getDiseaseState()>0){
                prev++;
                if(p->getRace()==1) prevB++;
                if ((p->getCareState()==2)||((stats->getTime() - p->getARTBeginTime()) <SIMWEEKS))
                    sup++;//is or has been on ART during this year
            }
            //partnerships
            pop++;
            int s=p->getNumCslPartCum()+p->getNumStbPartCum();
            if (s>1) part1++;
            
        }}
    res.push_back(prev);
    res.push_back(prevB/prev);
    res.push_back(sup/prev);
    res.push_back(part1/pop);
    return res;
}


//============================================================================================================================================================================
void Population::debug(){
    
}


void Population::debug_races(){
    double n1=0;    double n0=0;
    double I0=0;    double I1=0;
    double Inf0=0;    double Inf1=0;
    double p0=0;double p1=0;
    double nt0=0; double nt1=0;
    double ds10=0; double ds11=0;
    for(auto c:_vCSAs){
        for(auto p:*c){
            if (p->getRace()==0) {
                n0++;
                nt1=nt1+p->getNumTrans();
                if(p->getDiseaseState()>0){
                    I0++;
                    if(p->getDiseaseState()<4) Inf0++;
                    p0=p0+ p->getNumStbPartCum()+p->getNumCslPartCum();
                }
            }
            else {
                n1++;
                nt0=nt0+p->getNumTrans();
                
                if(p->getDiseaseState()>0){
                    I1++;
                    if(p->getDiseaseState()<4) Inf1++;
                    p1=p1+ p->getNumStbPartCum()+p->getNumCslPartCum();
                    
                }
            }
        }}
    printVector(vector<double> {n0,n1,I0,I1, 0,I1/(I0+I1),0, I0/n0,I1/n1,0,0,p0,p1, p0/I0,p1/I1 });
}


