//----
//---------------------------------------------------
//
/* class Driver
 *
 *this class contains wrapper functions for creating a simulation, running a full year, implementing PrEP and sensitivity analysis.
 *
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "Driver.h"
#include "Population.h"
#include "Functions.h"
#include "GlobalParams.h"
#include "Person.h"
#include "Stats.h"
#include "ModParam.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <string>

using namespace std; //for std::vector
using namespace GLOBALPARAMS;
using namespace FUNCTIONS;
//using namespace GLOBALSTATS;

Driver::Driver(){
    rng = gsl_rng_alloc(gsl_rng_taus2);    //initializing RNG, default seed
    gsl_rng_set (rng, time(NULL));
    //create a SUS pop
    ModParam mp("defaults.cfg");
    vector<double> hiv0(nCSAs,0);
    pop= new Population(rng,  &mp, nCSAs,vCSA_POP0,vCSA_RACE_BLACK_PROP0,vvCSA_AGEGROUP0);
}

void Driver::reset(){
    delete pop;
    rng = gsl_rng_alloc(gsl_rng_taus2);    //initializing RNG, default seed
    gsl_rng_set (rng, time(NULL));
    //create a SUS pop
    ModParam mp("defaults.cfg");
    vector<double> hiv0(nCSAs,0);
    pop= new Population(rng,  &mp, nCSAs,vCSA_POP0,vCSA_RACE_BLACK_PROP0,vvCSA_AGEGROUP0);
    
}

Driver::Driver(ModParam * modelParams){
    rng = gsl_rng_alloc(gsl_rng_taus2);    //initializing RNG, default seed
    gsl_rng_set (rng, time(NULL));
    //create a SUS pop
    mp_local=modelParams;
    vector<double> hiv0(nCSAs,0);
    pop= new Population(rng,mp_local, nCSAs,vCSA_POP0,vCSA_RACE_BLACK_PROP0,vvCSA_AGEGROUP0);
}
Driver::Driver(int tID, ModParam * modelParams){
    rng = gsl_rng_alloc(gsl_rng_taus2);    //initializing RNG, default seed
    gsl_rng_set (rng, time(NULL)*tID);
    //create a SUS pop
    mp_local=modelParams;
    vector<double> hiv0(nCSAs,0);
    pop= new Population(rng,mp_local, nCSAs,vCSA_POP0,vCSA_RACE_BLACK_PROP0,vvCSA_AGEGROUP0);
}
Driver::Driver( const Driver& src )
: rng(src.rng),mp_local(src.mp_local){
    pop= new Population(*src.pop);
}
Driver::~Driver(){
    //delete all CSA members
    delete pop;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//RUNNING----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Driver::stepYear(bool modelNaturalDyn, bool modelPartnership,bool modelDiseaseProg,bool modelTranmission  ){//models oneYear of Sim.
    yearBegin();
    yearRun(modelPartnership, modelDiseaseProg, modelTranmission);
    yearEnd(modelNaturalDyn);
}

void Driver::yearBegin(){//models oneYear of Sim.
}
void Driver::yearRun( bool modelPartnership,bool modelDiseaseProg,bool modelTranmission ){//models oneYear of Sim.
    for (int i=0;i<SIMWEEKS;i++){
        if(modelPartnership)        pop->modelPartnerships(rng, true,true,true);
        if(modelTranmission)        pop->modelTranmsission(rng);
        if(modelDiseaseProg)        pop->modelDiseaseProgress(rng);
        pop->stats->incTime();
    }
}
void Driver::yearEnd(bool modelNaturalDyn ){//models oneYear of Sim.
    //end of year:
    if(modelNaturalDyn)     pop->modelNaturalDynamics(rng);
}

void Driver::burnin( int demogBurnin, int partBurnin, int epidemicBurnin){//bring the mdoel to steady state
    //<<BURN IN>>----
    bool DBG=true;
    if (DBG) cout<<"Burn in started"<<endl;
    pop->stats->resetAllStats();
    // if (DBG) cout<<"Demog burn in ...."<<endl;
    for (int j=0;j<demogBurnin;j++){
        stepYear(true, false, false,false);//runs for burnoout + 1 year
    }
    //if (DBG)     cout<<"Partnership burn in ...."<<endl;
    for (int j=0;j<partBurnin;j++){
        stepYear(true, true, false,false);//runs for burnoout + 1 year
    }
    pop->stats->resetAllStats();    pop->resetCumPartnerships();
    //    if (DBG)     cout<<"Epidemic started..."<<endl;
    pop->setEpidemic(rng);
    pop->setInitialDSCS(rng);
    //  if (DBG)  printVector(pop->returnOutputs_HIVmeasures(true), "Intitial values:---- ");
    pop->stats->resetAllStats();
    //    if (DBG)     cout<<"Infection burn in ..."<<endl;
    for (int j=0;j<epidemicBurnin;j++){
        stepYear(true, true, true,true);
        //     printVector(pop->returnOutputs_HIVmeasures(false));
        //     pop->stats->resetAllStats();
    }
    pop->stats->resetAllStats();
    pop->resetCumPartnerships();
    if (DBG) cout<<"Burn in ends"<<endl;
    
}

void Driver::initilizeEpidemic(){
    pop->setEpidemic(rng);
    pop->setInitialDSCS(rng);
    
}
//EXPERIMENTATION---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<double> Driver::runModel_Inc(int years ){//takes a steadystate model, runs and records the annual incidence
    vector<double> vIncYears;
    for (int y=0;y<years;y++){
        yearBegin();
        yearRun(true,true,true);
        //outputs:
        vIncYears.push_back(pop->stats->returnStat_nIncHIV());
        yearEnd(true);
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    return vIncYears;
}
vector<double> Driver::modelRun_prev(int simTime){//runs model and return {prev,Inc}
    vector<double> vRes;
    for (int j=0;j<simTime;j++){
        yearBegin();
        yearRun(true,true,true);
        //outputs:
        vRes.push_back(pop->returnPrevalence());
        yearEnd(true);
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    return vRes;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//PREP---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<vector<double>> Driver::runPrepScenarios(vector<double> vScenarios,vector<double> vCov,vector<double> vAdh,vector<double> vDuration,vector<double> vDropout){
    // vector<double> scenario=vScenarios; //different scenarios for choosing eligible individuals
    
    int yBeforePrep=5;
    vector<vector<double>> vvRes;
    //0- run the model for a number of years, record baseline inc
    vector<double> vvBefore=runModel_Inc(yBeforePrep);
    double inc0=vvBefore.back(); //last year's incidence
    double prev0=pop->returnPrevalence();//last year prev
    //1-copy the initial population
    Population originalPop= *pop; //make a copy of this population at steady state
    //scenarios:
    for (double s:vScenarios){
        for (double c:vCov){
            for (double a:vAdh){
                for (double drp:vDropout){
                    for (double dur:vDuration) {
                        vector<double> prepInfo={s,dur,c,a,drp};//    " Duration Cov Adh dropout"
                       // printVector(prepInfo);
                        delete pop;
                        pop = new Population(originalPop);
                        //2-run prep
                        vector<vector<double>> vvAfter=runPrep(prepInfo);
                        vector<double> v0= vectorPercentImp(vvAfter[0], inc0);//incidence
                        vector<double> v1= vectorPercentImp(vvAfter[1], prev0);//prevelance
                        vector<double> v2= vvAfter[2];//prep stats
                        //3-write information and outputs
                        vector<double> vRes=prepInfo;//0-5 prep information
                        //0-inc
                        vRes.push_back(inc0);//6  last year incidences
                        vRes.push_back(0);//empty cell
                        vRes.insert(vRes.end(), v0.begin(), v0.end());
                        vRes.push_back(0);//empty cell
                        //1-prev
                        vRes.push_back(prev0);//6  last year incidences
                        vRes.push_back(0);//empty cell
                        vRes.insert(vRes.end(), v1.begin(), v1.end());
                        vRes.push_back(0);//empty cell
                        //2-prep stats
                        vRes.insert(vRes.end(), v2.begin(), v2.end());
                        vRes.push_back(0);//empty cell
                        
                        vvRes.push_back(vRes);
                        
                    }}}}}
    return vvRes;
    
}


vector<vector<double>> Driver::runPrep( vector<double> prepInfo ){//returns a few outputs //{scenario, dur, cov, adh
    vector<double> vIncYears;
    vector<double> vPrevYears;
    vector<double> vPrepStats;
    int duration=prepInfo[1]+5;
    for (int y=0;y<duration;y++){
        yearBegin();
        yearRun(true,true,true);
        //record incidence
        vIncYears.push_back(pop->stats->returnStat_nIncHIV());
        vPrevYears.push_back(pop->returnPrevalence());
        //end of year:
        pop->modelNaturalDynamics(rng);
        pop->modelPrEP(rng,prepInfo);
        //printVector( pop->stats->returnStat_PrepInfo()); cout<<endl;
        
        //reset
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    vector<vector<double>> temp=pop->stats->returnStat_PrepInfo();
    vPrepStats= pop->stats->returnStat_PrepInfo().back();//last year
    return {vIncYears,vPrevYears,vPrepStats};
}


vector<double> Driver::runPrep_Inc( vector<double> prepInfo ){//takes a steadystate model, implements the prep and records the annual incidence
    vector<double> vIncYears;
    int duration=prepInfo[1]+5;
    for (int y=0;y<duration;y++){
        yearBegin();
        yearRun(true,true,true);
        //record incidence
        vIncYears.push_back(pop->stats->returnStat_nIncHIV());
        //end of year:
        pop->modelNaturalDynamics(rng);
        pop->modelPrEP(rng,prepInfo);
        //reset
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    return vIncYears;
}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//FULL CALIBRATION---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Driver::CalibrateDemographics_annualOutputs( int calibYears, int popPrintingIntervals, int csaPrintingIntervals){//creates a single file for pop demog outputs and seperate files for CSA's demographics in the given intervals
    cout<<" <<<<<<<<<<<Calibration run for demographics- a single run>>>>>>>>>>>>"<<endl;
    string ofPop="calibDemog-pop";
    vector<string> colNames1={"popSize pBlack pAge0 Age1 Age2 Age3 Age4 Age5 Age6 Age7 Age8 Age9 Age10 Age11"};
    writeVector(colNames1,ofPop,true);//initializing the file
    writeVector(pop->returnOutput_DemographicsPop(false,true), ofPop, false);
    
    ostringstream osCSA;
    osCSA<<"calibDemog-csa"<<0;
    vector<string> colNames2={"CSAId size nB Age1 Age2 Age3 Age4 Age5 Age6 Age7 Age8 Age9 Age10 Age11 Age12"};
    writeVector(colNames2, osCSA.str(), true);
    writeVector(pop->returnOutput_DemographicsCSA(false),osCSA.str(),false);
    
    for (int i=0;i<calibYears;i++){
        ostringstream osCSAs;
        stepYear(true,false,false,false);
        if (i%popPrintingIntervals==0)
            //POP: add to the same file
            writeVector(pop->returnOutput_DemographicsPop(false, true),ofPop,false);
        if (i%csaPrintingIntervals==0){
            //CSA: new file
            osCSAs<<"calibDemog-csa"<<i;
            writeVector(colNames2, osCSAs.str(), true);
            writeVector(pop->returnOutput_DemographicsCSA(false),osCSAs.str(),false);
        }
    }
    cout<<"Demographics ran for "<<calibYears<<" years, and outputs were printed to"<<ofPop<<" and "<< "csaDemog files"<<endl;
}

void Driver::calibratePartnership_annualOutputs( int demogBurnin, int partBurnin, int steadyYears){
    cout<<" <<<<<<<<<<<Calibration run for partnership- a single run>>>>>>>>>>>>"<<endl;
    //1-overall stats
    vector<vector<double>> vvAnnualOutputs;
    //2-race mixing
    vector<vector<int>> vRaceMixingProps=vector<vector<int>> (2, vector<int> (2,0)); //race mixing proportions 2*2
    //3- age mixing: freq partnership by age
    vector<int> temp0;
    vector<vector<int>> vvFPAG=vector<vector<int>> (vAGE_GROUP_LIMITS.size(), temp0);//freq of partnerships in each age group////returns a 2D vector containing the number of partners for each person falling in each age-group
    vector<vector<int>> vvFPAG_stb=vector<vector<int>> (vAGE_GROUP_LIMITS.size(), temp0);
    
    //<<BURN IN>>------------------------------------------
    pop->stats->resetAllStats();
    cout<<"Demog burn in ...."<<endl;
    for (int j=0;j<demogBurnin;j++){
        stepYear(true, false, false,false);//runs for burnoout + 1 year
    }
    cout<<"Partnership burn in ...."<<endl;
    for (int j=0;j<partBurnin;j++){
        stepYear(true, true, false,false);//runs for burnoout + 1 year
    }
    pop->stats->resetAllStats();
    pop->resetCumPartnerships();
    
    //<<STEADY STATE>>-------------------------------------------------------------------------------------
    cout<<"Partnership running..."<<endl;
    for (int i=0;i<steadyYears;i++){ //it's important to collect partnership outputs before modeling naturalDynamics where newborns are added to the population
        yearBegin();
        yearRun( true, false,false);//runs for burnoout + 1 year
        //update fpag
        vector<vector<int>> temp=pop->returnFreqPartAgeGroup_all();
        for (int i=0;i<temp.size();i++){//lump of recorded freq in the last year
            for(int val: temp[i]){
                vvFPAG[i].push_back(val);
            }}
        temp=pop->returnFreqPartAgeGroup_stb();
        for (int i=0;i<temp.size();i++){
            for(int val:temp[i]){
                vvFPAG_stb[i].push_back(val);
            }}
        
        vvAnnualOutputs.push_back(pop->returnOutput_Partnerships(false,true));
        
        yearEnd(true);
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    
    //1-write annual outputs
    string    filename= "calibPart-summary";
    vector<string> colNames={"year freq0 freq1 freq2 freq3 freq4 freq5+ pStbOnlyL12M pCslOnlyL12M pStbCslL12M pStbNow"};
    writeVector(colNames,filename,true);
    writeVector(vvAnnualOutputs,filename, false);
    //2- race------------------------------------------------------------------------------------------------
    //report matrix of race-mixing [2*2]
    writeVector(pop->stats->returnStat_RaceMixing(), "calibPart-raceMixing", true);
    
    //3- age mixing------------------------------------------------------------------------------------------
    //report info on abs(sqrt(age)-differences
    vector<double> m= pop->stats->returnStat_AgeMixingDiffs();
    double mean= vectorAve(m);
    double std= vectorStd(m);
    double n=m.size();
    filename="calibPart-ageMixingsDiffs";
    writeFile("mean std n",filename, true);
    writeVector(vector<double> {mean,std,n},filename, false);
    writeFile("freq of ADSR",filename, false);
    vector<double> bins={0,.5,1,1.5,2,2.5,3};
    m=returnDist (m,bins);
    bins.push_back(bins.back()+10);//a final element
    writeVector(vector<vector<double>> {bins,m},filename, false);
    
    //3-1  recording ADSR for each race seperately------------------
    m= pop->stats->returnStat_ADSR_Race()[0];
    mean= vectorAve(m);std= vectorStd(m);n=m.size();
    filename="calibPart-ageMixingsDiffs_race0";
    writeFile("mean std n",filename, true);
    writeVector(vector<double> {mean,std,n},filename, false);
    writeFile("freq of ADSR",filename, false);
    m=returnDist (m,bins); bins.push_back(bins.back()+10);//a final element
    writeVector(vector<vector<double>> {bins,m},filename, false);
    //3-2  recording ADSR for each race seperately------------------
    m= pop->stats->returnStat_ADSR_Race()[1];
    mean= vectorAve(m);std= vectorStd(m);n=m.size();
    filename="calibPart-ageMixingsDiffs_race1";
    writeFile("mean std n",filename, true);
    writeVector(vector<double> {mean,std,n},filename, false);
    writeFile("freq of ADSR",filename, false);
    m=returnDist (m,bins); bins.push_back(bins.back()+10);//a final element
    writeVector(vector<vector<double>> {bins,m},filename, false);
    //4- FPAG--------------------------------------------------------------------------------------------------
    //we can compute the freq dist here and then write it to file
    //all:
    vector<vector<double>> temp;
    vector<double> vbins={0,1,2,3,4};
    //    temp.push_back(vbins);
    filename="calibPart-allPartnerFreqAgeGroups";
    for (auto vec:vvFPAG)
        temp.push_back(returnDist(vec, vbins));
    writeVector(temp,filename, true);//rows:age-groups   col:prop of people haveing n partnership in a year on average
    temp.clear();
    //stb:
    filename="calibPart-stbPartnerFreqAgeGroups";
    //    vbins={0,1,2,3,4,5};
    //    temp.push_back(vbins);
    for (auto vec:vvFPAG_stb)
        temp.push_back(returnDist(vec, vbins));
    writeVector(temp,filename, true);//rows:age-groups   col:prop of people haveing n partnership in a year on average
    
    
}

void Driver::calibrateEpidemic_annualOutputs( int steadyYears){
    //<STEADY STATE>--------------------------------------------------------------
    vector<vector<double>> vvAnnualOutputs;
    vector<vector<double>> vvAnnualPrev_ageRace;
    vector<vector<double>> vvAnnualInc_ageRace;
    vector<double> vec;
    //
    for (int j=0;j<steadyYears;j++){
        yearBegin();
        yearRun(true,true,true);
        //outputs:
        vvAnnualOutputs.push_back(pop->returnOutputs_HIVmeasures(false));
        //
        vec=pop->returnPrev_ageRace();
        vvAnnualPrev_ageRace.push_back(vectorDivision(vec,vectorSum(vec)));//prop of prev
        //
        vec=pop->stats->returnStat_incRaceAge();
        vvAnnualInc_ageRace.push_back(vectorDivision(vec,vectorSum(vec)));
        
        yearEnd(true);
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    //ANNUAL OUTPUTS----------
    vector<string> colNames={"year nPop nInc nPrev nDiagHIV nDiagAIDS nMortHIV pEverGotAIDS pDS1 pDS2 pDS3 pDS4 pDS5  pDiag_inf_inst pLinked_Diag_inst pOnART_Linked_inst pSupp_onART_inst pOnART_cum popWhite prevHIVWhite propPrevByWhite pDS123_W pDS4_W pDS5_W pDiag_W pOnART_W pSupp_W popBlack prevHIVBlacks propPrevByBlack pDS123_B pDS4_B pDS5_B pDiag_B pOnART_B pSupp_B prep1 prep2 prep3 prep4 prep5"};//TALLY: cumulative through last year TS: at the end of each week through last year (averaged)
    writeVector(colNames,"calibEpidmic-annualOutputs",true);
    writeVector(vvAnnualOutputs,"calibEpidmic-annualOutputs",false);
    
    //PREV by AGE RACE
    colNames={"PrevW1","PrevW2","PrevW3","PrevW4","PrevW5","PrevW6","PrevW7","PrevW8","PrevW9","PrevW10","PrevW11","PrevW12",
        "PrevB1","PrevB2","PrevB3","PrevB4","PrevB5","PrevB6","PrevB7","PrevB8","PrevB9","PrevB10","PrevB11","PrevB12"};
    writeVector(colNames,"calibEpidmic-prevByAgeRace",true);
    writeVector(vvAnnualPrev_ageRace,"calibEpidmic-prevByAgeRace",false);
    
    //INC by AGE RACE
    colNames={"IncW1","IncW2","IncW3","IncW4","IncW5","IncW6","IncW7","IncW8","IncW9","IncW10","IncW11","IncW12",
        "IncB1","IncB2","IncB3","IncB4","IncB5","IncB6","IncB7","IncB8","IncB9","IncB10","IncB11","IncB12"};
    writeVector(colNames,"calibEpidmic-IncByAgeRace",true);
    writeVector(vvAnnualInc_ageRace,"calibEpidmic-IncByAgeRace",false);
}

//
vector<vector<double>> Driver::calibrateModel_summaryOutputs( int steadyYears){
    //<STEADY STATE>--------------------------------------------------------------
    vector<vector<double>> vvEpiOutputs;
    vector<vector<double>> vvPrev_ageRace;
    vector<vector<double>> vvInc_ageRace;
    vector<vector<double>> vvPartnerships;
    
    vector<double> vec;
    
    //
    for (int j=0;j<steadyYears;j++){
        yearBegin();
        yearRun(true,true,true);
        //outputs:
        vvEpiOutputs.push_back(pop->returnOutputs_HIVmeasures(false));
        //
        vec=pop->returnPrev_ageRace();
        vvPrev_ageRace.push_back(vectorDivision(vec,vectorSum(vec)));//proportion of prev
        //
        vec=pop->stats->returnStat_incRaceAge();
        vvInc_ageRace.push_back(vectorDivision(vec,vectorSum(vec)));//prop of prev
        
        vvPartnerships.push_back(pop->returnOutput_Partnerships(false,true));
        
        yearEnd(true);
        pop->stats->resetAnnualStats();
        pop->resetCumPartnerships();
    }
    //ANNUAL OUTPUTS----------
    
    vector<vector<double>> vvRes;
    vvRes.push_back(vectorAveRows(vvEpiOutputs));
    vvRes.push_back(vectorAveRows(vvPrev_ageRace));
    vvRes.push_back(vectorAveRows(vvInc_ageRace));
    vvRes.push_back(vectorAveRows(vvPartnerships));
    
    return vvRes;
}

//=================================================-===================================================================================================
vector<vector<double>> Driver::SA_oneway(int burnin, int steady,vector<ModParam *> vMPs,vector<double> prepInfo){//change params one at a time
    //starts with a model in the steady state
    Population originalPop= *pop; //make a copy of this population at steady state
    ModParam * originalMp=mp_local;//baseline mp file
    
    vector<vector<double>> vvRes;//each line is a SA experiment for a specific param setting
    //loop ove all MPs
    int recipe=0;
    for (ModParam * mpSA:vMPs){
        cout<<"SA-"<<recipe<<endl;
        //<< 0-copy the population
        delete pop;
        pop = new Population(originalPop);
        //
        //<< 1-implement the SA mp file in the population
        pop->copyParamSettings(rng,mpSA);
        
        //<< 2-run for a short burnin
        for (int j=0;j<burnin;j++){
            yearBegin();
            yearRun(true,true,true);
            yearEnd(true);
            pop->stats->resetAnnualStats();
            pop->resetCumPartnerships();
        }
        //<< 3-run for steady state
        vector<vector<double>> vvSAoutputs;
        double inc=0;
        for (int j=0;j<steady;j++){
            yearBegin();
            yearRun(true,true,true);
            vvSAoutputs.push_back(pop->returnSAoutputs());
            inc=pop->stats->returnStat_nIncHIV();
            yearEnd(true);
            pop->stats->resetAnnualStats();
            pop->resetCumPartnerships();
        }
        vector<double> vSAoutputs= vectorAveRows(vvSAoutputs); //average annual values through time
        
        //<< 4-run a PrEP scenario
        vector<double> vPrepOutputs=prepInfo;// prep information
        vPrepOutputs.push_back(0);
        vPrepOutputs.push_back(inc); //inc0 (the last value recorded)
        //--run prep
        vector<vector<double>> vvAfter=runPrep( prepInfo);
        vector<double> vImp= vectorRelDiffPercent(vvAfter[0], inc);//relative improvements
        vPrepOutputs.push_back(0);//empty cell
        vPrepOutputs.insert(vPrepOutputs.end(), vImp.begin(), vImp.end());
        
        //<< 5- record outputs for this SA##########################
        vector<double> temp= mpSA->returnValueList();//SA params
        temp.push_back(0);
        temp.insert(temp.end(),vSAoutputs.begin(),vSAoutputs.end());//sa outputs
        temp.push_back(0);
        temp.insert(temp.end(),vPrepOutputs.begin(),vPrepOutputs.end());//prep outputs
        temp.push_back(0);
        temp.push_back(recipe); recipe++; //recipe id
        vvRes.push_back(temp);
        
    }
    return vvRes;
    
}

void Driver::SA_random(int burnin, int steady,vector<ModParam *> vMPs,std::mutex &simLock){//change params one at a time
    //starts with a model in the steady state
    Population originalPop= *pop; //make a copy of this population at steady state
    ModParam * originalMp=mp_local;//baseline mp file
    int recipe=0;
    //loop ove all MPs
    for (ModParam * mpSA:vMPs){
        cout<<"sa"<<endl;
        //0-copy the population
        delete pop;
        pop = new Population(originalPop);
        
        //2-implement the dummy mp file in the population
        pop->copyParamSettings(rng,mpSA);
        
        //3-run for burnin
        for (int j=0;j<burnin;j++){
            yearBegin();
            yearRun(true,true,true);
            yearEnd(true);
            pop->stats->resetAnnualStats();
            pop->resetCumPartnerships();
        }
        //4-run for steady state
        vector<vector<double>> vvOut;
        for (int j=0;j<steady;j++){
            yearBegin();
            yearRun(true,true,true);
            vvOut.push_back(pop->returnSAoutputs());
            yearEnd(true);
            pop->stats->resetAnnualStats();
            pop->resetCumPartnerships();
        }
        vector<double> vOut= vectorAveRows(vvOut); //average annual values
        
        //5- record outputs for this SA
        vector<double> vRes= mpSA->returnValueList();
        vRes.insert(vRes.end(),vOut.begin(),vOut.end());
        vRes.push_back(recipe);recipe++;
        
        //6-write output
        simLock.lock();
        writeVector(vRes, "SA_random", false);
        simLock.unlock();
    }
    //end
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//HELPERS-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<vector<double>> Driver::returnAnimationInfo(){//"TNOW pId pCSAid pDS pNumStb qId qCSAid"
    vector<double> vec;
    vector<vector<double>> res;
    for(auto * c: * pop){
        for(auto * p:*c){
            vec.push_back(pop->stats->getTime()/SIMWEEKS);
            vec.push_back(p->getId());
            vec.push_back(p->getCSAId());
            vec.push_back(p->getDiseaseState());
            vec.push_back(p->getNumStbPart());
            if (p->getNumStbPart()>0){
                for (auto * s: *p){
                    if( s->getType()==1){
                        auto * q= s->getPartner();
                        vec.push_back(q->getId());
                        vec.push_back(q->getCSAId());
                    }                }}
            else{
                vec.push_back(0);
                vec.push_back(0);
            }
            res.push_back(vec);
            vec.clear();
        }}
    return res;
}

void Driver::debugPop(){
    for (int i=0;i<10;i++){
        cout<<gsl_rng_uniform(rng)<<" ";
    }
    cout<<endl;
    // pop->modelPrEP(rng);
}




