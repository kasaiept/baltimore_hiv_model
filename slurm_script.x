#!/bin/bash -l

#SBATCH
#SBATCH --job-name=pkHivModel
#SBATCH --time=24:0:0
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24 ####Set this to number of threads, max 24 with 1 node
#SBATCH --mail-type=end ####will send an email at the end of run
#SBATCH --mail-user=pkasaie1@jhu,.edu

make clean
make -j10
./main.o
