//---------------------------------------------------
/* Driver.h
 *
 *Parastu Kasaie, Feb 2016
 *
 */
//---------------------------------------------------


#ifndef __HIVMODEL__DRIVER__
#define __HIVMODEL__DRIVER__

#include <stdio.h>
#include "Population.h"
#include "Functions.h"
#include "GlobalParams.h"
#include "Person.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include "Stats.h"
#include <mutex>
using namespace std; //for std::vector
using namespace GLOBALPARAMS;
using namespace FUNCTIONS;


//calibration measures:

class Driver{
public:
    Driver();
    void reset();
    Driver(ModParam * mp);
    Driver(int tID, ModParam * modelParams);//for initializing a random rng based on thread
    Driver( const Driver& src );
    ~Driver();
    void replacePopulation();//recreates a new pop, deletes the prev. one
    
    //MODEL RUN
    void stepYear(bool modelNaturalDyn, bool modelPartnership,bool modelDiseaseProg,bool modelTranmission);//models one Year of Simulation
    void yearBegin();
    void yearRun( bool modelPartnership,bool modelDiseaseProg,bool modelTranmission );
    void yearEnd(bool modelNaturalDyn );
    //
    void burnin( int demogBurnin, int partBurnin, int epidemicBurnin);
    void initilizeEpidemic();
    //
    vector<vector<double>> modelRun(int simTime);
    void replicateModel(int rep,int simTime);
    //
    vector<double> modelRun_prev(int simTime);//runs the model and returns prev vector
    void replicateModel_prev(int rep,int simTime);
    vector<vector<double>> replicateModel_prev1(int rep,int simTime);
    
    
    //FULL CALIBRATIONS: runs a single model and writes outputs for each year
    void CalibrateDemographics_annualOutputs( int years, int popPrintingIntervals, int csaPrintingInterval);//for naturalDynamics, moutputing popsize, race, age///creates a single file for pop demog outputs and seperate files for cSAs in the given intervals
    void calibratePartnership_annualOutputs( int demogBurnin, int partBurnin, int steadyYears);//outputs results to a file
    void calibrateEpidemic_annualOutputs( int steadyYears);
    
    //SUMMARY CALIBRATION: runs a model and returns average outputs over a number of years
    vector<vector<double>> calibrateModel_summaryOutputs( int steadyYears);//outputs results to a file
    
    //PREP
    void runPrep_full(int years, int prepStartTime, vector<double> prepInfo  );//runs the model from steady state and retunrs the HIV outputs
    vector<vector<double>> runPrepScenarios(vector<double> vScenarios,vector<double> vCov,vector<double> vAdh,vector<double> vDuration,vector<double> vDropout);
    vector<vector<double>>  runPrep(vector<double> prepInfo );//runs the model and returns the HIV inc and prep
    vector<double> runModel_Inc(int years );
    vector<double> runPrep_Inc( vector<double> prepInfo );//runs the model and returns the HIV inc
    
    //SENSITIVITY ANALYSIS
    vector<vector<double>>  SA_oneway(int burnin, int steady,vector<ModParam *> vMPs,vector<double> prepInfo);
    void SA_random(int burnin, int steady,vector<ModParam *> vMPs,std::mutex &simLock);
    
    
    inline gsl_rng * getRNG(){return rng;}
    vector<vector<double>> returnAnimationInfo();
    void debugPop();
    Population * getPop(){return pop;}
    ModParam * mp_local;
private:
    Population * pop;
    gsl_rng * rng;
    
    
};


#endif /* defined(__HIVMODEL__DRIVER__) */
